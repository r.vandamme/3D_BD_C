#ifndef BD_VORO_C_WRAPPER_H
#define BD_VORO_C_WRAPPER_H

#ifdef __cplusplus
extern "C" {
#endif

void c_voro_get_volumes(
	double x_min, double x_max,
	double y_min, double y_max,
	double z_min, double z_max,
	int n_part,
	double pos[][3],
	double vols[]
);

void c_voro_get_volumes_and_neighbours(
	double x_min, double x_max,
	double y_min, double y_max,
	double z_min, double z_max,
	int n_part,
	double pos[][3],
	double vols[],
	int n_neighbours[],
	int **neighbours
);

#ifdef __cplusplus
}
#endif


#endif
