#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "mt19937ar.c" /* Random number generator */
#include "voro_c_wrapper.h"

/*========================== Global program modes set on compile ==========================*/

#define SOFTEN_WCA 1			// Softens the WCA potential for improved stability.
#define DISTURB_INITIAL 1		// Applies small random offset to initial configurations.
#define DIM 3					// Dimensionality. Might work for 2, probably needs major adjustment.
#define FORCETROUBLE 0			// Turns on various print functions for troubleshooting.
#define CELLTROUBLE 0			// Turns on various print functions for troubleshooting.
#define MESHTROUBLE 0			// Turns on various print functions for troubleshooting.
#define PARTICLEINTERACTIONS 1	// Whether particle-particle interactions are turned on.

/*========================== Global program modes set on load ==========================*/

int HAVE_MESH=0;			// Whether we simulate a membrane using a triangulated mesh.
int DYNAMIC_MESH=0;			// Whether the mesh can move.
int CROSSLINKS=0;			// Whether there are crosslinkers in the simulation.
int ON_SPHERE=0;			// Whether we simulate on an analytical sphere surface.
int INSIDE_SPHERE=0;		// Whether we simulate in a repulsive sphere surface.
int INSIDE_CYLINDER=0;		// Whether we simulate in a repulsive cylindrical surface.
int LONG_BOX=0;				// Whether the simulation volume is elongated in one direction.
int SPHERES=0;				// Whether all particles are spheres (or if 0, are rods)
int HAVE_GRAVITY=0;			// Whether we have gravity (in the minus y direction)
int HAVE_PLANAR_WALLS=0;	// Whether there are flat walls at some y values
int QUASI_2D=0;				// Whether we are in quasi-2D confinement
int FIX_TIME_STEP=0;        // Whether we use a fixed time step
int DIFFUSION_CONST_FROM_LOD=0; // Whether the rod aspect ratio defines the diffusion constants

/*========================== Defined constants ==========================*/

#define MAX_PART_CELL 4000		// Maximum # of particles per cell. Currently arbitrarily chosen.
#define N_GR_BINS 1000			// # of bins for the g(r) histogram.
#define N_P_ETA_BINS 500		// # of bins for the p(packfrac) histogram.
#define N_SWIMSPEED_BINS 200	// # of bins for the effective swim speed histogram.
#define N_DENSITY_X_BINS 200	// # of bins for the slab packing fraction histogram.
#define N_PRESSURE_X_BINS 200	// # of bins for the slab pressure histogram.
#define MAX_CL_PART 50			// Maximum # of crosslinkers attached to one particle.
#define MAX_VERTEX_NB 16		// Maximum # of neighbours a vertex can have in the mesh.
#define MAX_FACES_PER_VERTEX 16	// Maximum # of faces that one vertex can be part of.
#define NPARTS_TO_TRACK_MSD 600	// Maximum # of particles to calculate the mean square displacement for
#define NPARTS_TO_TRACK_ORCOR 600// Maximum # of particles to calculate the orientational correlation for
#define TWO_POW_ONE_SIXTH 1.1224620483093729814335330496792 // range at which WCA is cut off
#define TWO_POW_ONE_THIRD 1.2599210498948731647672106072782 // range at which WCA is cut off, squared

/*========================== Structures ==========================*/

typedef struct {	// Struct for the periodic box
    double L[DIM];			// axis lengths
    double hL[DIM];			// half of the length
    double iL[DIM];			// one over the length
    double ihL[DIM];		// one over half the length
    double shL;				// Smallest length
	double bhL;				// Biggest length
	double volume;			// Volume
}tbox;
tbox box;
typedef struct {	// Struct for the particles
	long int index;						// Index of particle in particle array
  double pos[DIM]; 					// Current position
	double or[DIM];						// Current parallel orientation
	double orper1[DIM];					// Vector perpendicular to the rod 1
	double orper2[DIM];					// Vector perpendicular to the rod 2
	double force[DIM];					// Force on the particle
	double torque[DIM];					// Torque on the particle
  double d;							// Width of the rod (used as length scale, keep 1.!)
  double l;							// Length of the rod
  double hll;							// Half the length of the rod's line segment (hll=l/2-d)
	double vol;							// Volume of particle
  long int cell;						// Cell of the particle in the particle-particle cell list
  long int cell_wall;					// Cell of the particle in the wall cell list
  double nem;							// Nematic order parameter
  double localpackfrac;				// A measure of the packing fraction around the particle
  double virialstress[DIM][DIM];		// Virial stress tensor of one particle
  double swimstress[DIM][DIM];		// Virial stress tensor of one particle
  int isactive;						// Whether this particle is active or not
  double prevrands_trans[3];			// Previous random numbers in BAOAB integration
  double prevrands_rota[3];			// Previous random numbers in BAOAB integration
  double prev_pos[DIM];				// Previous position for velocity tracking
  double prev_or[DIM];				// Previous position for velocity-orientation correlation tracking
  //int ncrosslinks;					// Number of crosslinkers attached to particle
  //tcrosslink cllist[MAX_CL_PART];	// Pointers to crosslinkers attached to particle
  double rpos[DIM]; // non-periodic position
} tpart;
tpart *part;
typedef struct {	// Struct for chains of particles
	short int np;	// Number of particles in the chain
	long int *part;	// Indices of particles in the chain
} tchain;
tchain *chain;
typedef struct {	// Struct for the crosslinkers
	long int index;		// Index of crosslink in array of crosslinks
	tpart *p1;			// Pointer to particle 1 the crosslinker is attached to
	tpart *p2;			// Pointer to particle 2 the crosslinker is attached to
	double l1;			// How far along (units d) is the attachment to 1 along its orientation ui
	double l2;			// How far along (units d) is the attachment to 2 along its orientation uj
	double length;		// Length of the crosslink
	double force[DIM];	// Current extension force
} tcrosslink;
tcrosslink *crosslink;
typedef struct {	// Struct for the cell list for the particle-particle interaction
	long int index;				// Index of cell in cell array
    long int neighbours[27];	// Number of neighbouring cells
    long int n;					// Number of particles in cell
    long int *particles;		// Array of particles in cell
} tcell;
tcell *cells;
typedef struct {	// Struct for the cell list for the particle-mesh interaction
	long int index;				// Index of cell in cell array
    long int neighbours[27];	// Array of neighbouring cells
    long int np;				// Number of particles in cell
    long int *particles;		// Array of particles in cell
    long int nv;				// Number of vertices in cell
    long int *vertices;			// Array of vertices in cell
    long int ne;				// Number of edges in cell
    long int *edges;			// Array of edges in cell
    long int nf;				// Number of faces in cell
    long int *faces;			// Array of faces in cell
} twallcell;
twallcell *wallcells;
typedef struct {	// Struct containing cell pointers to check for spherical wall
	long int index;			// Index of cell in cell array
	long int n_cells;		// Number of cells in the list below.
	twallcell* cell[20000];	// Array of pointers to cells.
} tspherecells;
tspherecells sphere_cell_list;

/* Structures to define an object of a given shape with a mesh */

typedef struct {	// Struct for the vertices of the mesh
	long int index;								// Index of vertex in vertex array
	double pos[DIM];							// Current position
	double force[DIM];							// Force on the particle
	long int cell;								// Wall cell
	short int nnb;								// Number of neighouring vertices
	long int nb[MAX_VERTEX_NB];					// Array of neighbouring vertices
	double eqr[MAX_VERTEX_NB];					// Equilibrium distance from neighbours
	double eqv[MAX_VERTEX_NB][DIM];				// Equilibrium vector from neighbours
	short int innfaces;							// Number of faces this vertex is part of
	long int infaces[MAX_FACES_PER_VERTEX];		// Face indices of which this vertex is part of
    double prevrands[DIM];						// Previous random numbers in BAOAB integration
} tvertex;
typedef struct {	// Struct for the edges of the mesh
	/* Edges connect 2 vertices */
	long int index;						// Index of edge in edge array
	long int vertices[2];				// Vertex indices of the edge
	long int faces[2];					// Edge is hinge for these two faces
	double vec[DIM];					// Vector going from vertex 0 to vertex 1
	double or[DIM];						// Orientation defined as vec/norm(vec)
	double centerpos[DIM];				// Position of the middle of the line
	double elen;						// Length of the line
	double eq_elen;						// Equilibrium length of the edge
	double hll;							// Half of the length of the line
	long int cell;						// Index of the wall cell this edge is in
	/* Edges are also hinges for faces */
	unsigned int is_boundary;
	tvertex *hinge_v[4];
	unsigned int hinge_v_i[4];
	double eq_sin_h_th;
	double hinge_e_vec[4][3]; // Apart from this one, there are 4 edges
} tedge;
typedef struct {	// Struct for the faces of the mesh
	long int index;						// Index of face in face array
	short int nvertices;				// Number of vertices in this face
	long int *vertices;					// Indices of vertices in this face
	short int nedges;					// Number of edges in this face
	long int edges[3];					// Indices of edges in this face
	long int cell;						// Index of the wall cell this face is in
	double centerpos[DIM];				// Mean position of the vertices
	double outwardnormal[DIM];			// Unit vector pointing out of the mesh
	double rotmatrix_xy[DIM][DIM];		// Matrix that would rotate this face parallel to yz plane
	double t_xy[3][3];					// Vertex positions in the xy plane
	double T[2][2];						// Barycentric transformation matrix
	double detTinv;						// Inverse determinant for barycentric transformation matrix
	double area;						// Area of the face
} tface;
typedef struct {	// Struct for a mesh object of some shape
	tvertex *vertices;
	long int nvertices;
	tedge *edges;
	long int nedges;
	tface *faces;
	long int nfaces;
	double volume;
} tmesh;
tmesh mesh;

/* Structures to make tracking statistics easier */
typedef struct {	// Struct for an observable with a mean and a variance
	long int samples;	// Number of times this value has been measured
	double current;		// Current value
	double mean;		// Current mean of all measured values
	double prev_mean;	// Mean at previous measurement
	double mean_sq;		// Square of current mean
	double mvariance;	// Current variance times number of measurements
	int n_saved_to_file;    // How often this histogram has been saved to a file
} tobservable;
typedef struct {	// Struct for a histogram of observables
	long int n_bins;        // Number of bins in this histogram
	double *xvalue;         // Values of "x-axis" at bin centers
	tobservable *value;     // Array of pointers to observables in this histogram
	int n_saved_to_file;    // How often this histogram has been saved to a file
} thistogram;
typedef struct {	// Struct for a histogram of observables
	long int n_bins;        // Number of bins in this histogram
	int xval_size;          // Size of each xvalue element
	double **xvalue;        // Values of "x-axis" at bin centers
	int val_size;           // Size of each value element
	tobservable **value;    // Array of pointers to observables in this histogram
} t_array_histogram;

/*========================== Global parameters ==========================*/

/* Physical parameters and dimensionless constants: */

long int n_part=1000;					// # of particles
double packfrac=0.4;					// Initial packing fraction (constant if NVT)
double Peclet=50;						// (parallel translational) Peclet number
double lod=1.5;							// (total!) length / diameter of the rods (aspect ratio)
double stretch=1E-1;						// Energy scale of stretching the mesh, in units kT
double bend=1E-2;							// Energy scale of bending the mesh, in units kT
double areafrac=0.80;					// Initial area fraction (only used for ON_SPHERE==1)
double crosslink_unbinding_prob=0.01;	// Rate of crosslink unbinding (k_u*dt, dimensionless)
double crosslink_binding_prob=0.1;		// Rate of crosslink binding (k_b*dt, dimensionless)

/* Global variables which aren't neccessarily physical input variables: */

double fixed_time_step;                 // Factor of unit time that one time step will be.
double active_force;					// Active force
double persistence_length;				// self_propulsion_velocity*tau_r/(dim-1), is usually the longest lengthscale
double friction_gamma;					// Friction coefficient, kT / diffusion
double self_propulsion_velocity;		// Swimming speed of a particle, in [particle_diameter/timescale]
double density;							// Dimensionless number density
double time_r;							// Running time
double time_max;						// Time when the simulation stops
unsigned long long int tot_steps=1E6;	// The number of steps that we integrate
unsigned long long int eq_steps=1E6;	// Initialization
unsigned long long int step=0;			// Steps
unsigned long int nprint=1E3;			// How often we print to terminal
unsigned long int nsample=1E9;			// How often we measure (every nsample timesteps)
unsigned long int nfsample=4e9;			// How often we measure things frequently (every nsample timesteps)
unsigned long int snapshots=1000;		// Number of snapshots in the big time series file
unsigned long int few_snapshots=3;		// Number of snapshots in the sparse time series file
unsigned long int datapoints=10000;		// Number of data points in measurement file
double dt;								// Time step
double virial;							// Virial coefficient to measure pressure
double swim_virial;						// virial for swim pressure
double diffusion_t_sph;      // translational diffusion coefficient of spheres
double diffusion_para_rod;   // (parallel) translational diffusion coefficient of rods
double diffusion_perp_rod;				// Perpendicular diffusion coefficient
double random_force_prefactor_t_sph;	// Coefficient to make integration faster
double random_force_prefactor_para_rod;	// Coefficient to make integration faster
double random_force_prefactor_perp_rod;	// Coefficient to make integration faster
double random_force_prefactor_rota_rod;	// Coefficient to make integration faster
double random_force_prefactor_r_sph;	// Coefficient to make integration faster
double wca_prefactor;					// Coefficient to make integration faster
double force_prefactor_sph;				// Coefficient to make integration faster
double force_prefactor_para;			// Coefficient to make integration faster
double force_prefactor_perp;			// Coefficient to make integration faster
double force_prefactor_rota;			// Coefficient to make integration faster
double active_prefactor_rod;			// Coefficient to make integration faster
double active_prefactor_sph;			// Coefficient to make integration faster
double boundary_force_prefactor;		// Coefficient to make integration faster
double stretching_force_prefactor;		// Coefficient to make integration faster
double bending_force_prefactor;			// Coefficient to make integration faster
double max_force_translation_sq;		// Cutoff for displacement due to forces to avoid instability
double max_torque_rotation_sq;			// Cutoff for rotation due to torques to avoid instability
int run=0;								// Variable to avoid overwriting input file
double Vpart;							// Total volume occupied by particles
double aspect_ratio_box=5;				// Aspect ratio of simulation volume if elongated
double height_Q2D=1.12246;				// Height of the quasi-2D confinement in particle diameters
double diameter_cylinder=5;				// Diameter of a confining cylinder (2 is default value)

double RADIUS,RADIUSSQ;

/* Stuff for shrinking droplet */
double startpackfrac=0.20;
double endpackfrac=0.50;

/* Parameters relating to crosslinks */

long int n_crosslinks;					// Current number of crosslinks. Varies.
double crosslink_force_prefactor=1E0;	// Prefactor of the crosslink spring force (Kd^2/kT, dimensionless)
double crosslink_speed=1E-2;			// Speed of crosslinks in units of d/dt. (that's not a derivative!)
double crosslink_displacement;			// Displacement along rod per timestep dt. Units of d.
int MAX_CROSSLINKS;						// Maximum number of crosslinks in the system at any given time.

/* Parameters that have averages that need to be tracked */

unsigned long long int F_softened,F_evaluated;				// Number of forces softened/evaluated
unsigned long long int T_softened,T_evaluated;				// Number of torques softened/evaluated
unsigned long long int measurements;						// how many measurements we have done

/* Observables */
tobservable v_eff;
tobservable pressure, nematic_order, polar_order;
tobservable virial_pressure,swim_pressure;
tobservable max_cluster_fraction, degree_of_clustering;

/* Histograms */
thistogram hist_MSD;
long int n_time_lags_in_MSD_array;
thistogram hist_or_cor;
long int n_time_lags_in_orcorr_array;
double (**pos_at_t_of_p)[DIM],(**or_at_dt_of_p)[DIM];// positions, orientations of particles at multiple times, used in MSD/orcorr calc.
thistogram hist_gr,hist_gr_idgas;
double maxsep;					// Maximum possible distance between two particles that makes sense for g(r)
thistogram hist_p_eta;
thistogram hist_packfrac_voro;
thistogram hist_density_x;
thistogram hist_virial_pressure_x;
thistogram hist_swim_pressure_x;
thistogram hist_effective_velocity;
thistogram hist_velocity_probability;
thistogram hist_spatial_velocity_correlation;
thistogram hist_spatial_orientation_correlation;
t_array_histogram arrayhist_3D_density_field;

/* Fields */
// double (***velocity_field)[DIM]; // Array of bins of coarse-grained velocities
// double (***vorticity_field)[DIM]; // Array of bins of coarse-grained vorticities
int n_field_bins_x,n_field_bins_y,n_field_bins_z;


/* Needed for the cell list */

long int n_cells;							// number of cells in the particle-particle cell list
double max_interaction_length_2rods;		// maximum range at which two rods interact with each other
double max_interaction_length_bbox;			// constant used for bounding boxes
int nx,ny,nz;								// # of cells in each direction
int neighbouring_cells;						// number of cell neighbours+itself, needed so n_cells<27 works

long int n_cells_wall;						// Number of cells in the wall cell list
double max_interaction_length_rod_point;	// maximum range at which a rod can interact with a point
int nx_w,ny_w,nz_w;							// # of cells in each direction
int neighbouring_cells_wall;				// number of cell neighbours+itself, needed so n_cells<27 works
double max_vertex_spacing;					// Helper variable to track how large wall cells should be
double max_vertex_spacing_initial;			// Helper variable for vertex overlap avoidance
double max_interaction_length_rod_mesh;		// Maximum range at which a rod can interact with a mesh object
double current_cell_size;					// Current size of the existing cell list
double current_wallcell_size;				// Current size of the existing wall cell list

/* Variables for the random number generator */
double storedval=0;						// Stored random gaussian number for next evaluation
double gaussianrand();

/* mathematical tensor, require initialization from inittensors() at main-level */
double Identity[DIM][DIM];				// Identity tensor
double Rot_flip_z[DIM][DIM];			// Rotation matrix flipping the z

/*========================== Math functions ==========================*/

/* --------- 3D inner vector product ---------
 * Inner product between two vectors in 3D space. Easily expanded to arbitrary dimensions.
 */
static inline double inprod(double v1[3], double v2[3]){
	return v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2];
}
/* --------- norm of a vector ---------
 * Calculates the norm of a vector. Seriously. There's not anything more to it. In fact,
 * this particular comment is already redundant and superfluous. Why are you still reading
 * it?
 */
static inline double norm(double vec[DIM]){
	double nrm=0;
	int d;
	for(d=0;d<DIM;d++){nrm+=vec[d]*vec[d];}
	return sqrt(nrm);
}
/* --------- square of the norm of a vector ---------
 * Square of the norm of a vector. Sometimes we compare squared values, which means
 * it's a waste to calculate an additional square root.
 */
static inline double normsq(double vec[DIM]){
	double nrm=0;
	int d;
	for(d=0;d<DIM;d++){nrm+=vec[d]*vec[d];}
	return nrm;
}
/* --------- Simple 3D outer product function ---------
 * Outer product / cross product of two vectors in 3D space.
 * Newvec = v1 X v2
 */
static inline void crossprod(double newvec[3],double v1[3], double v2[3]){
	newvec[0]=v1[1]*v2[2]-v1[2]*v2[1];
	newvec[1]=v1[2]*v2[0]-v1[0]*v2[2];
	newvec[2]=v1[0]*v2[1]-v1[1]*v2[0];
	return;
}
/* --------- Simple vector normalization function ---------
 * Normalizes whatever vector is its input to a length of 1.
 */
void normalize(double vec[DIM]){
	int d;
	double norm=0.,invnorm;
	for(d=0;d<DIM;d++){norm+=(vec[d]*vec[d]);}
	if(norm==0.){printf("Error: trying to normalize null vector!\n");exit(666);}
	//printf(" %lf %lf %lf \n",vec[0],vec[1],vec[2]);
	invnorm=1./sqrt(norm);
	for(d=0;d<DIM;d++){vec[d]*=invnorm;}
	return;
}
/* --------- Gives the direction of vec ---------
 * Normalizes whatever vector is its input to a length of 1 and assigns that to dir.
 */
void direction(double dir[DIM], double vec[DIM]){
	int d;
	double norm=0.,invnorm;
	for(d=0;d<DIM;d++){norm+=(vec[d]*vec[d]);}
	if(norm==0.){printf("Error: trying to normalize null vector!\n");exit(666);}
	//printf(" %lf %lf %lf \n",vec[0],vec[1],vec[2]);
	invnorm=1./sqrt(norm);
	for(d=0;d<DIM;d++){dir[d]=vec[d]*invnorm;}
	return;
}
/* --------- Dyadic vector product ---------
 * if u={u1,u2,u3} and v={v1,v2,v3} then the dyadic product uv is
 * uv= { {u1v1,u1v2,u1v3},{u2v1,u2v2,u2v3},{u3v1,u3v2,u3v3} }
 */
static inline void dyadic(double t[DIM][DIM], double v1[DIM], double v2[DIM]){
	int i,j;
	for(i=0;i<DIM;i++){
		for(j=0;j<DIM;j++){
			t[i][j]=v1[i]*v2[j];
		}
	}
	// 	printf(" v1: %6.06lf %6.06lf %6.06lf \n",v1[0],v1[1],v1[2]);
	// 	printf(" v2: %6.06lf %6.06lf %6.06lf \n",v2[0],v2[1],v2[2]);
	// 	printf(" t:\n (%lf  %lf  %lf)\n (%lf  %lf  %lf)\n (%lf  %lf  %lf)\n\n",
	// 		t[0][0],t[0][1],t[0][2],
	// 		t[1][0],t[1][1],t[1][2],
	// 		t[2][0],t[2][1],t[2][2]);
	return;
}
/* --------- Initialize various constant tensor (e.g. identity) ---------
 * Any mathematical tensors that remain constant over the entirety of the simulation
 * (such as the identity tensor or levi-civita tensor) go here.
 */
void inittensors(void){
	int i,j;

	/* Identity tensor */
	for(i=0;i<DIM;i++){
		for(j=0;j<DIM;j++){
			Identity[i][j]=(i==j)?1.:0.;
		}
	}

	/* Rotation matrix that flips the z direction */
	Rot_flip_z[0][0]=1;
	Rot_flip_z[0][1]=0;
	Rot_flip_z[0][2]=0;

	Rot_flip_z[1][0]=0;
	Rot_flip_z[1][1]=-1;
	Rot_flip_z[1][2]=0;

	Rot_flip_z[2][0]=0;
	Rot_flip_z[2][1]=0;
	Rot_flip_z[2][2]=-1;
}
/* --------- Truncates a vector to have its squared norm not exceed the squared threshold ---------
 * Used primarily to make sure displacements and rotations do not obtain huge values
 * and destabilize the entire simulation. This function leaves the direction intact.
 * Returns a boolean value that says whether vector was truncated or not.
 */
static inline int vectruncate(double v[DIM], double threshold_sq){
	double norm_sq = normsq(v);
	if( norm_sq > threshold_sq ){ // check norm
		double sf = sqrt( threshold_sq / norm_sq ); // scaling factor
		for(int d = 0; d < DIM; d++){ v[d] *= sf; } // scale vector
		return 1;
	}
	return 0;
}
/* --------- vector addition nv = v1 + v2 ---------
 *
 */
static inline void vecadd(double nv[DIM], double v1[DIM], double v2[DIM]){
	int d;
	double newv[DIM];
	for(d=0;d<DIM;d++){
		newv[d] = v1[d] + v2[d];
	}
	memcpy(nv,newv,sizeof(newv));
}
/* --------- vector subtract nv = v1 - v2 ---------
 *
 */
static inline void vecsubtract(double nv[DIM], double v1[DIM], double v2[DIM]){
	int d;
	double newv[DIM];
	for(d=0;d<DIM;d++){
		newv[d] = v1[d] - v2[d];
	}
	memcpy(nv,newv,sizeof(newv));
}
/* --------- Vector-matrix product v.m ---------
 * Takes the vector-matrix product of a square matrix.
 */
static inline void vminprod(double nv[DIM], double v[DIM], double m[DIM][DIM]){
  int i,j;
  for(i=0;i<DIM;i++){
	nv[i]=0;
    for(j=0;j<DIM;j++){
      nv[i]+=(m[j][i]*v[j]);
    }
  }
  return;
}
/* --------- Matrix-vector product m.v=(v.m)^T ---------
 * Takes the matrix-vector product of a square matrix. Is the transpose of vminprod().
 */
static inline void mvinprod(double nv[DIM], double m[DIM][DIM], double v[DIM]){
	int i,j;
	for(i=0;i<DIM;i++){
		nv[i]=0;
		for(j=0;j<DIM;j++){
			nv[i]+=(m[i][j]*v[j]);
		}
	}
	return;
}
/* --------- 3D Matrix-matrix product m=M1.M2 ---------
 *
 */
static inline void mmprod(double m[3][3], double M1[3][3], double M2[3][3]){
	int i,j,k;
	double newM[3][3];
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			newM[i][j]=0.;
			for(k=0;k<3;k++){
				newM[i][j]+=((M1[i][k])*(M2[k][j]));
			}
		}
	}
	memcpy(m,newM,sizeof(newM));
}
/* --------- 2D matrix trace, sum of its diagonal ---------
 *
 */
static inline double mtrace2D(double m[2][2]){
	return m[0][0]+m[1][1];
}
/* --------- 3D matrix trace, sum of its diagonal ---------
 *
 */
static inline double mtrace3D(double m[3][3]){
	return m[0][0]+m[1][1]+m[2][2];
}
/* --------- Calculates determinant of 3x3 matrix ---------
 *
 */
static inline double det3D(double m[3][3]){
	return ( m[0][0] * (m[1][1]*m[2][2] - m[2][1]*m[1][2] )
         - m[0][1] * (m[1][0]*m[2][2] - m[2][0]*m[1][2] )
         + m[0][2] * (m[1][0]*m[2][1] - m[2][0]*m[1][1] ));
}
/* --------- Calculates determinant of 2x2 matrix ---------
 *
 */
static inline double det2D(double m[2][2]){
	return  (m[0][0]*m[1][1]-m[0][1]*m[1][0]);
}
/* --------- Transpose of a 3x3 matrix ---------
 *
 */
static inline void mtranspose3D(double nm[3][3], double m[3][3]){
	double newm[3][3];

	newm[0][0] = m[0][0];
	newm[0][1] = m[1][0];
	newm[0][2] = m[2][0];

	newm[1][0] = m[0][1];
	newm[1][1] = m[1][1];
	newm[1][2] = m[2][1];

	newm[2][0] = m[0][2];
	newm[2][1] = m[1][2];
	newm[2][2] = m[2][2];

	memcpy(nm,newm,sizeof(newm));
}
/* --------- Calculates the inverse of a 3x3 matrix m ---------
 *
 */
static inline void minverse3D(double nm[3][3], double m[3][3]){
	double det = det3D(m);
	if(det == 0.0){ printf("Error: Attempted to invert non-invertable matrix!\n"); exit(42); }
	double invdet = 1.0 / det;
	// Calculate the inverse and store in temp matrix to avoid m = nm errors
	double tm[3][3];
	tm[0][0] = (m[1][1] * m[2][2] - m[2][1] * m[1][2]) * invdet;
	tm[0][1] = (m[0][2] * m[2][1] - m[0][1] * m[2][2]) * invdet;
	tm[0][2] = (m[0][1] * m[1][2] - m[0][2] * m[1][1]) * invdet;
	tm[1][0] = (m[1][2] * m[2][0] - m[1][0] * m[2][2]) * invdet;
	tm[1][1] = (m[0][0] * m[2][2] - m[0][2] * m[2][0]) * invdet;
	tm[1][2] = (m[1][0] * m[0][2] - m[0][0] * m[1][2]) * invdet;
	tm[2][0] = (m[1][0] * m[2][1] - m[2][0] * m[1][1]) * invdet;
	tm[2][1] = (m[2][0] * m[0][1] - m[0][0] * m[2][1]) * invdet;
	tm[2][2] = (m[0][0] * m[1][1] - m[1][0] * m[0][1]) * invdet;
	// Copy temp matrix into nm
	memcpy(nm,tm,sizeof(tm));
}
/* --------- Calculates the highest absolute eigenvalue/vector ----------
 * Power iteration method for computing eigenvalues of a matrix. Yields the highest absolute
 * eigenvalue as "nrm" and corresponding eigenvector as "v". This is an iterative scheme, and loops
 * until it reaches a certain accuracy. This very occasionally produces the wrong eigenvalues,
 * so should not be used in other iterative schemes.
 */
double eig(double m[DIM][DIM]){
	double nv[DIM],v[DIM];
	int d;
	double nrm,prevnrm;

	/* Calculate once */
	for(d=0;d<DIM;d++){v[d]=genrand();}
	mvinprod(nv,m,v);
	nrm=norm(nv);
	for(d=0;d<DIM;d++){v[d]=nv[d]/nrm;}

	/* Then iterate until convergence */
	do{
		mvinprod(nv,m,v);
		prevnrm=nrm;
		nrm=norm(nv);
		for(d=0;d<DIM;d++){v[d]=nv[d]/nrm;}
		//printf("vec: %lf %lf %lf\n",v[0],v[1],v[2]);
		//printf("nrm: %lf\n\n",nrm);
	}while(fabs(nrm-prevnrm)>1E-5);
	return nrm;
}
/* --------- Sign fixing function function ---------
 * Needed for rod distance function. Takes a and b, returns a with the same sign as b.
 */
static inline double samesign(double a, double b){
	return a=fabs(a),(b<0)?-a:a;
}

/*========================== Geometric functions ==========================*/

/* --------- Distance between two points ---------
 *
 */
double distance(double p1[DIM], double p2[DIM]){
	int d;
	double distsq=0;
	for(d=0;d<DIM;d++){
		distsq+=(p1[d]-p2[d])*(p1[d]-p2[d]);
	}
	return sqrt(distsq);
}
/* --------- Distance squared between two points ---------
 *
 */
double distancesq(double p1[DIM], double p2[DIM]){
	int d;
	double distsq=0;
	for(d=0;d<DIM;d++){
		distsq+=(p1[d]-p2[d])*(p1[d]-p2[d]);
	}
	return distsq;
}
/* --------- Nearest image convention for CM distance ---------
 * Takes two positions as input and returns a vector from position 1 to position 2 that takes into
 * account the periodicity of the simulation volume.
 */
void nearestimage(double pos1[DIM], double pos2[DIM], double nearvec[DIM]){
	// NOTE: Since this function is called so often, I've spent some time trying to optimize it.
	// There are currently three possible versions you can try that are completely equivalent.
	// The little note above each block gives an estimate of the amount of processor clock
	// cycles needed to evaluate that version of the function.

	// NOTE: estimated 27 cycles
	// double r12[DIM];
	// int d;
	// for(d=0;d<DIM;d++){
	// 	r12[d]=pos2[d]-pos1[d];	// from 1 to 2, so pos2-pos1
	// 	nearvec[d]=(fabs(r12[d])<box.hL[d]) ? r12[d] : (r12[d]-( (r12[d]>0)?box.L[d]:-box.L[d]) );
	// }

	// NOTE: estimated 24 cycles
	int d;
	for(d = 0; d < DIM; d++){
		nearvec[d] = pos2[d] - pos1[d]; // from 1 to 2, so pos2-pos1
		if(fabs(nearvec[d]) > box.hL[d]){
			if(nearvec[d] > 0.0) nearvec[d] -= box.L[d];
			else                 nearvec[d] += box.L[d];
		}
	}

	// NOTE: estimated 33 cycles
	// int d;
	// for(d = 0; d < DIM; d++){
	// 	nearvec[d] = pos2[d] - pos1[d];
	// }
	// if     (nearvec[0] >  box.hL[0]){ nearvec[0] -= box.L[0]; }
	// else if(nearvec[0] < -box.hL[0]){ nearvec[0] += box.L[0]; }
	// if     (nearvec[1] >  box.hL[1]){ nearvec[1] -= box.L[1]; }
	// else if(nearvec[1] < -box.hL[1]){ nearvec[1] += box.L[1]; }
	// if     (nearvec[2] >  box.hL[2]){ nearvec[2] -= box.L[2]; }
	// else if(nearvec[2] < -box.hL[2]){ nearvec[2] += box.L[2]; }
}
/* --------- Nearest image distance squared ---------
 *
 */
double nearestimagedistsq(double pos1[DIM], double pos2[DIM]){
	double nearvec[DIM];
	nearestimage(pos1, pos2, nearvec);
	return normsq(nearvec);
}
/* --------- Calculates the unit normal to a plane (or face) in 3D space ----------
 * Calculates the normal to a plane.
 * Input: points on the plane p0,p1,p2
 * Output: normal vector nrm
 */
void planenormal(double nrm[3], double p0[3], double p1[3], double p2[3]){
	double u[3],v[3];
	nearestimage(p0,p1,u);
	nearestimage(p0,p2,v);
	// 	nearestimage(p1,p0,u);
	// 	nearestimage(p2,p0,v);
	// 	u[0]=p1[0]-p0[0];	u[1]=p1[1]-p0[1];	u[2]=p1[2]-p0[2];
	// 	v[0]=p2[0]-p0[0];	v[1]=p2[1]-p0[1];	v[2]=p2[2]-p0[2];
	crossprod(nrm,u,v);
	normalize(nrm);
	//printf("nrm: %f %f %f\n",nrm[0],nrm[1],nrm[2]);
	return;
}
/* --------- Calculates the rotation matrix to rotate face into xy plane ----------
 * Calculates the rotation matrix needed to align the normal vector parallel to the positive z
 * direction, which aligns the face with the xy plane.
 *
 * Source:
 * http://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
 */
void setxyrotationmatrix(double xy_rotation[3][3], double normal[3]){
	int i,j;
	double z[3]={0,0,1};
	double v[3];

	double n_in_z=inprod(normal,z);
	if(n_in_z+1.<1E-5){ // method doesn't work for n.z == -1
		memcpy(xy_rotation,Rot_flip_z,sizeof(Rot_flip_z));
		return;
	}

	/* Set cross product vector v = n X z */
	crossprod(v,normal,z);
	if(norm(v)<1E-5){ // if n X z == 0
		memcpy(xy_rotation,Identity,sizeof(Identity));
		return;
	}
	//printf("\nf->nrml: %f %f %f\n",f->outwardnormal[0],f->outwardnormal[1],f->outwardnormal[2]);
	//printf("\n v: %f %f %f\n",v[0],v[1],v[2]);

	/* Fraction using cosine (inner product) of angle between the vectors */
	double frac=1./(1.+n_in_z);

	/* Set skew-symmetric cross product matrix */
	double vcr[3][3]={ {0,-v[2],v[1]}, {v[2],0,-v[0]}, {-v[1],v[0],0} };
	double vcrsq[3][3];
	mmprod(vcrsq,vcr,vcr);

	/* Now set the rotation matrix */
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			xy_rotation[i][j]=Identity[i][j]+vcr[i][j]+vcrsq[i][j]*frac;
		}
	}

	// 	printf("Rotation matrix:\n (%lf, %lf, %lf) \n (%lf, %lf, %lf) \n (%lf, %lf, %lf) \n",
	// 		   f->rotmatrix_xy[0][0],f->rotmatrix_xy[0][1],f->rotmatrix_xy[0][2],
	// 		   f->rotmatrix_xy[1][0],f->rotmatrix_xy[1][1],f->rotmatrix_xy[1][2],
	// 		   f->rotmatrix_xy[2][0],f->rotmatrix_xy[2][1],f->rotmatrix_xy[2][2]);
	// 	exit(1);
	return;
}
/* --------- Calculates various properties of a face needed for distance calculation ----------
 * This function calculates two things in particular: the tensors t_xy and T. t_xy is a 3x3 tensor
 * containing the three positions of the vertices of the triangle that forms the face. We translate
 * the entire face so that its first vertex is at the origin (0,0,0), and rotate it such that it
 * lies in the xy plane. So t_xy[0] is always (0,0,0) and t_xy[1][2] and t_xy[2][2] are always 0.
 * The tensor T denotes the transformation matrix to transform a point (x,y) into barycentric
 * coordinates: that is, as a linear combination of the three face vertex positions. Finally, we
 * also calculate the inverse of the determinant of T so that we only have to do it once.
 */
void calc_face_helper_vars(tface *f){
	int i,d;
	double t_origin[3][3];

	/* Calculate rotation matrix to get f into xy plane */
	setxyrotationmatrix(f->rotmatrix_xy, f->outwardnormal);

	/* Translate face close to origin to avoid needing nearest image */
	t_origin[0][0]=0.;
	t_origin[0][1]=0.;
	t_origin[0][2]=0.;
	for(i=1;i<3;i++){
		for(d=0;d<3;d++){
			t_origin[i][d]=mesh.vertices[ f->vertices[i] ].pos[d]-mesh.vertices[ f->vertices[0] ].pos[d];
			if(t_origin[i][d]>box.hL[d]){
				t_origin[i][d]-=box.L[d];
			}else if(t_origin[i][d]<-box.hL[d]){
				t_origin[i][d]+=box.L[d];
			}
		}
	}

	// 	printf("t_origin[0]: %lf %lf %lf\n",t_origin[0][0],t_origin[0][1],t_origin[0][2]);
	// 	printf("t_origin[1]: %lf %lf %lf\n",t_origin[1][0],t_origin[1][1],t_origin[1][2]);
	// 	printf("t_origin[2]: %lf %lf %lf\n\n",t_origin[2][0],t_origin[2][1],t_origin[2][2]);

	/* Rotate vertices into xy system */
	// 	mvinprod(f->t_xy[0],f->rotmatrix_xy,t_origin[0]); // No use rotating (0,0,0) around (0,0,0)
	mvinprod(f->t_xy[1],f->rotmatrix_xy,t_origin[1]);
	mvinprod(f->t_xy[2],f->rotmatrix_xy,t_origin[2]);


	/* The face is now in the xy-plane, so all vertices have same z */
	// 	if( fabs(f->t_xy[0][2]-f->t_xy[1][2])>1E-4 ){
	// 		printf("z not equal: %f vs %f!\n",f->t_xy[0][2],f->t_xy[1][2]);
	// 	}
	// 	if( fabs(f->t_xy[0][2]-f->t_xy[2][2])>1E-4 ){
	// 		printf("z not equal: %f vs %f!\n",f->t_xy[0][2],f->t_xy[2][2]);
	// 	}

	// 	printf("f->t_xy[0]: %lf %lf %lf\n",f->t_xy[0][0],f->t_xy[0][1],f->t_xy[0][2]);
	// 	printf("f->t_xy[1]: %lf %lf %lf\n",f->t_xy[1][0],f->t_xy[1][1],f->t_xy[1][2]);
	// 	printf("f->t_xy[2]: %lf %lf %lf\n\n",f->t_xy[2][0],f->t_xy[2][1],f->t_xy[2][2]);

	/* Calculate the edges to check for face distance check */
	// 	for(i=0;i<3;i++){
	// 		j=(i+1>2)?0:i+1;
	// 		f->edge[i][0]=f->t_xy[i][0]-f->t_xy[j][0];
	// 		f->edge[i][1]=f->t_xy[i][1]-f->t_xy[j][1];
	// 		//printf("i j f->edge: %d %d (%f %f)\n",i,j,f->edge[i][0],f->edge[i][1]);
	// 	}

	/* Get linear transformation matrix T */
	//double T[2][2]={ {t_xy[0][0]-t_xy[2][0],t_xy[1][0]-t_xy[2][0]} , {t_xy[0][1]-t_xy[2][1],t_xy[1][1]-t_xy[2][1]} };
	//double T[2][2]={ {f->edge[1][0],f->edge[2][0]} , {f->edge[1][1],f->edge[2][1]} };
	f->T[0][0]=f->t_xy[0][0]-f->t_xy[2][0];
	f->T[0][1]=f->t_xy[1][0]-f->t_xy[2][0];
	f->T[1][0]=f->t_xy[0][1]-f->t_xy[2][1];
	f->T[1][1]=f->t_xy[1][1]-f->t_xy[2][1];
	// 	printf("T: ((%f %f)\n   (%f,%f))\n",f->T[0][0],f->T[0][1],f->T[0][1],f->T[1][1]);
	// 	f->T[0][0]=-f->edge[2][0];
	// 	f->T[0][1]=f->edge[1][0];
	// 	f->T[1][0]=-f->edge[2][1];
	// 	f->T[1][1]=f->edge[1][1];

	// 	printf("det T: %f\n",det2D(f->T));
	f->detTinv=1./det2D(f->T);

	return;
}
/* --------- Edge equation which calculates whether a point is left or right of a line ----------
 * Takes three points as input. p1 and p2 define a line, and p defines a point of which we need to
 * know whether it is left of, right of, or on the line.
 * Source:
 * http://people.csail.mit.edu/ericchan/bib/pdf/p17-pineda.pdf
 */
double edgeeq(double p1[3], double dp[2], double p[3]){
	printf("p1: %lf %lf %lf\n",p1[0],p1[1],p1[2]);
	printf("dp: %lf %lf \n",dp[0],dp[1]);
	printf("p: %lf %lf %lf\n",p[0],p[1],p[2]);
	return (p[0]-p1[1])*dp[1]-(p[1]-p1[0])*dp[0];
}
/* --------- Calculates the barycentric coordinates of a point with a triangle ----------
 * http://gamedev.stackexchange.com/questions/23743/whats-the-most-efficient-way-to-find-barycentric-coordinates
 * ^source, sorta.
 * Input:
 *  p       coordinates of a point in the plane of the triangle
 *  t       array of vertices positions
 * Output:
 *  bary    array of barycentric coordinates
 */
void getbarycentriccoords_fast(double p_xy[3], tface *f, double bary[3]){
	int d;
	double vec_p_min_v3[3];

	/* Get nearest image of point to vertex distance */
	for(d=0;d<2;d++){
		vec_p_min_v3[d]=p_xy[d]-f->t_xy[2][d];
	}

	/* Most stuff is cached with face data, so computation is fast */
	bary[0]=( f->T[1][1]*vec_p_min_v3[0]-f->T[0][1]*vec_p_min_v3[1])*f->detTinv;
	bary[1]=(-f->T[1][0]*vec_p_min_v3[0]+f->T[0][0]*vec_p_min_v3[1])*f->detTinv;
	bary[2]=1-bary[0]-bary[1];

	return;
}
/* --------- Checks whether a point falls within a face (2D!) ----------
 *
 */
int is_point_inside_xy_face_fast(double p_xy[3], tface *f, double barycoords[3]){
	int d;

	/* Calculate barycentric coordinates */
	getbarycentriccoords_fast(p_xy,f,barycoords);
	//printf("f barycoords %lf %lf %lf\n\n",barycoords[0],barycoords[1],barycoords[2]);

	/* Check the coordinates to see whether point falls in triangle */
	for(d=0;d<3;d++){
		if(barycoords[d]<0 || barycoords[d]>1){return 0;}
	}
	return 1;
}
/* --------- Projects a vector v onto a plane tangent to a sphere ----------
 * This function takes a vector r and a vector v, and projects the vector v onto the plane
 * tangent to a sphere with center at (0,0,0) and at a position r.
 */
void project_tangent_to_sphere(double nv[3], double r[3], double v[3]){
	int d;
	double rhat[3];

	direction(rhat,r);

	for(d=0;d<3;d++){
		nv[d]=v[d]-rhat[d]*inprod(rhat,v);
	}
}
/* --------- Projects a vector v normal to a sphere ----------
 * This function takes a vector r and a vector v, and projects the vector v normal to a sphere with
 * its center at (0,0,0) and at a position r.
 */
void project_normal_to_sphere(double *val, double r[3], double v[3]){
	double rhat[3];

	direction(rhat,r);

	*val=inprod(rhat,v);
}

/*========================== Geometric distance functions ==========================*/

/* --------- Calculates the shortest distance between two lines ---------
 * Based on the code:
 * http://www.sklogwiki.org/SklogWiki/index.php/Rev._source_code_for_the_minimum_distance_between_two_rods_in_C
 * which comes from this paper by Carlos Vega: DOI: 10.1016/0097-8485(94)80023-5
 * Modified to also return the two points on the rods where the force is exerted, which are the
 * lever arms required for the calculation of the torques.
 *
 * Input:
 * 	-	CMdistvec12			vector giving the center of mass distance
 * 	-	hl1, hl2			half the length of the lines
 * 	-	or1, or2			orientations of the lines
 * Output:
 * 	-	distsq12			square of the shortest distance between the two lines
 * 	-	lambda1,lambda2		lambda * or gives the point on the line closest to the other line
 */
void calc_dist_line_line(double CMdistvec12[DIM], double hl1, double hl2, double or1[DIM],
						double or2[DIM], double *distsq12, double *lambda1, double *lambda2){
	double rr,ru1,ru2,u1u2,cc,la1,la2;

	// r_ij.r_ij
	rr=inprod(CMdistvec12,CMdistvec12);
	// r_ij.u_i
	ru1=inprod(CMdistvec12,or1);
	// r_ij.u_j
	ru2=inprod(CMdistvec12,or2);
	// u_i.u_j
	u1u2=inprod(or1,or2);
	// alignment factor
	cc=1-u1u2*u1u2;
	// Checking whether the rods are or not parallel:
	// The original code is modified to have symmetry:
	if(cc<1e-10){
		if(ru1 && ru2){
			la1=ru1/2;
			la2=-ru2/2;
		}else{
			*lambda1=0.;
			*lambda2=0.;
			*distsq12=rr;
			return;
		}
	}else{
		// Step 1
		la1=(ru1-u1u2*ru2)/cc;
		la2=(-ru2+u1u2*ru1)/cc;
	}
	// 	printf("la mu: %lf %lf\n",la1,la2);
	// Step 2
	if(fabs(la1)>hl1 || fabs(la2)>hl2){
		// Step 3 - 7
		if(fabs(la1)-hl1>fabs(la2)-hl2){
			la1=samesign(hl1,la1);
			la2=la1*u1u2-ru2;
			if(fabs(la2)>hl2){
	// 				printf("la2>hl2\n");
				la2=samesign(hl2,la2);
			}
		}else{
	// 			printf("la mu: %lf %lf\n",la1,la2);
			la2=samesign(hl2,la2);
			la1=la2*u1u2+ru1;
	// 			printf("la mu: %lf %lf\n",la1,la2);
			if(fabs(la1)>hl1){
	// 				printf("la1>hl1\n");
				la1=samesign(hl1,la1);
			}
	// 			printf("la mu: %lf %lf\n\n",la1,la2);
		}
	}

	// Step 8
	// 	printf("la mu: %lf %lf\n",la1,la2);

	/*if(FORCETROUBLE){
		printf("r1={%lf,%lf,%lf}\n",pi->pos[0],pi->pos[1],pi->pos[2]);
		printf("r2={%lf,%lf,%lf}\n",pj->pos[0],pj->pos[1],pj->pos[2]);
		printf("box: {%lf,%lf,%lf}\n",box.L[0],box.L[1],box.L[2]);
		printf("r12={%lf,%lf,%lf}\n",CMdistvec12[0],CMdistvec12[1],CMdistvec12[2]);
		printf("n1={%lf,%lf,%lf}\n",or1[0],or1[1],or1[2]);
		printf("n2={%lf,%lf,%lf}\n",or2[0],or2[1],or2[2]);
		printf("la mu: %lf %lf\n",la1,la2);
		printf("rmin %lf\n\n",sqrt(rr+la1*la1+la2*la2+2*(la2*ru2-la1*(ru1+la2*u1u2))));
	}*/
	*lambda1=la1;
	*lambda2=la2;
	*distsq12=rr+la1*la1+la2*la2+2*(la2*ru2-la1*(ru1+la2*u1u2));
	return;
}
/* --------- Calculates the minimum distance between a point and a plane ----------
 * http://paulbourke.net/geometry/pointlineplane/
 */
void calc_dist_point_plane(double point[3], double planep[3], double n[3], double distvec[3]){
	int d;
	double dist;
	double D=-planep[0]*n[0]-planep[1]*n[1]-planep[2]*n[2];
	//printf("pp: %lf %lf %lf\n",planep[0],planep[1],planep[2]);
	//printf("n:  %lf %lf %lf\n",n[0],n[1],n[2]);
	//printf("D:  %lf\n",D);
	dist=point[0]*n[0]+point[1]*n[1]+point[2]*n[2]+D;
	//*distsq=dist*dist;
	for(d=0;d<3;d++){distvec[d]=dist*n[d];}
	return;
}

/*========================== Random number generation ==========================*/

/* --------- Initialize random number generator ---------
 * Seeds the Marsenne Twister random number generator.
 */
void init_random_seed(void){
	unsigned long seed = time(NULL);
	init_genrand(seed);

	// Save the seed to a file for later reproducability
	char file_name[255];
	sprintf(file_name,"Random_seed.txt");
	FILE* seed_file = fopen(file_name,"w");
	fprintf(seed_file,"%lu",seed);
	printf("Seeded Marsenne Twister RNG with seed %lu \n",seed);
	fclose(seed_file);

	return;
}
/* --------- Generates a random number from a gaussian distribution ---------
 * Using the polar Box-Muller transform, we map two [-1,1] numbers from a uniform random distribution
 * onto a (unbounded) normal distribution. Since we only need one output number per function call, we
 * store the second generated number for the next function call.
 * For more info: https://en.wikipedia.org/wiki/Box-Muller_transform
 */
double gaussianrand(void){
	double v1,v2,rsq,fac;
	if(storedval==0.){
		do{
			v1=2*(genrand()-0.5);
			v2=2*(genrand()-0.5);
			rsq=v1*v1+v2*v2;
		}while(rsq>=1.0 || rsq==0.0);
		fac=sqrt(-2.0*log(rsq)/rsq);
		storedval=v1*fac;
		return v2*fac;
	}else{
		fac=storedval;
		storedval=0.0;
		return fac;
	}
}
/* --------- Generates a random (DIM-dimensional, norm<1) vector ---------
 * Uniformly generates points or vector in DIM-dimensional space.
 */
void fillrvec(double v[DIM]){
	int d;
	double lv2=2;
	while(lv2>1.){
		lv2=0.;
		for(d=0;d<DIM;d++){
			v[d]=2.*(genrand()-0.5);
			lv2+=(v[d]*v[d]);
		}
	}
	//printf("normsq %lf v %lf %lf %lf\n",lv2,v[0],v[1],v[2]);
	return;
}

/*========================== Scalar histogram functions ==========================*/

/* --------- Resets an observable to zero measurements ---------
 *
 */
void clear_observable(tobservable *o){
	o->samples=0;
	o->current=0.;
	o->mean=0.;
	o->prev_mean=0.;
	o->mean_sq=0.;
	o->mvariance=0.;
}
/* --------- Updates an observable after its current value is changed ---------
 * For expressions of incremental averages and variance, see
 * http://people.ds.cam.ac.uk/fanf2/hermes/doc/antiforgery/stats.pdf
 * or just google it.
 */
void update_observable(tobservable *o){
	o->prev_mean = o->mean;
	o->samples++;
	o->mean    += (1./o->samples) * (o->current - o->mean);
	o->mean_sq += (1./o->samples) * (o->current*o->current - o->mean_sq);
	if(o->samples > 1){
		o->mvariance += (o->current - o->prev_mean) * (o->current - o->mean);
	}
	// 	printf("    current value: %lf\n",o->current);
	// 	printf("       mean value: %lf\n",o->mean);
	// 	printf("  prev_mean value: %lf\n",o->prev_mean);
	// 	printf("    mean_sq value: %lf\n",o->mean_sq);
	// 	printf("  mvariance value: %lf\n",o->mvariance);
	// 	printf("     # of samples: %ld\n",o->samples);
}
/* --------- Save an observable to a file ---------
 *
 */
void save_observable(char* filename, tobservable *o){
	FILE* output;
	// Make the file if it doesn't exist yet, else append into existing file
	if(o->n_saved_to_file == 0){
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"a");
	}
	// Write the observable's data to the file
	fprintf(output,"%.012e %.012e %ld\n",
		o->mean,
		o->mvariance / o->samples,
		o->samples
	);
	o->n_saved_to_file++;
	fclose(output);
}
/* --------- Save an observable to a file with a timestamp ---------
 *
 */
void save_observable_with_time(char* filename, tobservable *o, double time){
	FILE* output;
	// Make the file if it doesn't exist yet, else append into existing file
	if(o->n_saved_to_file == 0){
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"a");
	}
	// Write the observable's data to the file
	fprintf(output,"%e %.012e %.012e %ld\n",
		time,
		o->mean,
		o->mvariance / o->samples,
		o->samples
	);
	o->n_saved_to_file++;
	fclose(output);
}
/* --------- Resets an element to zero measurements ---------
 *
 */
void clear_element(thistogram *h, long int element){
	clear_observable( &(h->value[element]) );
}
/* --------- Updates a histogram element after its current value is changed ---------
 * Takes a pointer to the histogram to update and the integer index of the bin to update.
 * Really simple function. Only really exists so that notation is consistent with array
 * histogram functions.
 */
void update_element(thistogram *h, long int element){
	update_observable( &(h->value[element]) );
}
/* --------- Resets a histogram to zero measurements ---------
 *
 */
void clear_histogram(thistogram *h){
	long int b;
	for(b=0;b<h->n_bins;b++){
		//h->xvalue[b] = 0.;
		clear_observable( &(h->value[b]) );
	}
}
/* --------- Creates a histogram and allocates memory ---------
 *
 */
void create_histogram(thistogram *h, long int n_bins){
	h->n_bins = n_bins;
	h->xvalue = calloc(h->n_bins,sizeof(*h->xvalue));
	h->value  = calloc(h->n_bins,sizeof(*h->value));
	clear_histogram( h );
}
/* --------- Removes a histogram and frees memory ---------
 *
 */
void destroy_histogram(thistogram *h){
	free( h->value );
	free( h->xvalue );
}
/* --------- Resizes a histogram to a new size ---------
 *
 */
void grow_histogram(thistogram *h, long int new_n_bins){
	long int i,old_n_bins=h->n_bins;
	if(new_n_bins<old_n_bins){
		printf("Error: new # bins < current # bins for grow_histogram. Exiting.\n");
		exit(42);
	}
	h->n_bins = new_n_bins;
	h->xvalue = realloc(h->xvalue,h->n_bins*sizeof(*h->xvalue));
	h->value  = realloc(h->value,h->n_bins*sizeof(*h->value));
	/* Safety initialize */
	for(i=old_n_bins;i<new_n_bins;i++){
		h->xvalue[i]=0.;
		memset(&h->value[i],0,sizeof(h->value[i]));
	}
}
/* --------- Updates a histogram after its values are changed ---------
 *
 */
void update_histogram(thistogram *h){
	long int b;
	for(b=0; b < h->n_bins; b++){
		update_observable( &(h->value[b]) );
	}
}
/* --------- Saves a histogram to a file ---------
 *
 */
void save_histogram(char* filename, thistogram *h){
	FILE* output;
	char fullfilename[256];
	strcpy(fullfilename, filename);
	char endstring[10];
	sprintf(endstring,"_0.dat");
	strcat(fullfilename, endstring);
	output=fopen(fullfilename,"w");
	// Write the histogram's data to the file
	for(int i = 0; i < h->n_bins; i++){
		fprintf(output,"%.012e %.012e %.012e %ld\n",
			h->xvalue[i],
			h->value[i].mean,
			h->value[i].mvariance / h->value[i].samples,
			h->value[i].samples
		);
	}
	h->n_saved_to_file++;
	fclose(output);
}

/*========================== Array histogram functions ==========================*/

/* --------- Updates an array histogram element after its current value is changed ---------
 * Takes a pointer to the array histogram of which an element is to be updated and the integer
 * index of the element that has to be updated. It then updates all observables in the element
 * array.
 * Schematic representation:
 *
 *      array_histogram h
 * ____________________________
 * |                          |
 * |    element               |
 * |  __________              |
 * |  |        |              |
 * v  v        v              v
 * [  {o1,o2,o3} , {o1,o2,o3} ]
 *     ^  ^  ^
 * these get updated
 */
void array_hist_update_element(t_array_histogram *h, long int element){
	int i;
	for(i=0;i<h->val_size;i++){
		update_observable( &(h->value[element][i]) );
	}
}
/* --------- Updates an array histogram element after its current value is changed ---------
 * Takes a pointer to the array histogram of which an element is to be updated and the integer
 * index of the element that has to be updated. It then updates all observables in the element
 * array.
 * Schematic representation:
 *
 *      array_histogram h
 * ____________________________
 * |                          |
 * |    element               |
 * |  __________              |
 * |  |        |              |
 * v  v        v              v
 * [  {o1,o2,o3} , {o1,o2,o3} ]
 *     ^  ^  ^
 * these get cleared
 */
void array_hist_clear_element(t_array_histogram *h, long int element){
	int i;
	for(i=0;i<h->val_size;i++){
		clear_observable( &(h->value[element][i]) );
	}
}
/* --------- Updates statistics of all observables in an array histogram ---------
 *
 */
void array_hist_update_all(t_array_histogram *h){
	long int b;
	for(b=0;b<h->n_bins;b++){
		array_hist_update_element(h,b);
	}
}
/* --------- Resets an array histogram to zero measurements ---------
 *
 */
void array_hist_reset(t_array_histogram *h){
	long int b;
	for(b=0;b<h->n_bins;b++){
		//h->xvalue[b] = 0.;
		array_hist_clear_element(h,b);
	}
}
/* --------- Creates a histogram with array elements and allocates memory ---------
 * Each bin of this histogram has a scalar index, and contains an array of array_size observables.
 */
void array_hist_create(t_array_histogram *h, long int n_bins, int xval_size, int val_size){
	int i;
	h->n_bins = n_bins;
	h->xval_size = xval_size;
	h->xvalue = calloc(h->n_bins,sizeof(*h->xvalue));
	h->val_size = val_size;
	h->value  = calloc(h->n_bins,sizeof(*h->value));
	for(i=0;i<h->n_bins;i++){
		h->xvalue[i] = calloc(xval_size,sizeof(*h->xvalue[i]));
		h->value[i]  = calloc(val_size,sizeof(*h->value[i]));
	}
	array_hist_reset( h );
}
/* --------- Removes an array histogram and frees memory ---------
 *
 */
void array_hist_destroy(t_array_histogram *h){
	int i;
	free( h->xvalue );
	for(i=0;i<h->n_bins;i++){ free(h->value[i]); }
	free( h->value );
}
/* --------- Sets the current value of all observables to 0, but leave the rest ---------
 * This is primarily used if we want to incrementally calculate bin values. We set the
 * current value of each observable to zero, but leave all statistical values untouched.
 */
void array_hist_clear_current(t_array_histogram *h){
	long int b;
	int i;
	//printf("Gonna clear %ld elements of %d size\n",h->n_bins,h->val_size);
	for(b=0;b<h->n_bins;b++){
		for(i=0;i<h->val_size;i++){
			h->value[b][i].current = 0.;
		}
	}
}
/* --------- Resizes an array histogram to a new size ---------
 *
 */
void array_hist_grow(t_array_histogram *h, long int new_n_bins){
	long int b,old_n_bins=h->n_bins;
	int i;
	if(new_n_bins<old_n_bins){
		printf("Error: new # bins < current # bins for grow_histogram. Exiting.\n");
		exit(42);
	}
	h->n_bins = new_n_bins;
	h->xvalue = realloc(h->xvalue,h->n_bins*sizeof(*h->xvalue));
	h->value  = realloc(h->value,h->n_bins*sizeof(*h->value));
	/* Allocate memory for elements */
	for(b=old_n_bins;b<new_n_bins;b++){
		h->value[b] = calloc(h->val_size,sizeof(*h->value[b]));
	}
	/* Safety initialize */
	for(b=old_n_bins;b<new_n_bins;b++){
		for(i=0;i<h->val_size;i++){
			h->xvalue[b][i]=0.;
		}
		for(i=0;i<h->val_size;i++){
			clear_observable( &(h->value[b][i]) );
			//memset(&h->value[b][i],0,sizeof(h->value[b][i]));
		}
	}
}
/* --------- Saves a histogram to a file ---------
 *
 */
void array_hist_save(t_array_histogram *h, char *file_name){
	long int b;
	int i;
	static int save_count=0;
	static char full_file_name[256];
	static char end_string[16];
	FILE* output;
	if(save_count==0){
		sprintf(end_string,"_%04d.dat",run);
		strcpy(full_file_name,file_name);
		strcat(full_file_name,end_string);
	}
	output=fopen(full_file_name,"w");
	/* Write stuff */
	for(b=0;b<h->n_bins;b++){
		for(i=0;i<h->xval_size;i++){
			fprintf(output,"%12.012lf ",h->xvalue[b][i]);
		}
		for(i=0;i<h->val_size;i++){
			fprintf(output,"%12.012lf %12.012lf\n",
				h->value[b][i].mean,
				h->value[b][i].mvariance/h->value[b][i].samples
			);
		}
	}
	save_count++;
	fclose(output);
}

/*========================== Helper functions ==========================*/

/* --------- Skips headers in input snapshot file ---------
 * Our snapshot files can contain headers, marked by a '#' as the first character in the line.
 * These headers provide some text with details about the run the snapshots are from. But as they
 * don't contain any data, we don't want to actually read them in most of the time. This function
 * moves a file pointer to the first (next encountered) line that does not start with a '#',
 * skipping the headers.
 */
void skipheaders(FILE *f){
	char line[2000];
	int sol,eol; // start of line, end of line
	while(1){
		sol=ftell(f);
		if(!fgets(line,sizeof(line),f)){printf("Error while reading header.\n");}
		eol=ftell(f);
		if(line[0]!='#'){break;} // Stop at first non-comment line
	}
	fseek(f,sol-eol,SEEK_CUR); // and rewind file pointer to start of that line
	return;
}
/* --------- Prints a short documentation of the program ---------
 *
 */
void printhelp(void){
	printf("\n Brownian Dynamics + activity & confinement \n\n\
	This program does Brownian dynamics for passive or active spherocylinders, with the         \n\
	possibility of confining them to some arbitrary geometry that is defined either explicitly, \n\
	for geometrically simple configurations such as a sphere or a cylinder, or as a mesh of     \n\
	vertices and faces for more complicated geometries.                                       \n\n\
	The input options for this program are:                                                     \n\
	-b FILENAME      :  loads in the file FILENAME as the confining geometry.                   \n\
	-d               :  enables mesh dynamics: makes the mesh mobile and deformable             \n\
	-p FILENAME x    :  loads in simulation parameters from line x in the text file FILENAME.   \n\
	-cl              :  enables crosslinks between the spherocylinders that mimic biological    \n\
	                    active or passive myosin motor proteins.                                \n\
	-sphereparticles :  all particles are treated explicitly as spheres.                        \n\
	-onsph           :  constrains all particle movement onto the surface of a sphere.          \n\
	-insph           :  constrains all particle movement to the inside of a sphere.             \n\
	-cylinder x      :  constrains all particle movement to the inside of a cylinder of         \n\
	                    width x particle diameters. If no x is given, it uses a default number. \n\
	-longbox x       :  elongates the simulation volume, which promotes the formation of        \n\
	                    interfaces along the short plane if any want to form. Default x is 5.   \n\
	-quasi2D x       :  constrains particle movement between two walls which are separated by   \n\
	                    x particle diameters.                                                   \n\
	-load FILENAME   :  loads in the file FILENAME as the initial configuration.                \n\
	-lod x           :  sets the aspect ratio of rods to x.                                     \n\
	-N x             :  sets number of particles to x.                                          \n\
	-packfrac x      :  sets the packing fraction to x.                                         \n\
	-                :                                                                          \n\
	-                :                                                                          \n\
	\n");
}
/* --------- Calculate which cell position {x,y,z} falls into ---------
 * Takes the positions x,y,z and returns the number of the cell that position
 * corresponds to.
 */
int coords2cell(double x, double y, double z, int nx, int ny, int nz){
	long int i= ((long int)(nx*(x+box.hL[0])*box.iL[0]) % nx)*ny*nz +
				((long int)(ny*(y+box.hL[1])*box.iL[1]) % ny)*nz +
				((long int)(nz*(z+box.hL[2])*box.iL[2]) % nz);
	return i;
}
/* --------- Put particle in the correct cell ---------
 * First calculates the cell that corresponds to the particle's current position and checks whether
 * this is a valid number. Next, if the cell number doesn't match that of its position, places the
 * particle in the right cell, adjusting the cell list as needed.
 */
void move2cell(long int p){
	long int j,i  = coords2cell(part[p].pos[0],part[p].pos[1],part[p].pos[2],nx,ny,nz);
	long int iold = part[p].cell;
	if(i<0 || i>=n_cells){
		printf("Error: attempting to place particle in cell outside of box!\n");
		printf(" particle number: %ld\n",p);
		printf(" particle coords: %f %f %f\n",part[p].pos[0],part[p].pos[1],part[p].pos[2]);
		printf(" box lengths    : %f %f %f\n",box.hL[0],box.hL[1],box.hL[2]);
		/*save_recent();*/
		exit(666);
	}
	if(i!=iold){ // If not the same cell
		// Find particle in old cell, remove it and fill the hole in the array by moving the rest down
		for(j=cells[iold].n-1; j>=0; j--){
			if((cells[iold].particles[j]) == p){
				cells[iold].particles[j] = cells[iold].particles[ cells[iold].n-1 ];
				break;
			}
		}
		cells[iold].n--;
		// Check whether adding the particle to the new cell exceeds the limit
		if(cells[i].n+1 > MAX_PART_CELL){
			printf("Too many particles in a cell! Either a bug or MAX_PART_CELL is set too low. Exiting...\n");
			exit(42);
		}
	// 		if(cells[iold].n!=0){ // Gotta prevent realloc to zero size, since that is undefined behaviour!
	// 			cells[iold].particles = realloc(cells[iold].particles,cells[iold].n*sizeof(*cells[iold].particles));
	// 		}
	// 		cells[i].particles = realloc(cells[i].particles,(cells[i].n+1)*sizeof(*cells[i].particles));
		// Add the particle to the new cell
		cells[i].particles[ cells[i].n ] = p;
		cells[i].n++;
		part[p].cell = i;
	}
}
/* --------- Put particle in the correct cell ---------
 * First calculates the cell that corresponds to the particle's current position and checks whether
 * this is a valid number. Next, if the cell number doesn't match that of its position, places the
 * particle in the right cell, adjusting the cell list as needed.
 */
void move2cell_wall(long int p){
	long int j,i=coords2cell(part[p].pos[0],part[p].pos[1],part[p].pos[2],nx_w,ny_w,nz_w);
	long int iold=part[p].cell_wall;
	if(i<0 || i>=n_cells_wall){
		printf("Error: wall cell out of box?\n");/*save_recent();*/
		printf(" particle number: %ld\n",p);
		printf(" particle coords: %f %f %f\n",part[p].pos[0],part[p].pos[1],part[p].pos[2]);
		exit(666);
	}
	/* If new cell is not the same as old cell a.k.a. whether we have to change anything */
	if(i!=iold){
		/* Move last element to old cell list position of particle p */
		for(j=wallcells[iold].np-1;j>=0;j--){
			if((wallcells[iold].particles[j])==p){
				wallcells[iold].particles[j]=wallcells[iold].particles[ wallcells[iold].np-1 ];
				break;
			}
		}
		/* Update and resize old cell */
		wallcells[iold].np--;
		if(wallcells[iold].np!=0){ // Gotta prevent realloc to zero size, since that is undefined behaviour!
			wallcells[iold].particles = realloc(wallcells[iold].particles,wallcells[iold].np*sizeof(*wallcells[iold].particles));
		}
		/* Update and resize new cell */
		wallcells[i].particles = realloc(wallcells[i].particles,(wallcells[i].np+1)*sizeof(*wallcells[i].particles));
		wallcells[i].particles[ wallcells[i].np ] = p;
		wallcells[i].np++;
		/* Update cell of particle in particle data */
		part[p].cell_wall = i;
	}
}
/* --------- Put vertex in the correct cell ---------
 * First calculates the cell that corresponds to the vertex's current position and checks whether
 * this is a valid number. Next, if the cell number doesn't match that of its position, places the
 * vertex in the right cell, adjusting the cell list as needed.
 */
void move_vertex_to_wall_cell(long int v){
	long int j,i=coords2cell(mesh.vertices[v].pos[0],mesh.vertices[v].pos[1],mesh.vertices[v].pos[2],nx_w,ny_w,nz_w);
	long int iold=mesh.vertices[v].cell;

	if(i<0 || i>=n_cells_wall){
		printf("Error: wall cell for vertex out of box?\n");/*save_timeseries_sparse_mesh();*/
		printf(" vertex number: %ld\n",v);
		printf(" vertex coords: %f %f %f\n",mesh.vertices[v].pos[0],mesh.vertices[v].pos[1],mesh.vertices[v].pos[2]);
		exit(666);
	}
	/* If new cell is not the same as old cell a.k.a. whether we have to change anything */
	if(i!=iold){
		/* Move last element to old cell list position of vertex v */
		for(j=wallcells[iold].nv-1;j>=0;j--){
			if(wallcells[iold].vertices[j]==v){
				wallcells[iold].vertices[j]=wallcells[iold].vertices[ wallcells[iold].nv-1 ];
				break;
			}
		}
		/* Update and resize old cell */
		wallcells[iold].nv--;
		if(wallcells[iold].nv!=0){ // Gotta prevent realloc to zero size, since that is undefined behaviour!
			wallcells[iold].vertices = realloc(wallcells[iold].vertices,wallcells[iold].nv*sizeof(*wallcells[iold].vertices));
		}
		/* Update and resize new cell */
		wallcells[i].vertices = realloc(wallcells[i].vertices,(wallcells[i].nv+1)*sizeof(*wallcells[i].vertices));
		wallcells[i].vertices[ wallcells[i].nv ] = v;
		wallcells[i].nv++;
		/* Update cell of particle in particle data */
		mesh.vertices[v].cell = i;
	}
}
/* --------- Put edge in the correct cell ---------
 * First calculates the cell that corresponds to the vertex's current position and checks whether
 * this is a valid number. Next, if the cell number doesn't match that of its position, places the
 * vertex in the right cell, adjusting the cell list as needed.
 */
void move_edge_to_wall_cell(long int e){
	long int j,i=coords2cell(mesh.edges[e].centerpos[0],mesh.edges[e].centerpos[1],mesh.edges[e].centerpos[2],nx_w,ny_w,nz_w);
	long int iold=mesh.edges[e].cell;
	if(i<0 || i>=n_cells_wall){
		printf("Error: wall cell for edge out of box?\n");/*save_timeseries_sparse_mesh();*/
		printf(" edge number: %ld\n",e);
		printf(" edge center coords: %f %f %f\n",mesh.edges[e].centerpos[0],mesh.edges[e].centerpos[1],mesh.edges[e].centerpos[2]);
		exit(666);
	}
	/* If new cell is not the same as old cell a.k.a. whether we have to change anything */
	if(i!=iold){
		/* Move last element to old cell list position of edge e */
		for(j=wallcells[iold].ne-1;j>=0;j--){
			if((wallcells[iold].edges[j])==e){
				wallcells[iold].edges[j]=wallcells[iold].edges[ wallcells[iold].ne-1 ];
				break;
			}
		}
		/* Update and resize old cell */
		wallcells[iold].ne--;
		if(wallcells[iold].ne!=0){ // Gotta prevent realloc to zero size, since that is undefined behaviour!
			wallcells[iold].edges = realloc(wallcells[iold].edges,wallcells[iold].ne*sizeof(*wallcells[iold].edges));
		}
		/* Update and resize new cell */
		wallcells[i].edges = realloc(wallcells[i].edges,(wallcells[i].ne+1)*sizeof(*wallcells[i].edges));
		wallcells[i].edges[ wallcells[i].ne ] = e;
		wallcells[i].ne++;
		/* Update cell of particle in particle data */
		mesh.edges[e].cell = i;
	}
}
/* --------- Put face in the correct cell ---------
 * First calculates the cell that corresponds to the vertex's current position and checks whether
 * this is a valid number. Next, if the cell number doesn't match that of its position, places the
 * vertex in the right cell, adjusting the cell list as needed.
 */
void move_face_to_wall_cell(long int f){
	long int j,i=coords2cell(mesh.faces[f].centerpos[0],mesh.faces[f].centerpos[1],mesh.faces[f].centerpos[2],nx_w,ny_w,nz_w);
	long int iold=mesh.faces[f].cell;
	if(i<0 || i>=n_cells_wall){
		printf("Error: wall cell for vertex out of box?\n");/*save_timeseries_sparse_mesh();*/
		printf(" face number: %ld\n",f);
		printf(" face center coords: %f %f %f\n",mesh.faces[f].centerpos[0],mesh.faces[f].centerpos[1],mesh.faces[f].centerpos[2]);
		exit(666);
	}
	/* If new cell is not the same as old cell a.k.a. whether we have to change anything */
	if(i!=iold){
		/* Move last element to old cell list position of face f */
		for(j=wallcells[iold].nf-1;j>=0;j--){
			if((wallcells[iold].faces[j])==f){
				wallcells[iold].faces[j]=wallcells[iold].faces[ wallcells[iold].nf-1 ];
				break;
			}
		}
		/* Update and resize old cell */
		wallcells[iold].nf--;
		if(wallcells[iold].nf!=0){ // Gotta prevent realloc to zero size, since that is undefined behaviour!
			wallcells[iold].faces = realloc(wallcells[iold].faces,wallcells[iold].nf*sizeof(*wallcells[iold].faces));
		}
		/* Update and resize new cell */
		wallcells[i].faces = realloc(wallcells[i].faces,(wallcells[i].nf+1)*sizeof(*wallcells[i].faces));
		wallcells[i].faces[ wallcells[i].nf ] = f;
		wallcells[i].nf++;
		/* Update cell of particle in particle data */
		mesh.faces[f].cell = i;
	}
}
/* --------- Put particle in periodic box (after cell initialization) ---------
 * Applies periodic boundary conditions to the particle position i.e. if it falls outside of the
 * box, it places the particle back inside. Then checks this position with the cell list.
 */
void put_particle_back_in_box(long int p){
	while(part[p].pos[0]>box.hL[0]) part[p].pos[0]-=box.L[0];
	while(part[p].pos[0]<-box.hL[0]) part[p].pos[0]+=box.L[0];
	while(part[p].pos[1]>box.hL[1]) part[p].pos[1]-=box.L[1];
	while(part[p].pos[1]<-box.hL[1]) part[p].pos[1]+=box.L[1];
	while(part[p].pos[2]>box.hL[2]) part[p].pos[2]-=box.L[2];
	while(part[p].pos[2]<-box.hL[2]) part[p].pos[2]+=box.L[2];
	move2cell(p);
	if(HAVE_MESH){move2cell_wall(p);}
}
/* --------- Put vertex in periodic box (after cell initialization) ---------
 * Applies periodic boundary conditions to the vertex position i.e. if it falls outside of the
 * box, it places the vertex back inside. Then checks this position with the cell list.
 */
void putvertexinbox(long int v){
	while(mesh.vertices[v].pos[0]>box.hL[0]) mesh.vertices[v].pos[0]-=box.L[0];
	while(mesh.vertices[v].pos[0]<-box.hL[0]) mesh.vertices[v].pos[0]+=box.L[0];
	while(mesh.vertices[v].pos[1]>box.hL[1]) mesh.vertices[v].pos[1]-=box.L[1];
	while(mesh.vertices[v].pos[1]<-box.hL[1]) mesh.vertices[v].pos[1]+=box.L[1];
	while(mesh.vertices[v].pos[2]>box.hL[2]) mesh.vertices[v].pos[2]-=box.L[2];
	while(mesh.vertices[v].pos[2]<-box.hL[2]) mesh.vertices[v].pos[2]+=box.L[2];
	//move_vertex_to_wall_cell(v); // done in update_mesh_properties()
}

/*========================== Object distance functions ==========================*/

/* --------- Calculates the square of the minimum distance between two spherocylinders ---------
 * The shortest distance between two spherocylinders is the shortest distance between two lines.
 *
 * Input:
 * 	-	pi, pj				pointers to spherocylinder particles i and j
 * 	-	min_rij_CM			center of mass distance, usually with nearest image convention applied
 * Output:
 *	-	lambda_i,lambda_j	lambda * or is the torque arm: points of closest approach of the two rods
 * 	-	distsq_shortest		shortest distance between the rods squared
 */
double distsq_rods(tpart *pi, tpart *pj, double min_rij_CM[DIM], double *lambda_i, double *lambda_j){
	double distsq_shortest;

	calc_dist_line_line(min_rij_CM, pi->hll, pj->hll, pi->or, pj->or,
						&distsq_shortest, lambda_i, lambda_j);

	return distsq_shortest;
}
/* --------- Calculates the square omtranspose3Df the minimum distance between a rod and a sphere ---------
 * Calculates the shortest distance between a rod and a sphere. First calculates the shortest
 * distance between a point and a line, then corrects for the fact that the rods have a finite
 * length. Also returns the point on the rod that is closest to the sphere.
 * Input:
 *  - Pointer to the rod (*prod)
 *  - Pointer to the sphere (*psph)
 *  - Center-to-center distance between rod and sphere (dist_c2c)
 * Output:
 *  - Returns the closest distance between the rod and the sphere
 *  - Point on the rod that is closest to the sphere (arm)
 */
double distsq_rod_sphere(tpart *prod, tpart *psph, double dist_c2c[DIM], double arm[DIM]){
	double ru,lambda;
	int d;

	/* Calculate shortest distance between a line and a point */
	ru=inprod(dist_c2c,prod->or);
	lambda=ru;

	/* Correct for the fact that the rods are _finite_ line segments */
	if(ru>(prod->hll)){lambda=prod->hll;}else if(lambda<-(prod->hll)){lambda=-prod->hll;}

	/* Define the point on the rod closest to the sphere */
	for(d=0;d<DIM;d++){arm[d]=lambda*prod->or[d];}

	/* Return the minimum distance */
	return normsq(dist_c2c)+lambda*lambda-2.*ru*lambda;
}
/* --------- Calculates the distance squared between a rod and a vertex ---------
 *
 */
void calc_distvec_rod_vertex(tpart *p, tvertex *v, double *distsq_pv, double *lambda_p, double near_pv[DIM]){
	double lambda,rpvu;

	/* First apply nearest image convention */
	nearestimage(p->pos,v->pos,near_pv);

	/* Calculate shortest distance between finite length rod and vertex */
	rpvu=inprod(near_pv,p->or);
	lambda=rpvu;
	if(rpvu>(p->hll)){lambda=p->hll;}else if(lambda<-(p->hll)){lambda=-p->hll;}
	*distsq_pv=normsq(near_pv)+lambda*lambda-2.*rpvu*lambda;
	*lambda_p=lambda;
}
/* --------- Calculates the distance squared between a rod and an edge ---------
 *
 */
void calc_distvec_rod_edge(tpart *p, tedge *e, double *distsq_pe, double *lambda_p, double *lambda_e, double near_pe[DIM]){

	/* First apply nearest image convention to center positions */
	nearestimage(p->pos,e->centerpos,near_pe);

	/* Then calculate line-line distance */
	calc_dist_line_line(near_pe, p->hll, e->hll, p->or, e->or,
						distsq_pe, lambda_p, lambda_e);
}
/* --------- Calculates the distance vector/square between a point and the plane of a face ----------
 *
 */
void calc_distvec_point_faceplane(double p_cartesian[3], tface *f, double dvec[3], double bary[3]){
	double p_xy[3];
	int d;
	double p[3];

	/* Translate point closer to origin of box, same as face */
	for(d=0;d<3;d++){
		p[d]=p_cartesian[d]-mesh.vertices[ f->vertices[0] ].pos[d];
		if(fabs(p[d])>box.hL[d]){
			p[d]-=((p[d]>0)?box.L[d]:-box.L[d]);
		}
	}

	/* Rotate p into the xy-axis-aligned face coordinate system */
	mvinprod(p_xy,f->rotmatrix_xy,p);


	/* Check if p_xy is inside face, if so then z-dist to p_xy is min distance */
	if(is_point_inside_xy_face_fast(p_xy,f,bary)==1){
		/* Calculate distance point to plane, or... */
		//calc_dist_point_plane(p_cartesian,mesh.vertices[ f->vertices[0] ].pos,f->outwardnormal,dvec);
		//calc_dist_point_plane(p_xy,f->t_xy[0],z,dvec);
		//printf("\ndist: %f\n",norm(dvec));
		//printf("dvec: %f %f %f\n",dvec[0],dvec[1],dvec[2]);
		/* Alternatively, take z-difference and multiply by outward normal. Faster, same result. */
		double dist=f->t_xy[0][2]-p_xy[2];
		for(d=0;d<3;d++){
			dvec[d]=dist*f->outwardnormal[d];
		}
		//printf("dist: %f\n",dist);
		//printf("dvec: %f %f %f\n",dvec[0],dvec[1],dvec[2]);
		return;

	// 		if(normsq(dvec)<pow(2.,1./6.)){
	// 		if( (f == &mesh.faces[6] || f == &mesh.faces[7]) ){
	// 			printf("dist2: %f\n",norm(dvec));
	// 			printf("t_xy: %f %f %f\n     %f %f %f\n     %f %f %f\n",
	// 				t_xy[0][0],t_xy[0][1],t_xy[0][2],
	// 				t_xy[1][0],t_xy[1][1],t_xy[1][2],
	// 				t_xy[2][0],t_xy[2][1],t_xy[2][2]);
	// 			printf("p_cart: %lf %lf %lf\n",p_cartesian[0],p_cartesian[1],p_cartesian[2]);
	// 			printf("p_xy: %lf %lf %lf\n",p_xy[0],p_xy[1],p_xy[2]);
	// 			printf("nrml: %lf %lf %lf\n",f->outwardnormal[0],f->outwardnormal[1],f->outwardnormal[2]);
	// 			printf("plane dvec: %lf %lf %lf\n",dvec[0],dvec[1],dvec[2]);
	// 		}
	}else{
		/* if not in plane, shortest distance cannot be face plane, so return large number */
		for(d=0;d<3;d++){dvec[d]=1E6;}
		return;
	}
}
/* --------- Calculates the distance vector/square between a rod and a face ---------
 *
 */
void calc_distvec_rod_faceplane(tpart *p, tface *f, double rminvec[DIM], double *rmin2, int *endsign, double bary[3]){
	int d;
	double armp[DIM];	// Arm of the torque
	double bary1[3]={0.},bary2[3]={0.};

	/* Calculate distance of rod ends to plane defined by face */
	double dist1sq,dist2sq;
	double p1[DIM],p2[DIM];
	double dvec1[DIM]={0.},dvec2[DIM]={0.};
	//double rminvec[DIM];
	for(d=0;d<DIM;d++){
		armp[d]=p->hll*p->or[d];
		p1[d]=p->pos[d]+armp[d];
		p2[d]=p->pos[d]-armp[d];
	}
	//printf("p1:   (%lf,%lf,%lf)\n",p1[0],p1[1],p1[2]);
	//printf("p2:   (%lf,%lf,%lf)\n",p2[0],p2[1],p2[2]);
	//printf("\n");
	calc_distvec_point_faceplane(p1,f,dvec1,bary1);
	calc_distvec_point_faceplane(p2,f,dvec2,bary2);

	dist1sq=normsq(dvec1);
	dist2sq=normsq(dvec2);

	// 	if(inprod(dvec1,dvec2)<-1E-4 && dvec1[0]!=1E6  && dvec2[0]!=1E6){
	// 		printf("plane dvec: %lf %lf %lf\n",dvec1[0],dvec1[1],dvec1[2]);
	// 		printf("plane dvec: %lf %lf %lf\n",dvec2[0],dvec2[1],dvec2[2]);
	// 		printf("%f\n",inprod(dvec1,dvec2));
	// 		save_timeseries_sparse_mesh();
	// 		exit(1);/*rod intersects plane*/
	// 	}
	if(dist1sq<dist2sq){
		*rmin2=dist1sq;
		memcpy(bary,bary1,sizeof(bary1));
		memcpy(rminvec,dvec1,sizeof(dvec1));
		*endsign=1;
	}else{
		*rmin2=dist2sq;
		memcpy(bary,bary2,sizeof(bary2));
		memcpy(rminvec,dvec2,sizeof(dvec2));
		*endsign=-1;
	}


	// 	if( (f == &mesh.faces[6] || f == &mesh.faces[7]) && *rmin2<1000 ){
	// 		printf("dvec1: %f %f %f\n",dvec1[0],dvec1[1],dvec1[2]);
	// 		printf("dvec2: %f %f %f\n",dvec2[0],dvec2[1],dvec2[2]);
	// 		printf("dist1sq: %lf\n",dist1sq);
	// 		printf("dist2sq: %lf\n",dist2sq);
	// 		printf("rmin2: %f\n",*rmin2);
	// 	}
	//printf("rminvec: %lf %lf %lf\n",rminvec[0],rminvec[1],rminvec[2]);

	return;
}


/*========================== NOTE WIP Cube functions ==========================*/
// typedef struct {
// 	double normal[3];
// 	int index[4];
// } tsquareface;
// struct cube { // cube struct
// 	double normal[6][3];
// 	unsigned char f_idx[6][4];
// };
// void calc_distvec_point_squareface(tvertex *v, tsquareface *f, double vec[3]){
// 	// Find a rotation matrix so that face becomes {(0,0,Z),(X,0,Z),(0,Y,Z),(X,Y,Z)}
// 	double xy_rotation[3][3];
// 	setxyrotationmatrix(xy_rotation, f->normal);
// 	// Transform vertex coordinates into a frame where the face is {(0,0,Z),(X,0,Z),(0,Y,Z),(X,Y,Z)}
// 	double vt[3];
// 	vminprod(vt, v, xy_rotation);
// 	// In this frame, the distance vector is easy to calculate. It's just the
// 	// height if it's above the face, otherwise we add xy distance to the edges.
// 	vec[0] = ( x < 0.5 ? 0.0 : (x < -0.5 ? vt[0] + 0.5 : vt[0] - 0.5) );
// 	vec[0] = ( y < 0.5 ? 0.0 : (y < -0.5 ? vt[1] + 0.5 : vt[1] - 0.5) );
// 	vec[2] = vt[2] - 0.5;
// 	// Transform vector back into real space
// 	mtranspose3D(xy_rotation, xy_rotation); // Rotation matrix transpose = its inverse
// 	vminprod(vec, vec, xy_rotation);
// }
// double distsq_cubes(tpart *pi, tpart *pj, double min_rij_CM[DIM], ){
// 	// Project vertices onto center-to-center vector
//
// 	// Sort vertices by distance along this axis
//
// 	// From this sorting we might be able to discern which parts of
// 	// the cubes are closest. With this info we need to do one of two
// 	// checks: an edge-edge distance check or a vertex-face one.
//
// 	// ========= edge-edge case =========
// 	//
//
//
// 	// ========= Vertex-face case ===========
// 	// Whichever of all vertices is closest to the middle between the
// 	// two cubes is guaranteed to be the nearest point to the other cube.
//
// 	// The shortest distance is then the distance from this vertex
// 	// to the closest face of the other cube, which is the one that
// 	// has its vertices closest to the middle between the two cubes.
//
// 	// Since there are only 6 possible faces we can hardcode
// 	// the options for efficiency.
// }

/*========================== Particle number manipulation functions ==========================*/

/* --------- Removes the crosslink i and fixes the crosslink list ---------
 *
 */
void removecrosslink(long int i){
	memcpy(&crosslink[i],&crosslink[n_crosslinks-1],sizeof(tcrosslink));
	n_crosslinks--;
}
/* --------- Adds a crosslink to the system and fixes the crosslink list ---------
 * Adds a crosslink between two particles.
 */
void addcrosslink(long int i, long int j){
	crosslink[n_crosslinks].p1=&part[i];
	crosslink[n_crosslinks].p2=&part[j];
	crosslink[n_crosslinks].l1=(genrand()-0.5)*part[i].l;
	crosslink[n_crosslinks].l2=(genrand()-0.5)*part[j].l;
	n_crosslinks++;
}
/* --------- Unbinds the crosslinks for a given time step ---------
 * We assume a constant unbinding rate k0 that is independent of the crosslink extension. Then, the
 * probability of a given crosslink unbinding in the time interval dt is p=k0*dt. So we loop over
 * all crosslinks and remove the link according to this probability.
 */
void unbind_crosslinks(void){
	short int d;
	long int i;
	tcrosslink *cl;
	double pos1[DIM],pos2[DIM],lvec[DIM];
	for(i=0;i<n_crosslinks;i++){
		cl=&crosslink[i];
		/* Random unbinding */
		if(genrand()<crosslink_unbinding_prob){
			removecrosslink(i);
			continue;
		}
		/* If bond is too long */
		for(d=0;d<DIM;d++){
			pos1[d]=cl->p1->pos[d]+cl->p1->or[d]*cl->l1;
			pos2[d]=cl->p2->pos[d]+cl->p2->or[d]*cl->l2;
		}
		nearestimage(pos1,pos2,lvec);
		cl->length=norm(lvec);
		if(cl->length>5){
			removecrosslink(i);
			continue;
		}
	}
}
/* --------- Binds new crosslinks for a given time step ---------
 * For the moment, we assume a constant binding rate as long as the particles are closer than a
 * certain cutoff distance. Not correct, WIP.
 */
void bind_crosslinks(void){
	int i,j;
	double distnear,dvec[DIM],arm1[DIM],arm2[DIM];
	for(i=0;i<n_part-1;i++){
		for(j=i+1;j<n_part;j++){
			if(genrand()<crosslink_binding_prob){
				nearestimage(part[i].pos,part[j].pos,dvec);
				distnear=distsq_rods(&part[i],&part[j],dvec,arm1,arm2);
				if(distnear<5){
					addcrosslink(i,j);
				}
			}
		}
	}
}

/*========================== Particle shape change functions ==========================*/

/* --------- Elongates a rod along its + direction ---------
 *
 */
void elongate_rod_forward(tpart *p, double dl){
	int d;
	p->l+=dl;
	p->hll+=0.5*dl;
	for(d=0;d<3;d++){
		p->pos[d]+=0.5*dl*p->or[d];
	}
}
/* --------- Elongates a rod along its - direction ---------
 *
 */
void elongate_rod_backward(tpart *p, double dl){
	int d;
	p->l+=dl;
	p->hll+=0.5*dl;
	for(d=0;d<3;d++){
		p->pos[d]-=0.5*dl*p->or[d];
	}
}
/* --------- Elongates a rod along its + direction ---------
 *
 */
void polymerize_filament_plus_end(tpart *p){
	// some kind of monte carlo step
	// elongate rod with some dl (
}

/*========================== Mesh functions ==========================*/

/* --------- Updates the properties of a vertex after movement ---------
 *
 */
void update_vertex_properties(tvertex *v){
	// not needed as of yet
}
/* --------- Updates the properties of an edge after movement ---------
 *
 */
void update_edge_properties(tedge *e){
	int d;
	double normvec;
	tvertex *ptr_v1,*ptr_v2;
	ptr_v1=&mesh.vertices[ e->vertices[0] ];
	ptr_v2=&mesh.vertices[ e->vertices[1] ];
	//vecsubtract(e->vec,ptr_v2->pos,ptr_v1->pos);
	nearestimage(ptr_v1->pos,ptr_v2->pos,e->vec);
	direction(e->or,e->vec);
	normvec=norm(e->vec);
	if(normvec>max_vertex_spacing){
		max_vertex_spacing=normvec;
		max_interaction_length_rod_mesh=1.1*(part[0].hll+0.5*max_vertex_spacing+part[0].d*pow(2.,1./6.));
	}
	e->elen=normvec;
	e->hll=0.5*normvec;
	for(d=0;d<3;d++){
		e->centerpos[d]=ptr_v1->pos[d]+0.5*e->vec[d];
		if(e->centerpos[d]>box.hL[d]){ e->centerpos[d]-=box.L[d];}
		else if(e->centerpos[d]<-box.hL[d]){ e->centerpos[d]+=box.L[d];}
	}
}
/* --------- Updates the properties of a face after movement ---------
 *
 */
void update_face_properties(tface *f){
	int d;

	double *p1=mesh.vertices[ f->vertices[0] ].pos;
	double *p2=mesh.vertices[ f->vertices[1] ].pos;
	double *p3=mesh.vertices[ f->vertices[2] ].pos;

	/* Calculate outward normal */
	planenormal(f->outwardnormal,p1,p2,p3);

	/* Calculate area (based on Heron's formula */
	double l1=2*mesh.edges[ f->edges[0] ].hll;
	double l2=2*mesh.edges[ f->edges[1] ].hll;
	double l3=2*mesh.edges[ f->edges[2] ].hll;
	double s=0.5*(l1+l2+l3);
	f->area=sqrt( s*(s-l1)*(s-l2)*(s-l3) );

	/* Face center positions */
	double v12[3],v13[3];
	nearestimage(p1,p2,v12);
	nearestimage(p1,p3,v13);
	for(d=0;d<3;d++){
		f->centerpos[d]=p1[d]+(v12[d]+v13[d])/3;
		if(f->centerpos[d]>box.hL[d]){ f->centerpos[d]-=box.L[d];}
		else if(f->centerpos[d]<-box.hL[d]){ f->centerpos[d]+=box.L[d];}
	}

	/* xy projection variables */
	calc_face_helper_vars(f);
}
/* --------- Updates the properties the mesh after movement ---------
 *
 */
void update_mesh_properties(void){
	long int i;
	/* Recalculate edge properties (also recalculates max vertex distance) */
	max_vertex_spacing=0.;
	for(i=0;i<mesh.nedges;i++){
		update_edge_properties( &mesh.edges[i] );
	}

	/* Recalculate face properties */
	for(i=0;i<mesh.nfaces;i++){
		update_face_properties( &mesh.faces[i] );
	}

	/* Cell list updates */
	if(current_wallcell_size<max_interaction_length_rod_mesh){
		/* Rebuild cell list, make cells larger */
		for(i=0;i<n_cells_wall;i++){
			free(wallcells[i].particles);
			free(wallcells[i].vertices);
			free(wallcells[i].edges);
			free(wallcells[i].faces);
		}
		free(wallcells);
		init_cells_wall();
	// 	}else if(current_wallcell_size>1.2*max_interaction_length_rod_mesh){
	// 		/* Rebuild cell list, make cells smaller */
	// 		free(wallcells);
	// 		init_cells_wall();
	}else{
		/* Update the existing cell lists */
		for(i=0;i<mesh.nvertices;i++){ move_vertex_to_wall_cell(i); }
		for(i=0;i<mesh.nedges;i++){ move_edge_to_wall_cell(i); }
		for(i=0;i<mesh.nfaces;i++){ move_face_to_wall_cell(i); }
	}
}

/*========================== Force calculation functions ==========================*/

/* --------- Calculates the force of the confining wall (closest mesh object) ---------
 * Calculates the distance to all nearby mesh objects. First, selects a nearby vertex based on the
 * wall cell list. Then calculates rod-vertex distance, rod-edge distance and rod-face distance
 * for all these objects (NOTE: no shortcuts?). The distance and the arm of the torque on the rod
 * are saved for all these checks, and the forces and torques on the rod are calculated based on
 * an interaction with the closest object. If the mesh is dynamic, we also need to calculate the
 * forces on the mesh. For vertices, this is trivial: they are forces acting on a point. For edges
 * we distribute the force between both vertices with a weight according to the point of closest
 * approach to the rod along the edge line. For faces, we distribute the force between the three
 * vertices of the face according to the weights corresponding to the barycentric coordinates of
 * the point on the face closest to the rod.
 *
 * Possible shortcuts/speed gains/early exits:
 * 	-	Only calculate stuff for objects associated with the closest vertex.
 * 			Disadvantage:  fails when rod is e.g. parallel to face or when faces are small
 * 	-	Only calculate stuff for closest few vertices
 * 			Disadvantage: same as above, no guarantee that this works
 */
void force_closest_mesh_object(tpart *p){
	int i,j,d,id,nbcell;
	double rminvec[DIM],rmin2=1E6;
	double shortest_rmin2=1E6;
	int endsign=0;
	unsigned char closest_type=0;
	double nearvec[DIM];
	double lambda_p,lambda_e;
	tedge *ptr_closest_e=NULL;
	tface *ptr_closest_f=NULL;

	double closest_rminvec[DIM],closest_nearvec[DIM];
	int closest_endsign=0;
	double bary[3],closest_bary[3]={};

	double closest_lambda_p=0.,closest_lambda_e=0.;

	/* First find the closest mesh object by checking in nearby wall cells */
	int cellp=p->cell_wall;
	for(i=neighbouring_cells_wall-1;i>=0;i--){
		nbcell=wallcells[cellp].neighbours[i];

		/* Check faces for distance */
		for(j=wallcells[nbcell].nf-1;j>=0;j--){
			id=wallcells[nbcell].faces[j];
			calc_distvec_rod_faceplane(p,&mesh.faces[id],rminvec,&rmin2,&endsign,bary);

			if(rmin2<shortest_rmin2){
				// 				printf("f shortest_rmin2, rmin2: %f %f\n",shortest_rmin2,rmin2);
				shortest_rmin2=rmin2;
				closest_endsign=endsign;
				memcpy(closest_bary,bary,sizeof(bary));
				memcpy(closest_rminvec,rminvec,sizeof(rminvec));
				ptr_closest_f=&mesh.faces[id];
				closest_type=3;
	// 				if(inprod(mesh.faces[id].outwardnormal,closest_rminvec)<0){
	// 					printf("Face %d with normal (%f %f %f) has dvec (%f %f %f)\n",
	// 							id,
	// 							mesh.faces[id].outwardnormal[0],mesh.faces[id].outwardnormal[1],mesh.faces[id].outwardnormal[2],
	// 							closest_rminvec[0],closest_rminvec[1],closest_rminvec[2]);
	// 				}
			}
		}
		// 		if(time_r>0.25) printf("closest face is %f away \n",sqrt(shortest_rmin2));

		/* Check edges for distance */
		for(j=wallcells[nbcell].ne-1;j>=0;j--){
			id=wallcells[nbcell].edges[j];
			calc_distvec_rod_edge(p,&mesh.edges[id],&rmin2,&lambda_p,&lambda_e,nearvec);

			if(rmin2<shortest_rmin2){
				// 				printf("e shortest_rmin2, rmin2: %f %f\n",shortest_rmin2,rmin2);
				shortest_rmin2=rmin2;
				closest_lambda_p=lambda_p;
				closest_lambda_e=lambda_e;
				memcpy(closest_nearvec,nearvec,sizeof(nearvec));
				ptr_closest_e=&mesh.edges[id];
				closest_type=2;
			}
		}

	// 		/* Check vertices for distance  */
	// 		/* NOTE this check is actually not needed. Vertices are by definition on the ends of edges,
	//		   so we already calculate all relevant rod-vertex distances in the rod-edge check. */
	// 		for(j=wallcells[nbcell].nf-1;j>=0;j--){
	// 			id=wallcells[nbcell].vertices[j];
	// 			calc_distvec_rod_vertex(p,&mesh.vertices[id],&rmin2,&lambda_p,nearvec);
	//
	// 			if(rmin2<shortest_rmin2){
	// 				//printf("rmin2, shortest_rmin2: %f %f\n",rmin2,shortest_rmin2);
	// 				shortest_rmin2=rmin2;
	// 				closest_lambda_p=lambda_p;
	// 				memcpy(closest_nearvec,nearvec,sizeof(nearvec));
	// 				closest_type=1;
	// 			}
	// 		}
	}

	/* Calculate forces and torques, only nonzero for r_min/d < 2^(1/6), according to WCA potential */
	if(shortest_rmin2<1.25992104989487){
		double torque[DIM],armp[DIM];		// Torque wall on end, minimum distance vector
		double force[DIM],f_red;					// Force wall on particle

		/* Calculate needed variables inside the IF-statement for maximum efficiency */
		if(closest_type==3){
			for(d=0;d<DIM;d++){
				armp[d]=closest_endsign*p->hll*p->or[d];
			}
		}else if(closest_type==2){
			for(d=0;d<DIM;d++){
				armp[d]=closest_lambda_p*p->or[d];
				closest_rminvec[d]=closest_nearvec[d]-armp[d]+closest_lambda_e*ptr_closest_e->or[d];
			}
		}
	// 		else if(closest_type==1){
	// 			for(d=0;d<DIM;d++){
	// 				armp[d]=closest_lambda_p*p->or[d];
	// 				closest_rminvec[d]=closest_nearvec[d]+armp[d];
	// 			}
	// 		}
		else{printf("Closest object not identified: memory error likely! Exiting.\n");exit(42);}

		/* Force calculation */
		double r2i=1./(shortest_rmin2);
		double r6i=r2i*r2i*r2i;
		f_red=r2i*r6i*(r6i-0.5);

		/* Total force */
		for(d=0;d<DIM;d++){
			force[d]=boundary_force_prefactor*closest_rminvec[d]*f_red;
		}

		/* Set force vectors */
		for(d=0;d<DIM;d++){
			p->force[d]-=force[d];
		}

	// 		printf("closest_rminvec %f %f %f\n",closest_rminvec[0],closest_rminvec[1],closest_rminvec[2]);
	// 		printf("shortest_rmin2, endsign: %f %d\n",shortest_rmin2,endsign);
	// 		printf("force on p: (%lf,%lf,%lf)\n",p->force[0],p->force[1],p->force[2]);
	// 		printf("force:(%lf,%lf,%lf)\n",force[0],force[1],force[2]);
	// 		printf("or:   (%lf,%lf,%lf)\n\n",p->or[0],p->or[1],p->or[2]);

		/* Calculate torque */
		crossprod(torque,force,armp); // T = r x F_i = r x -f = f x r
		// 		crossprod(torque,armp,force); // T = -r x F_i = -r x -f = r x f

		/* Set torque vectors */
		for(d=0;d<DIM;d++){
			p->torque[d]+=torque[d];
		}

		if(DYNAMIC_MESH){
			if(closest_type==2){ // Edge is closest, distribute force over 2 vertices.
	// 				if(time_r>1) printf("e ");
	// 				if(time_r>0.65) printf("edge closest with %f\n",sqrt(shortest_rmin2));
				tvertex *ptr_v1,*ptr_v2;
				ptr_v1=&mesh.vertices[ ptr_closest_e->vertices[0] ];
				ptr_v2=&mesh.vertices[ ptr_closest_e->vertices[1] ];
				// 				if(time_r>0.65) printf("force on edge: %f %f %f\n",force[0],force[1],force[2]);
				double forcefrac = (closest_lambda_e-ptr_closest_e->hll)/ptr_closest_e->hll;
				for(d=0;d<DIM;d++){
					ptr_v1->force[d]+=(1-forcefrac)*force[d];
					ptr_v2->force[d]+=forcefrac*force[d];
				}
			}
			else
				if(closest_type==3){ // Face is closest, distribute force over 3 vertices.
	// 				if(time_r>0.65) printf("face closest with %f\n",sqrt(shortest_rmin2));
	// 				if(time_r>1) printf("f ");
				tvertex *ptr_v1,*ptr_v2,*ptr_v3;
				ptr_v1=&mesh.vertices[ ptr_closest_f->vertices[0] ];
				ptr_v2=&mesh.vertices[ ptr_closest_f->vertices[1] ];
				ptr_v3=&mesh.vertices[ ptr_closest_f->vertices[2] ];
	// 				if(time_r>0.65) printf("force on face: %f %f %f\n",force[0],force[1],force[2]);
	// 				double x[3]={1,0,0};
	// 				if(inprod(force,x)>0){printf("+ ");}
	// 				if(inprod(force,x)<0){printf("- ");}
				for(d=0;d<DIM;d++){ // Sum of barycentric coordinates is 1, so that's easy.
					ptr_v1->force[d]+=closest_bary[0]*force[d];
					ptr_v2->force[d]+=closest_bary[1]*force[d];
					ptr_v3->force[d]+=closest_bary[2]*force[d];
				}
			}
		}

		/* Calculate virial term for pressure calculation */
		//virial+=inprod(force,rpv);
	}
}
/* --------- Calculates the force of the confining wall (closest mesh object) ---------
 * Calculates the distance to all nearby mesh objects. First, selects a nearby vertex based on the
 * wall cell list. Then calculates rod-vertex distance, rod-edge distance and rod-face distance
 * for all these objects (NOTE: no shortcuts?). The distance and the arm of the torque on the rod
 * are saved for all these checks, and the forces and torques on the rod are calculated based on
 * an interaction with the closest object. If the mesh is dynamic, we also need to calculate the
 * forces on the mesh. For vertices, this is trivial: they are forces acting on a point. For edges
 * we distribute the force between both vertices with a weight according to the point of closest
 * approach to the rod along the edge line. For faces, we distribute the force between the three
 * vertices of the face according to the weights corresponding to the barycentric coordinates of
 * the point on the face closest to the rod.
 *
 * Possible shortcuts/speed gains/early exits:
 * 	-	Only calculate stuff for objects associated with the closest vertex.
 * 			Disadvantage:  fails when rod is e.g. parallel to face or when faces are small
 * 	-	Only calculate stuff for closest few vertices
 * 			Disadvantage: same as above, no guarantee that this works
 */
void force_all_mesh_objects(tpart *p){
	int i,j,d,id,nbcell;
	double rminvec[DIM],rmin2=1E6;
	int endsign=0;
	double nearvec[DIM];
	double lambda_p,lambda_e;
	tedge *ptr_e;
	tface *ptr_f;
	double bary[3];
	double torque[DIM],armp[DIM],force[DIM],f_red,forcefrac;
	double r2i,r6i;

	int cellp=p->cell_wall;
	for(i=neighbouring_cells_wall-1;i>=0;i--){
		nbcell=wallcells[cellp].neighbours[i];

		/* Calculate interaction with faces */
		for(j=wallcells[nbcell].nf-1;j>=0;j--){
			id=wallcells[nbcell].faces[j];
			ptr_f=&mesh.faces[id];
			calc_distvec_rod_faceplane(p,ptr_f,rminvec,&rmin2,&endsign,bary);

			/* Calculate forces and torques, only nonzero for r_min/d < 2^(1/6), according to WCA potential */
			if(rmin2<1.25992104989487){

				/* Calculate needed variables inside the IF-statement for maximum efficiency */
				for(d=0;d<DIM;d++){
					armp[d]=endsign*p->hll*p->or[d];
				}

				/* Force calculation */
				r2i=1./(rmin2);
				r6i=r2i*r2i*r2i;
				f_red=r2i*r6i*(r6i-0.5);

				/* Total force */
				for(d=0;d<DIM;d++){
					force[d]=boundary_force_prefactor*rminvec[d]*f_red*ptr_f->area;
				}

				/* Set force vectors */
				for(d=0;d<DIM;d++){
					p->force[d]-=force[d];
				}

				/* Calculate torque */
				crossprod(torque,force,armp); // T = r x F_i = r x -f = f x r

				/* Set torque vectors */
				for(d=0;d<DIM;d++){
					p->torque[d]+=torque[d];
				}

				if(DYNAMIC_MESH){
					tvertex *ptr_v1,*ptr_v2,*ptr_v3;
					ptr_v1=&mesh.vertices[ ptr_f->vertices[0] ];
					ptr_v2=&mesh.vertices[ ptr_f->vertices[1] ];
					ptr_v3=&mesh.vertices[ ptr_f->vertices[2] ];
					for(d=0;d<DIM;d++){ // Sum of barycentric coordinates is 1, so that's easy.
						ptr_v1->force[d]+=bary[0]*force[d];
						ptr_v2->force[d]+=bary[1]*force[d];
						ptr_v3->force[d]+=bary[2]*force[d];
					}
				}
			}
		}

		/* Calculate interaction with edges */
		for(j=wallcells[nbcell].ne-1;j>=0;j--){
			id=wallcells[nbcell].edges[j];
			ptr_e=&mesh.edges[id];
			calc_distvec_rod_edge(p,&mesh.edges[id],&rmin2,&lambda_p,&lambda_e,nearvec);

			/* Calculate forces and torques, only nonzero for r_min/d < 2^(1/6), according to WCA potential */
			if(rmin2<1.25992104989487){

				/* Calculate needed variables inside the IF-statement for maximum efficiency */
				for(d=0;d<DIM;d++){
					armp[d]=lambda_p*p->or[d];
					rminvec[d]=nearvec[d]-armp[d]+lambda_e*ptr_e->or[d];
				}

				/* Force calculation */
				r2i=1./(rmin2);
				r6i=r2i*r2i*r2i;
				f_red=r2i*r6i*(r6i-0.5);

				/* Total force */
				for(d=0;d<DIM;d++){
					force[d]=boundary_force_prefactor*rminvec[d]*f_red*ptr_e->elen;
				}

				/* Set force vectors */
				for(d=0;d<DIM;d++){
					p->force[d]-=force[d];
				}

				/* Calculate torque */
				crossprod(torque,force,armp); // T = r x F_i = r x -f = f x r

				/* Set torque vectors */
				for(d=0;d<DIM;d++){
					p->torque[d]+=torque[d];
				}

				if(DYNAMIC_MESH){
					tvertex *ptr_v1,*ptr_v2;
					ptr_v1=&mesh.vertices[ ptr_e->vertices[0] ];
					ptr_v2=&mesh.vertices[ ptr_e->vertices[1] ];
					forcefrac = (lambda_e-ptr_e->hll)/ptr_e->hll;
					for(d=0;d<DIM;d++){
						ptr_v1->force[d]+=(1-forcefrac)*force[d];
						ptr_v2->force[d]+=forcefrac*force[d];
					}
				}
			}
		}
	}
}
/* --------- Calculates the WCA force repelling particles from a spherical wall ---------
 *
 */
void force_repulsive_sphericalwall_rod(tpart *p){
	int d,endsign;
	double armp[DIM],pos[DIM],dir[DIM];
	double rmin2,rmin;

	/* Calculate unsigned torque arm */
	for(d=0;d<DIM;d++){
		armp[d]=p->hll*p->or[d];
	}

	/* For rod within sphere, rod ends are always closest parts, but both can be close */
	/* So calculate interaction with both rod ends */
	for(endsign=-1;endsign<=1;endsign+=2){

		/* Calculate distance of rod end to sphere */
		for(d=0;d<DIM;d++){pos[d]=p->pos[d]+endsign*armp[d];}
		rmin = RADIUS - norm(pos) + 0.5 * p->d;
		rmin2 = rmin*rmin;
		// 		if(rmin2<40){printf("RADIUSSQ, normsq(pos), rmin2: %lf %lf %lf\n",RADIUSSQ,normsq(pos),rmin2);}

		/* Calculate forces and torques, only nonzero for r_min/d < 2^(1/6), according to WCA potential */
		if(rmin2<1.25992104989487){
			double torque[DIM],rminvec[DIM];		// Torque wall on end, minimum distance vector
			double force[DIM],f;					// Force wall on particle

			/* Minimum distance vector connecting the rod end to the wall */
			direction(dir,pos);
			for(d=0;d<DIM;d++){
				rminvec[d]=-rmin*dir[d];
			}
			//printf("rminvec: (%lf,%lf,%lf)\n",rminvec[0],rminvec[1],rminvec[2]);

			/* Force calculation */
			double r2i=1./(rmin2);
			double r6i=r2i*r2i*r2i;
			f=r2i*r6i*(r6i-0.5);

			/* Total force */
			for(d=0;d<DIM;d++){
				force[d]=boundary_force_prefactor*rminvec[d]*f;
				p->force[d]+=force[d];
			}
			//printf("RADIUSSQ, normsq(pos), rmin2: %lf %lf %lf\n",RADIUSSQ,normsq(pos),rmin2);
			//printf("pos p: (%lf,%lf,%lf)\n",pos[0],pos[1],pos[2]);
			//printf("force on p: (%lf,%lf,%lf)\n",force[0],force[1],force[2]);
	 		//printf("force on p: (%lf,%lf,%lf)\n",p->force[0],p->force[1],p->force[2]);

			/* Calculate torque */
			if(endsign==1){
				crossprod(torque,force,armp); // T = r x F_i = r x -F = F x r
			}else{
				crossprod(torque,armp,force); // T = r x -F_i = r x F
			}

			/* Set torque vectors */
			for(d=0;d<DIM;d++){
				p->torque[d]+=torque[d];
			}

			/* Calculate virial term for pressure calculation */
 			//virial+=inprod(force,rpv);
		}
	}
}
/* --------- Calculates the WCA force repelling spheres from a spherical wall ---------
 *
 */
void force_repulsive_sphericalwall_sphere(tpart *p){
	int d;
	double pos[DIM],dir[DIM];
	double rmin2,rmin;

	/* Calculate distance of rod end to sphere */
	rmin = RADIUS - norm(p->pos) + 0.5 * p->d;
	rmin2=rmin*rmin;

	/* Calculate forces and torques, only nonzero for r_min/d < 2^(1/6), according to WCA potential */
	if(rmin2<1.25992104989487){
		double rminvec[DIM];	// Torque wall on end, minimum distance vector
		double force[DIM],f;				// Force wall on particle

		/* Minimum distance vector connecting the rod end to the wall */
		direction(dir,pos);
		for(d=0;d<DIM;d++){
			rminvec[d]=-rmin*dir[d];
		}
		//printf("rminvec: (%lf,%lf,%lf)\n",rminvec[0],rminvec[1],rminvec[2]);

		/* Force calculation */
		double r2i=1./(rmin2);
		double r6i=r2i*r2i*r2i;
		f=r2i*r6i*(r6i-0.5);

		/* Total force */
		for(d=0;d<DIM;d++){
			force[d]=boundary_force_prefactor*rminvec[d]*f;
			p->force[d]+=force[d];
		}
		//printf("RADIUSSQ, normsq(pos), rmin2: %lf %lf %lf\n",RADIUSSQ,normsq(pos),rmin2);
		//printf("pos p: (%lf,%lf,%lf)\n",pos[0],pos[1],pos[2]);
		//printf("force on p: (%lf,%lf,%lf)\n",force[0],force[1],force[2]);
		//printf("force on p: (%lf,%lf,%lf)\n",p->force[0],p->force[1],p->force[2]);

		/* Calculate virial term for pressure calculation */
		//virial+=inprod(force,rpv);
	}
}
/* --------- Calculates the WCA force repelling a point from a planar wall ---------
 *
 */
void force_repulsive_y_plane_wall_sphere(double location){
	long int i;
	double dist;
	tpart *p;
	//tvertex *v;
	double y_pos_plane = location;
	double r2;

	 /* Length scale for WCA wall repulsion is 50% of particle diameter */
	double max_v_sq=0.25;
	double max_v_sq_inv=1.0/max_v_sq;

	/* Particles */
	for(i=0;i<n_part;i++){
		p=&part[i];
		dist=y_pos_plane-p->pos[1];
		if(dist>box.hL[1]){dist-=box.L[1];}
		else if(dist<-box.hL[1]){dist+=box.L[1];}
		r2=(dist*dist)*max_v_sq_inv;

		if(r2<1.25992104989487){ // 2^(1/3) = ( 2^(1/6) )^2
			double fWCA;
			double r2i=1.0/(r2);
			double r6i=r2i*r2i*r2i;
			fWCA=boundary_force_prefactor*r2i*r6i*(r6i-0.5);

			/* Total force */
			p->force[1]-=dist*max_v_sq_inv*fWCA;
		}
	}

	// 	/* Vertices */
	// 	if(DYNAMIC_MESH){
	// 		for(i=0;i<mesh.nvertices;i++){
	// 			v=&mesh.vertices[i];
	// 			dist=y_pos_plane-v->pos[1];
	// 			//printf("y_pos_plane, posy v, boxy: %f %f %f\n",y_pos_plane,v->pos[1],box.hL[1]);
	// 			if(dist>box.hL[1]){dist-=box.L[1];}
	// 			else if(dist<-box.hL[1]){dist+=box.L[1];}
	// 			r2=(dist*dist)*max_v_sq_inv;
	//
	// 			if(r2<1.25992104989487){ // 2^(1/3) = ( 2^(1/6) )^2
	// 				double fWCA;
	// 				double r2i=1.0/(r2);
	// 				double r6i=r2i*r2i*r2i;
	// 				fWCA=wca_prefactor*r2i*r6i*(r6i-0.5);
	//
	// 				/* Total force */
	// 				v->force[1]=-dist*max_v_sq_inv*fWCA;
	// 			}
	// 		}
	// 	}
}
/* --------- Calculates the WCA force repelling rods from a planar wall ---------
 *
 */
void force_repulsive_y_plane_wall_rod(double location){
	long int i;
	int d,endsign;
	double armp[DIM],y_pos_end;
	double y_pos_plane=location;
	double r2,dist,dir;
	tpart *p;

	 /* Length scale for WCA wall repulsion is 50% of particle diameter */
	double max_v_sq=0.25;
	double max_v_sq_inv=1.0/max_v_sq;

	for(i=0;i<n_part;i++){
		p=&part[i];

		/* Calculate unsigned torque arm */
		for(d=0;d<DIM;d++){
			armp[d]=p->hll*p->or[d];
		}

		/* For rod within cylinder, rod ends are always closest parts, but both can be close */
		/* So calculate interaction with both rod ends */
		for(endsign=-1;endsign<=1;endsign+=2){

			/* Calculate distance of rod end to planar wall */
			y_pos_end=p->pos[1]+endsign*armp[1];
			dist=y_pos_plane-y_pos_end;
			if(dist>box.hL[1]){dist-=box.L[1];}
			else if(dist<-box.hL[1]){dist+=box.L[1];}
			dir=(dist>0) ? 1. : -1.;
			dist=fabs(dist);
			r2=(dist*dist)*max_v_sq_inv;

			/* Calculate forces and torques, only nonzero for r_min/d < 2^(1/6), according to WCA potential */
			if(r2<1.25992104989487){
				double torque[DIM];		// Torque wall on end, minimum distance vector
				double force[DIM],f;	// Force wall on particle

				/* Force calculation */
				double r2i=1./(r2);
				double r6i=r2i*r2i*r2i;
				f=r2i*r6i*(r6i-0.5);

				/* Total force */
				force[0]=0.;
				force[1]=boundary_force_prefactor*max_v_sq_inv*dist*dir*f;
				force[2]=0.;
				p->force[1]-=force[1];

				/* Calculate torque */
				if(endsign==1){
					crossprod(torque,force,armp); // T = r x F_i = r x -F = F x r
				}else{
					crossprod(torque,armp,force); // T = r x -F_i = r x F
				}

				/* Set torque vectors */
				for(d=0;d<DIM;d++){
					p->torque[d]+=torque[d];
				}

				/* Calculate virial term for pressure calculation */
				//virial+=inprod(force,rpv);
			}
		}
	}
}
/* --------- Calculates the WCA force repelling a point from a cylindrical wall ---------
 * Cylinder is centered around (0,0,0)
 */
void force_repulsive_y_cylinder_wall_sphere(double diameter){
	long int i;
	double dist;
	tpart *p;
	double r2;
	double dist_xz[3];
	int d;

	 /* Length scale for WCA wall repulsion is 10% of particle diameter */
	double max_v_sq=0.01;
	double max_v_sq_inv=1.0/max_v_sq;

	/* Particles */
	for(i=0;i<n_part;i++){
		p=&part[i];

		/* Distance to wall */
		dist_xz[0]=p->pos[0];
		dist_xz[1]=0.;
		dist_xz[2]=p->pos[2];
		for(d=0;d<DIM;d++){
			if(dist_xz[d]>box.hL[d]){dist_xz[d]-=box.L[d];}
			else if(dist_xz[d]<-box.hL[d]){dist_xz[d]+=box.L[d];}
		}
		dist=0.5*diameter-norm(dist_xz)-0.5;
		//printf("dist diam norm %lf %lf %lf\n",dist,0.5*diameter,norm(dist_xz));
		r2=(dist*dist)*max_v_sq_inv;

		if(r2<1.25992104989487){ // 2^(1/3) = ( 2^(1/6) )^2
			double distvec[3],dir[3];
			/* Minimum distance vector connecting the rod end to the wall */
			direction(dir,dist_xz);
			for(d=0;d<DIM;d++){
				distvec[d]=-fabs(dist)*dir[d];
			}

			/* WCA force */
			double fWCA;
			double r2i=1.0/(r2);
			double r6i=r2i*r2i*r2i;
			fWCA=boundary_force_prefactor*r2i*r6i*(r6i-0.5);

			/* Total force */
			for(d=0;d<DIM;d++){
				p->force[d]+=distvec[d]*max_v_sq_inv*fWCA;
			}
		}
	}
}
/* --------- Calculates the WCA force repelling rods from a cylindrical wall ---------
 *
 */
void force_repulsive_y_cylinder_wall_rod(double diameter){
	long int i;
	int d,endsign;
	double armp[DIM],dist_xz[DIM],dir[DIM];
	double r2,dist;
	tpart *p;

	 /* Length scale for WCA wall repulsion is 10% of particle diameter */
	double max_v_sq=0.01;
	double max_v_sq_inv=1.0/max_v_sq;

	for(i=0;i<n_part;i++){
		p=&part[i];

		/* Calculate unsigned torque arm */
		for(d=0;d<DIM;d++){
			armp[d]=p->hll*p->or[d];
		}

		/* For rod within cylinder, rod ends are always closest parts, but both can be close */
		/* So calculate interaction with both rod ends */
		for(endsign=-1;endsign<=1;endsign+=2){

			/* Calculate distance of rod end to sphere */
			dist_xz[0]=p->pos[0]+endsign*armp[0];
			dist_xz[1]=0.;
			dist_xz[2]=p->pos[2]+endsign*armp[2];
			dist=0.5*diameter-norm(dist_xz)-0.5;
			r2=(dist*dist)*max_v_sq_inv;

			/* Calculate forces and torques, only nonzero for r_min/d < 2^(1/6), according to WCA potential */
			if(r2<1.25992104989487){
				double torque[DIM],distvec[DIM];		// Torque wall on end, minimum distance vector
				double force[DIM],f;					// Force wall on particle

				/* Minimum distance vector connecting the rod end to the wall */
				direction(dir,dist_xz);
				for(d=0;d<DIM;d++){
					distvec[d]=fabs(dist)*dir[d];
				}

				/* Force calculation */
				double r2i=1./(r2);
				double r6i=r2i*r2i*r2i;
				f=r2i*r6i*(r6i-0.5);

				/* Total force */
				for(d=0;d<DIM;d++){
					force[d]=boundary_force_prefactor*max_v_sq_inv*distvec[d]*f;
					p->force[d]-=force[d];
				}

				/* HACK If one end goes outside of cylinder, push it back in */
				//if(dist<0){
				//	force[1]=100*boundary_force_prefactor*max_v_sq_inv*distvec[1]*f;
				//	p->force[1]-=force[1];
				//}

				/* Calculate torque */
				if(endsign==1){
					crossprod(torque,force,armp); // T = r x F_i = r x -F = F x r
				}else{
					crossprod(torque,armp,force); // T = r x -F_i = r x F
				}

				/* Set torque vectors */
				for(d=0;d<DIM;d++){
					p->torque[d]+=torque[d];
				}

				/* Calculate virial term for pressure calculation */
				//virial+=inprod(force,rpv);
			}
		}
	}
}
/* --------- Calculates gravity (in minus y direction) on all objects ---------
 *
 */
void force_gravity_y(void){
	int i;
	double const_gravity=1E2*dt;

	/* Particles */
	tpart *p;
	double gravity_on_p=const_gravity*Vpart/n_part; // scales with volume
	for(i=0;i<n_part;i++){
		p=&part[i];
		p->force[1]-=gravity_on_p;
	}

	/* Vertices */
	if(DYNAMIC_MESH){
		tface *f;
		tvertex *v1,*v2,*v3;
		for(i=0;i<mesh.nfaces;i++){
			f=&mesh.faces[i];
			v1=&mesh.vertices[ f->vertices[0] ];
			v2=&mesh.vertices[ f->vertices[1] ];
			v3=&mesh.vertices[ f->vertices[2] ];
			v1->force[1]-=const_gravity*f->area;
			v2->force[1]-=const_gravity*f->area;
			v3->force[1]-=const_gravity*f->area;
		}
	}
}
/* --------- Calculates the force resulting of crosslink extension ---------
 * The crosslinkers attach to somewhere along the surface of two rods and behave like springs.
 * So we calculate the two points in real space where the crosslinker ends are, and from that
 * calculate the elongation, turning that into a force vector acting on particle i (that on j
 * is then minus that). For simplicity we neglect the influence of finite rod thickness on the
 * elongation of the crosslinker.
 */
void force_crosslink(tcrosslink *cl, tpart *pi, tpart *pj){
	int d;
	double posi[DIM],posj[DIM],dvec[DIM],F[DIM];

	/* Calculate the points on which the crosslinkers are attached */
	for(d=0;d<DIM;d++){
		posi[d]=pi->pos[d]+cl->l1*pi->or[d];
		posj[d]=pj->pos[d]+cl->l2*pj->or[d];
	}

	/* Take the nearest image and obtain crosslink extension */
	nearestimage(posi,posj,dvec);

	/* Calculate the resulting force */
	for(d=0;d<DIM;d++){
		F[d]=crosslink_force_prefactor*dvec[d];
	}

	/* Apply the forces */
	for(d=0;d<DIM;d++){
		pi->force[d]+=F[d];
		pj->force[d]-=F[d];
		cl->force[d]=F[d];
	}

	/* Update the virial term */
	double near_rij[DIM];
	nearestimage(pi->pos,pj->pos,near_rij);
	virial+=inprod(F,near_rij);

	/* Calculate the energy of the crosslink */
	//double distsq=normsq(dvec);
	//energy=0.5*crosslink_force_prefactor*distsq;
}
/* --------- Calculates the self-avoidance WCA force between two vertices ---------
 *
 */
void force_mesh_selfavoidance(void){
	tvertex *v1_ptr,*v2_ptr;
	int i,d;
	long int j,v1i,v2i,cellv1,nbcell;
	double dvec[DIM];
	double fd,r2,r2i,r6i,fWCA;

	 /* Length scale for WCA vertex repulsion (~diameter) is half of original max vertex distance */
	double max_v_sq=0.25*(max_vertex_spacing_initial*max_vertex_spacing_initial);
	double max_v_sq_inv=1.0/max_v_sq;

	/* Get vertex pointers from cell list */
	for(v1i=0;v1i<mesh.nvertices;v1i++){
		v1_ptr=&mesh.vertices[v1i];
		cellv1=v1_ptr->cell;
		for(i=neighbouring_cells_wall-1;i>=0;i--){
			//printf("b4\n");
			nbcell=wallcells[cellv1].neighbours[i];
			//printf(" nbcell = %ld, has %ld vertices\n",nbcell,wallcells[nbcell].nv);
			for(j=wallcells[nbcell].nv-1;j>=0;j--){
				//printf(" j starts at %ld\n",wallcells[nbcell].nv-1);
				v2i=wallcells[nbcell].vertices[j];
				if(v1i==v2i){continue;} // Don't let vertex i interact with itself
				v2_ptr=&mesh.vertices[v2i];
				//printf("  v1 = %ld, v2 = %ld\n",v1i,v2i);
				//printf("  have pos (%lf %lf %lf), (%lf %lf %lf)\n",v1_ptr->pos[0],v1_ptr->pos[1],v1_ptr->pos[2],v2_ptr->pos[0],v2_ptr->pos[1],v2_ptr->pos[2]);

				/* Take the nearest image and obtain distance vector and related quantities */
				nearestimage(v1_ptr->pos,v2_ptr->pos,dvec);
				r2=normsq(dvec)*max_v_sq_inv; // (r/d_vertex)^2
				//printf("   after\n");

				/* Calculate forces */
				if(r2<1.25992104989487){ // Include WCA repulsion
					r2i=1.0/(r2);
					r6i=r2i*r2i*r2i;
					fWCA=wca_prefactor*r2i*r6i*(r6i-0.5);
					for(d=0;d<DIM;d++){
						fd=(max_v_sq_inv*dvec[d]*fWCA);
						v1_ptr->force[d]-=fd;
						v2_ptr->force[d]+=fd;
					}
				}
			}
		}
		//printf("%f %f %f\n",vi->force[0],vi->force[1],vi->force[2]);
		//printf("%f %f %f\n",vj->force[0],vj->force[1],vj->force[2]);
	}

}
/* --------- Calculates the force from stretching the mesh ---------
 * Stretching is usually modeled as a harmonic potential between vertices.
 */
void force_mesh_stretching(void){
	tvertex *v1,*v2;
	tedge *e;
	int i,d;
	double dvec[DIM],dir[DIM];
	double dist,fd,inv_eq_len;

	/* calculate bending contribution for all hinges */
	for(i=0;i<mesh.nedges;i++){
		e=&mesh.edges[i];
		/* Get vertices of edge */
		v1=&mesh.vertices[ e->vertices[0] ];
		v2=&mesh.vertices[ e->vertices[1] ];

		/* Get equilibrium length of edge */
		inv_eq_len=1.0/e->eq_elen;

		/* Take the nearest image and obtain distance vector and related quantities */
		nearestimage(v1->pos,v2->pos,dvec);
		dist=sqrt(normsq(dvec));
		direction(dir,dvec);

		/* Calculate forces */
		for(d=0;d<DIM;d++){
			fd=-(dist-e->eq_elen)*dir[d]*inv_eq_len*stretching_force_prefactor;
			v1->force[d]-=fd;
			v2->force[d]+=fd;
		}

	// 	printf("force %f %f %f\n",
	// 	(dist-mesh.vertices[0].eqr[0])*dvec[0]*stretching_force_prefactor,
	// 	(dist-mesh.vertices[0].eqr[0])*dvec[1]*stretching_force_prefactor,
	// 	(dist-mesh.vertices[0].eqr[0])*dvec[2]*stretching_force_prefactor);
		//printf("%f %f %f\n",vi->force[0],vi->force[1],vi->force[2]);
		//printf("%f %f %f\n",vj->force[0],vj->force[1],vj->force[2]);
	}
}
/* --------- Calculates the force from bending the mesh ---------
 * Based on the following papers:
 * DOI: 10.1016/j.cagd.2007.07.006
 * DOI: 10.1145/1198555.1198573
 */
void force_mesh_bending(void){
	int i,d;
	tedge *e;
	tvertex *v1,*v2,*v3,*v4;
	tface *f1,*f2;
	double sin_th,sin_h_th;
	double u[4][3];
	double n[2][3];
	double v13[3],v14[3],v23[3],v24[3];
	double inverse_norm_n_sq;
	double elen,area_norm;
	double tempvec[3];
	double force_magnitude;

	/* calculate bending contribution for all hinges */
	for(i=0;i<mesh.nedges;i++){
		/* Get pointers to edge and vertices */
		e=&mesh.edges[i];
		if(e->is_boundary){continue;} // Skip edges that aren't hinges i.e. are on the boundary of the mesh
		f1=&mesh.faces[ e->faces[0] ];
		f2=&mesh.faces[ e->faces[1] ];
		v1=e->hinge_v[0];
		v2=e->hinge_v[1];
		v3=e->hinge_v[2];
		v4=e->hinge_v[3];
	// 		printf("vertices: %d %d %d %d\n",e->hinge_v_i[0],e->hinge_v_i[1],e->hinge_v_i[2],e->hinge_v_i[3]);
	// 		printf("pos v1: %f %f %f\n",mesh.vertices[ e->hinge_v_i[0] ].pos[0],mesh.vertices[ e->hinge_v_i[0] ].pos[1],mesh.vertices[ e->hinge_v_i[0] ].pos[2]);
	// 		printf("pos v1: %f %f %f\n",v1->pos[0],v1->pos[1],v1->pos[2]);
	// 		printf("pos v2: %f %f %f\n",mesh.vertices[ e->hinge_v_i[1] ].pos[0],mesh.vertices[ e->hinge_v_i[1] ].pos[1],mesh.vertices[ e->hinge_v_i[1] ].pos[2]);
	// 		printf("pos v2: %f %f %f\n",v2->pos[0],v2->pos[1],v2->pos[2]);
	// 		printf("pos v3: %f %f %f\n",mesh.vertices[ e->hinge_v_i[2] ].pos[0],mesh.vertices[ e->hinge_v_i[2] ].pos[1],mesh.vertices[ e->hinge_v_i[2] ].pos[2]);
	// 		printf("pos v3: %f %f %f\n",v3->pos[0],v3->pos[1],v3->pos[2]);
	// 		printf("pos v4: %f %f %f\n",mesh.vertices[ e->hinge_v_i[3] ].pos[0],mesh.vertices[ e->hinge_v_i[3] ].pos[1],mesh.vertices[ e->hinge_v_i[3] ].pos[2]);
	// 		printf("pos v4: %f %f %f\n\n",v4->pos[0],v4->pos[1],v4->pos[2]);

		/* Inner product describes cosine of hinge bending angle */
		crossprod(tempvec,f1->outwardnormal,f2->outwardnormal);
		sin_th=inprod(tempvec,e->or); // n1 x n2 . e
		sin_h_th=samesign( sqrt(0.5*fabs(1-inprod(f1->outwardnormal,f2->outwardnormal))) , sin_th );
		//printf("sin_h_th, eq_sin_h_th: %f %f\n",sin_h_th,e->eq_sin_h_th);

		/* Get gradient of theta at all vertices */
	// 		nearestimage(v1->pos,v3->pos,v13);
	// 		nearestimage(v1->pos,v4->pos,v14);
		nearestimage(v3->pos,v1->pos,v13);
		nearestimage(v4->pos,v1->pos,v14);
		crossprod(n[0],v13,v14);
	// 		printf("n1: %f %f %f\n",n[0][0],n[0][1],n[0][2]);
		inverse_norm_n_sq=1.0/normsq(n[0]);
		for(d=0;d<3;d++){n[0][d]*=inverse_norm_n_sq;}
	// 		printf("n1: %f %f %f\n",n[0][0],n[0][1],n[0][2]);

	// 		nearestimage(v2->pos,v4->pos,v24);
	// 		nearestimage(v2->pos,v3->pos,v23);
		nearestimage(v4->pos,v2->pos,v24);
		nearestimage(v3->pos,v2->pos,v23);
		crossprod(n[1],v24,v23);
		// 		printf("n2: %f %f %f\n",n[1][0],n[1][1],n[1][2]);
		inverse_norm_n_sq=1.0/normsq(n[1]);
		for(d=0;d<3;d++){n[1][d]*=inverse_norm_n_sq;}
		// 		printf("n2: %f %f %f\n",n[1][0],n[1][1],n[1][2]);

		elen=2*e->hll;
		// 		printf("edge length: %f\n",elen);
		for(d=0;d<3;d++){
			u[0][d]=elen*n[0][d];
			u[1][d]=elen*n[1][d];
			u[2][d]=inprod(v14,e->vec)*elen*n[0][d]+inprod(v24,e->vec)*elen*n[1][d];
			u[3][d]=-inprod(v13,e->vec)*elen*n[0][d]-inprod(v23,e->vec)*elen*n[1][d];
		}
	// 		printf("u1: %f %f %f\n",u[0][0],u[0][1],u[0][2]);
	// 		printf("u2: %f %f %f\n",u[1][0],u[1][1],u[1][2]);
	// 		printf("u3: %f %f %f\n",u[2][0],u[2][1],u[2][2]);
	// 		printf("u4: %f %f %f\n",u[3][0],u[3][1],u[3][2]);

		/* Construct forces on the vertices of the hinge */
		area_norm=elen*elen/( norm(n[0])+ norm(n[1]) );
		// 		printf("area norm: %f\n",area_norm);
		force_magnitude=bending_force_prefactor*area_norm*(sin_h_th-e->eq_sin_h_th);
	// 		if( isnan(force_magnitude) ){
	// 			printf("nan bending force found!\n");
	// 			printf("areanorm sinhth, eqsinhth: %f %f %f\n",area_norm,sin_h_th,e->eq_sin_h_th);
	// 			printf("%f (%f %f %f) (%f %f %f)\n",
	// 				sqrt(0.5*fabs(1-inprod(f1->outwardnormal,f2->outwardnormal))),
	// 				f1->outwardnormal[0],f1->outwardnormal[1],f1->outwardnormal[2],
	// 				f2->outwardnormal[0],f2->outwardnormal[1],f2->outwardnormal[2]
	// 			);
	// 			exit(42);
	// 		}
		for(d=0;d<3;d++){
			v1->force[d]+=force_magnitude*u[0][d];
			v2->force[d]+=force_magnitude*u[1][d];
			v3->force[d]+=force_magnitude*u[2][d];
			v4->force[d]+=force_magnitude*u[3][d];
		}
	// 		printf("force v1: %f %f %f\n",bending_force_prefactor*area_norm*sin_h_th*u[0][0],bending_force_prefactor*area_norm*sin_h_th*u[0][1],bending_force_prefactor*area_norm*sin_h_th*u[0][2]);
	// 		printf("force v2: %f %f %f\n",bending_force_prefactor*area_norm*sin_h_th*u[1][0],bending_force_prefactor*area_norm*sin_h_th*u[1][1],bending_force_prefactor*area_norm*sin_h_th*u[1][2]);
	// 		printf("force v3: %f %f %f\n",bending_force_prefactor*area_norm*sin_h_th*u[2][0],bending_force_prefactor*area_norm*sin_h_th*u[2][1],bending_force_prefactor*area_norm*sin_h_th*u[2][2]);
	// 		printf("force v4: %f %f %f\n",bending_force_prefactor*area_norm*sin_h_th*u[3][0],bending_force_prefactor*area_norm*sin_h_th*u[3][1],bending_force_prefactor*area_norm*sin_h_th*u[3][2]);
	}
}
/* --------- Calculates the force and torques between two rods (WCA potential) ---------
 * This function takes the pointers to two particle structs *pi and *pj, and calculates the forces
 * and torques on them assuming they are two spherocylinders interacting through a repulsive WCA
 * (Weeks-Chandler-Anderson) potential. This potential is the purely repulsive part of the
 * Lennard-Jones potential. In order to improve stability there is an optional argument SOFTEN_WCA
 * which softens this potential, meaning it truncates it to a fixed value if a certain threshold is
 * exceeded. This is to prevent huge forces and torques if the Brownian displacements place two
 * particles in close proximity. The timestep dt should be adjusted so that this softening happens
 * as little as possible.
 */
void force_2rods_WCA(tpart *pi, tpart *pj){
	/* Nearest image convention for CM distance */
	double near_rij[DIM];					// Vector i->j, nearest image i->j
	nearestimage(pi->pos,pj->pos,near_rij);

	/* Early exit on spherical bounding box */
	if(normsq(near_rij)>max_interaction_length_2rods*max_interaction_length_2rods){return;}
	/* Early exit based on axis-aligned bounding boxes */
	int d;
	if(lod>3){ // Only really worth doing for longer rods.
		for(d=0;d<DIM;d++){
			if(fabs(near_rij[d])-max_interaction_length_bbox>lod*(fabs(pi->or[d])+fabs(pj->or[d]))){
				return;
			}
		}
	}

	/* Calculate the minimum distance between and closest points (arms) on the two rods */
	double lambda_i,lambda_j;	// lambda * or is the torque arm
	double rmin2;							// Helper variable, minimum distance between rods squared
	rmin2=distsq_rods(pi,pj,near_rij,&lambda_i,&lambda_j);

	/* Calculate forces and torques, only nonzero for r_min/d < 2^(1/6), according to WCA potential */
	if(rmin2 < TWO_POW_ONE_THIRD){
		/* Check for exceptions such as rmin2<=0 */
		// if(rmin2<=0.){return;}

		/* Minimum distance vector connecting the two lines in the center of the rods (NOT surface!) */
		double arm_i[DIM],arm_j[DIM];			// Lever arms of the torque on i and j
		double rminvec[DIM];					// Vector between closest 2 points
		for(d=0;d<DIM;d++){
			arm_i[d]=lambda_i*pi->or[d];
			arm_j[d]=lambda_j*pj->or[d];
			rminvec[d]=near_rij[d]-arm_i[d]+arm_j[d];
		}

		/* Force calculation */
		// double r2tt = TWO_POW_ONE_THIRD * rmin2; // If potential starts at 1.0, gotta make sure uwca(1)==0
		// double r2i = 1.0/r2tt;
		double r2i = 1.0 / rmin2;
		double r6i = r2i*r2i*r2i;
		double f = wca_prefactor * r2i * r6i * (r6i-0.5);

		/* Total force */
		double force[DIM];	// Force i->j and helper variable
		for(d=0;d<DIM;d++){
			force[d]=rminvec[d]*f;	// rminvec is closest line-to-line distance
		}

		/* Set force vectors */
		for(d=0;d<DIM;d++){
			pi->force[d]+=-force[d];	// Note that this force already includes the WCA force factor
			pj->force[d]+=force[d];
		}

		/* Calculate torque */
		double tor_i[DIM],tor_j[DIM];			// Torque due to interaction i with j
		crossprod(tor_i,force,arm_i); // T = r x F_i = r x -F
		crossprod(tor_j,arm_j,force); // F_j=-F_i so switch order
		if(FORCETROUBLE){
			printf("armi: %4.04lf %4.04lf %4.04lf\n fi: %4.04lf %4.04lf %4.04lf\n tori: %4.04lf %4.04lf %4.04lf \n\n",
				   arm_i[0],arm_i[1],arm_i[2],pi->force[0],pi->force[1],pi->force[2],tor_i[0],tor_i[1],tor_i[2]);
			printf("armj: %4.04lf %4.04lf %4.04lf\n fj: %4.04lf %4.04lf %4.04lf\n torj: %4.04lf %4.04lf %4.04lf \n\n",
				   arm_j[0],arm_j[1],arm_j[2],pj->force[0],pj->force[1],pj->force[2],tor_j[0],tor_j[1],tor_j[2]);
		}

		/* Set torque vectors */
		for(d=0;d<DIM;d++){
			pi->torque[d]+=tor_i[d];
			pj->torque[d]+=tor_j[d];
		}

		/* Calculate virial term for pressure calculation */
		virial+=inprod(force,near_rij);
		int d2;
		for(d=0;d<DIM;d++){
			for(d2=0;d<DIM;d++){
				pi->virialstress[d][d2]+=near_rij[d]*force[d2];
				pj->virialstress[d][d2]+=near_rij[d]*force[d2]; // -dvec * -force = dvec * force
			}
		}
	}
}
/* --------- Calculates the force between two spheres ---------
 *
 */
void force_2spheres_WCA(tpart *pi, tpart *pj){
	double dvec[DIM];

	/* Take the nearest image and obtain distance */
	nearestimage(pi->pos,pj->pos,dvec);
	//printf("%lf %lf %lf\n",dvec[0],dvec[1],dvec[2]);

	double r2 = normsq(dvec);

	/* WCA Force calculation */
	if(r2 < TWO_POW_ONE_THIRD){ // Repulsion starts at the diameter of the particle
		double fWCA,force[DIM];
		// double r2tt = TWO_POW_ONE_THIRD * r2; // If potential starts at 1.0, gotta make sure uwca(1)==0
		// double r2i = 1.0/(r2tt);
		double r2i = 1.0 / r2;
		double r6i = r2i*r2i*r2i;
		fWCA = wca_prefactor*r2i*r6i*(r6i-0.5);

		/* Total force */
		int d;
		for(d=0;d<DIM;d++){
			force[d]=dvec[d]*fWCA;
			pi->force[d]-=force[d];
			pj->force[d]+=force[d];
		}
		//printf("%f %f %f\n",vi->force[0],vi->force[1],vi->force[2]);
		//printf("%f %f %f\n",vj->force[0],vj->force[1],vj->force[2]);

		/* Calculate virial term for pressure calculation */
		virial+=inprod(force,dvec);
		int d2;
		for(d=0;d<DIM;d++){
			for(d2=0;d<DIM;d++){
				pi->virialstress[d][d2]+=dvec[d]*force[d2];
				pj->virialstress[d][d2]+=dvec[d]*force[d2]; // -dvec * -force = dvec * force
			}
		}
	// 		printf(" ( (%lf %lf %lf) (%lf %lf %lf) (%lf %lf %lf) )\n",
	// 			dvec[0]*force[0],dvec[0]*force[1],dvec[0]*force[2],
	// 			dvec[1]*force[0],dvec[1]*force[1],dvec[1]*force[2],
	// 			dvec[2]*force[0],dvec[2]*force[1],dvec[2]*force[2]);
	// 		printf(" ( (%lf %lf %lf) (%lf %lf %lf) (%lf %lf %lf) )\n",
	// 			pi->virialstress[0][0],pi->virialstress[0][1],pi->virialstress[0][2],
	// 			pi->virialstress[1][0],pi->virialstress[1][1],pi->virialstress[1][2],
	// 			pi->virialstress[2][0],pi->virialstress[2][1],pi->virialstress[2][2]);
	}
	return;
}
/* --------- Evaluate all pair interactions between particles ---------
 * For all particles, calculates the pair interaction with all particles in the nearest neighbouring
 * cells. The WCA potential is short-ranged, and the minimum cell size is defined so that this check
 * should cover all interactions. However, if you're reading this, check "max_interaction_length"!
 */
void evaluate_forces(void){
	int p;
	int i,j,id,d,d2;
	int cellp,nbcell;

	/* Reset the virial term */
	virial=0.;
	swim_virial=0.;

	/* Set all forces to zero before recalculation (since we use += to sum over all particles) */
	for(p=0;p<n_part;p++){
		for(d=0;d<DIM;d++){
			part[p].force[d]=0.;
			part[p].torque[d]=0.;
			for(d2=0;d2<DIM;d2++){
				part[p].virialstress[d][d2]=0.;
				part[p].swimstress[d][d2]=0.;
			}
		}
	}
	if(DYNAMIC_MESH){
		for(p=0;p<mesh.nvertices;p++){
			for(d=0;d<DIM;d++){
				mesh.vertices[p].force[d]=0.;
			}
		}
	}

	/* Calculate all pair interactions p<id with particles in neighbouring cells */
	if(PARTICLEINTERACTIONS){
		if(SPHERES){
			for(p=0;p<n_part-1;p++){
				cellp=part[p].cell;
				for(i=neighbouring_cells-1;i>=0;i--){
					nbcell=cells[cellp].neighbours[i];
					for(j=cells[nbcell].n-1;j>=0;j--){
						id=cells[nbcell].particles[j];
						if(p<id){force_2spheres_WCA(&part[p],&part[id]);}
					}
				}
			}
		}else{
			for(p=0;p<n_part-1;p++){
				cellp=part[p].cell;
				for(i=neighbouring_cells-1;i>=0;i--){
					nbcell=cells[cellp].neighbours[i];
					for(j=cells[nbcell].n-1;j>=0;j--){
						id=cells[nbcell].particles[j];
						if(p<id){force_2rods_WCA(&part[p],&part[id]);}
					}
				}
			}
		}
	}

	/* Calculate all forces as a result of the crosslinks */
	if(CROSSLINKS){
		for(i=0;i<n_crosslinks;i++){
			force_crosslink(&crosslink[i],crosslink[i].p1,crosslink[i].p2);
		}
	}

	/* Calculate all forces as a result of the spherical confining wall */
	if(INSIDE_SPHERE){
		if(SPHERES){
			for(p=0;p<n_part;p++){
				force_repulsive_sphericalwall_rod(&part[p]);
			}
		}else{
			for(p=0;p<n_part;p++){
				force_repulsive_sphericalwall_rod(&part[p]);
			}
		}
	}

	/* Calculate all forces as a result of the triangulated confining wall */
	if(HAVE_MESH){ // including forces on mesh, if mesh is dynamic
		for(p=0;p<n_part;p++){
			//force_closest_mesh_object(&part[p]);
			force_all_mesh_objects(&part[p]);
		}
	}

	/* Calculate gravity on all particles and faces */
	if(HAVE_GRAVITY){
		force_gravity_y();
	}

	/* Make some walls at some y */
	if(HAVE_PLANAR_WALLS){
		if(SPHERES){
			force_repulsive_y_plane_wall_sphere(-box.hL[1]);
		}else{
			force_repulsive_y_plane_wall_rod(-box.hL[1]);
		}
	}

	/* Cylinder */
	if(INSIDE_CYLINDER){
		if(SPHERES){
			force_repulsive_y_cylinder_wall_sphere(diameter_cylinder);
		}else{
			force_repulsive_y_cylinder_wall_rod(diameter_cylinder);
		}
	}


	/* Soften WCA potential to avoid instability due to excessively high forces/torques */
	// if(SOFTEN_WCA){
	// 	double normforcesq,normtisq;
	// 	for(p=0;p<n_part;p++){
	// 		normforcesq=normsq(part[p].force);
	// 		if( isnan(normforcesq) /*|| !isfinite(normforcesq)*/ ){
	// 			printf("Force exception detected during softening, is NaN or infinite!\n");
	// 			printf(" force part %d: %f %f %f\n",p,part[p].force[0],part[p].force[1],part[p].force[2]);
	// 			exit(1);
	// 		}
	// 		if(normforcesq>max_force_translation_sq){	// cutoff is so that max displacement is d/10
	// 			double sff=sqrt(max_force_translation_sq/normforcesq); // scale factor force
	// 			for(d=0;d<DIM;d++){part[p].force[d]*=sff;}
	// 			F_softened++;
	// 		}
	// 		F_evaluated++;
	// 		normtisq=normsq(part[p].torque);
	// 		if( isnan(normtisq) /*|| !isfinite(normtisq)*/ ){
	// 			printf("Torque exception detected during softening, is NaN or infinite!\n");
	// 			printf(" torque part %d: %f %f %f\n",p,part[p].torque[0],part[p].torque[1],part[p].torque[2]);
	// 			exit(1);
	// 		}
	// 		if(normtisq>max_torque_rotation_sq){	// cutoff is so that max rotation is 2d/10l rads
	// 			double sfti=sqrt(max_torque_rotation_sq/normtisq); // scale factor torque i
	// 			for(d=0;d<DIM;d++){part[p].torque[d]*=sfti;}
	// 			T_softened++;
	// 		}
	// 		T_evaluated++;
	// 	}
	// }

	/* If mesh is dynamic, calculate stretching and bending forces */
	if(DYNAMIC_MESH){
		force_mesh_stretching();
		force_mesh_selfavoidance();
		force_mesh_bending();
		/* Soften forces on vertices if needed */
		if(SOFTEN_WCA){
			double normvforcesq;
			for(p=0;p<mesh.nvertices;p++){
				normvforcesq=normsq(mesh.vertices[p].force);
				if( isnan(normvforcesq) /*|| !isfinite(normvforcesq)*/ ){
					printf("Force exception detected during softening for mesh, is NaN or infinite!\n");
					printf(" force vertex %d: %f %f %f\n",p,mesh.vertices[p].force[0],mesh.vertices[p].force[1],mesh.vertices[p].force[2]);
					exit(1);
				}
				if(normvforcesq>max_force_translation_sq){	// cutoff is so that max displacement is d/10
					double sff=sqrt(max_force_translation_sq/normvforcesq); // scale factor force
					for(d=0;d<DIM;d++){mesh.vertices[p].force[d]*=sff;}
					F_softened++;
				}
				F_evaluated++;
			}
		}
	}

	/* For testing cell-list issues */
	/* for(i=0;i<n_part-1;i++){
		for(j=i+1;j<n_part;j++){
			force_2rods_WCA(&part[i],&part[j]);
		}
	} */
}

/*========================== Equation of motion integration functions ==========================*/

/* --------- Moves all bound crosslinks along the rods ---------
 *
 */
void integrate_positions_crosslinks(void){
	int cl;

	/* Move crosslink ends along the rods */
	for(cl=0;cl<n_crosslinks;cl++){
		crosslink[cl].l1+=crosslink_displacement;
		crosslink[cl].l2+=crosslink_displacement;
		if(crosslink[cl].l1>crosslink[cl].p1->l){removecrosslink(cl);}
		else if(crosslink[cl].l2>crosslink[cl].p2->l){removecrosslink(cl);}
	}
}
/* --------- Integrates translational equations of motion for rods (BAOAB) ---------
 * BAOAB Scheme from DOI: 10.1093/amrx/abs010
 */
void integrate_positions_rods(void){
	int i,k,l,d;
	double uu[DIM][DIM]; 					// Dyadic product of the orientation of i
	double WCA_friction[DIM][DIM];			// One over the friction tensor
	double dx_B[DIM];						// Brownian displacement vector
	double dx_F[DIM];						// Displacement due to interaction forces
	double dx_A[DIM];						// Active displacement
	double G_rands_at_t[3];					// (para, perp1, perp2) Gaussian random numbers for BAOAB

	/* "Standard" equations of motion */
	for(i=0;i<n_part;i++){
		/* Save position as previous position */
		for(d=0;d<DIM;d++){
			part[i].prev_pos[d]=part[i].pos[d];
		}

		/* Calculate translational friction tensor */
		dyadic(uu,part[i].or,part[i].or); // calculate value of dyadic tensor uu
		for(k=0;k<DIM;k++){
			for(l=0;l<DIM;l++){
				WCA_friction[k][l]=force_prefactor_para*uu[k][l]+force_prefactor_perp*(Identity[k][l]-uu[k][l]);
			}
		}

		/* Brownian displacements */
		for(k=0;k<3;k++){
			G_rands_at_t[k]=gaussianrand();
		}
		// for(d=0;d<DIM;d++){ // BAOAB bugs out because perpendicular orientations get rechosen every step
		// 	dx_B[d]=random_force_prefactor_para_rod*(part[i].prevrands_trans[0]+G_rands_at_t[0])*part[i].or[d]+
		// 			random_force_prefactor_perp_rod*(part[i].prevrands_trans[1]+G_rands_at_t[1])*part[i].orper1[d]+
		// 			random_force_prefactor_perp_rod*(part[i].prevrands_trans[2]+G_rands_at_t[2])*part[i].orper2[d];
		// }
		for(d=0;d<DIM;d++){ // Euler-Maruyama doesnt have that problem
			dx_B[d]=2*random_force_prefactor_para_rod*G_rands_at_t[0]*part[i].or[d]+
					2*random_force_prefactor_perp_rod*G_rands_at_t[1]*part[i].orper1[d]+
					2*random_force_prefactor_perp_rod*G_rands_at_t[2]*part[i].orper2[d];
		}
		for(k=0;k<3;k++){
			part[i].prevrands_trans[k]=G_rands_at_t[k];
		}
		//printf(" t i dx_B: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n\n",time_r,i,dx_B[0],dx_B[1],dx_B[2]);

		// Calculate WCA part and truncate if needed
		mvinprod(dx_F, WCA_friction, part[i].force);
		F_softened += vectruncate(dx_F, max_force_translation_sq);
		F_evaluated++;
		// printf(" t i dx_F: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n",time_r,i,dx_F[0],dx_F[1],dx_F[2]);

		/* Active displacement */
		for(d=0;d<DIM;d++){
			dx_A[d]=active_prefactor_rod*part[i].or[d];
		}
		//printf(" t i dx_A: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n",time_r,i,dx_A[0],dx_A[1],dx_A[2]);

		/* Integrate equation of motion of the positions */
		for(d=0;d<DIM;d++){
			part[i].pos[d]+=(dx_F[d]+dx_B[d]);
			if(part[i].isactive){part[i].pos[d]+=dx_A[d];}
		}

		/* Integrate equation of motion of the positions */
		for(d=0;d<DIM;d++){
			part[i].rpos[d]+=(dx_F[d]+dx_B[d]);
			if(part[i].isactive){part[i].rpos[d]+=dx_A[d];}
		}

		/* Put particles back into box if the step takes them outside and update cells */
		put_particle_back_in_box(i);
	}
}
/* --------- Integrates rotational equations of motion for rods (BAOAB) ---------
 * BAOAB Scheme from DOI: 10.1093/amrx/abs010
 * Equations of motion from DOI: 10.1103/PhysRevE.50.1232
 */
void integrate_orientations_rods(void){
	int i,d;
	double du_B[DIM];				// Brownian reorientation vector
	double du_F[DIM];				// Reorientation due to interaction forces
	double rvec[DIM];				// Random vector only used for calculating perpendicular vectors
	double G_rands_at_t[3];			// (para, perp1, perp2) Gaussian random numbers for BAOAB
	double G_sum_vec[DIM];			//

	for(i=0;i<n_part;i++){
		/* Save orientation as previous orientation */
		for(d=0;d<DIM;d++){
			part[i].prev_or[d]=part[i].or[d];
		}

		/* Brownian reorientations */
		for(d=0;d<DIM;d++){
			G_rands_at_t[d]=gaussianrand();
			G_sum_vec[d]=random_force_prefactor_rota_rod*(part[i].prevrands_rota[d]+G_rands_at_t[d]);
		}
		crossprod(du_B,G_sum_vec,part[i].or);
		for(d=0;d<DIM;d++){part[i].prevrands_rota[d]=G_rands_at_t[d];}
		// 		printf(" t i du_B: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n",time_r,i,du_B[0],du_B[1],du_B[2]);

		// Calculate WCA part and truncate if needed
		crossprod(du_F,part[i].torque,part[i].or); // du_F = T x u
		for(d=0;d<DIM;d++){du_F[d]*=force_prefactor_rota;}
		T_softened += vectruncate(du_F, max_torque_rotation_sq);
		T_evaluated++;
		//		printf(" t i du_F: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n",time_r,i,du_F[0],du_F[1],du_F[2]);

		/* Integrate equation of motion of the orientations */
		for(d=0;d<DIM;d++){
			part[i].or[d]+=(du_F[d]+du_B[d]);
		}

		if(FORCETROUBLE){
			printf("du_F={%lf,%lf,%lf}\n",du_F[0],du_F[1],du_F[2]);
			printf("du_B={%lf,%lf,%lf}\n",du_B[0],du_B[1],du_B[2]);
		}

		/* Random reorientation changed the norm, so we normalize */
		normalize(part[i].or);

		/* Calculate new perpendicular vectors */
		fillrvec(rvec);
		normalize(rvec);
		crossprod(part[i].orper1,part[i].or,rvec);
		normalize(part[i].orper1);
		crossprod(part[i].orper2,part[i].or,part[i].orper1);
		normalize(part[i].orper2);
	}
}
/* --------- Integrates translational equations of motion for rods (BAOAB) ---------
 * BAOAB Scheme from DOI: 10.1093/amrx/abs010
 */
void integrate_positions_rods_on_sphere(void){
	int i,k,l,d;
	double uu[3][3]; 					// Dyadic product of the orientation of i
	double WCA_friction[3][3];			// One over the friction tensor
	double dx_B[3];						// Brownian displacement vector
	double dx_F[3];						// Displacement due to interaction forces
	double dx_A[3];						// Active displacement
	double G_rands_at_t[3];				// (para, perp1, perp2) Gaussian random numbers for BAOAB
	double dx_3D[3],dx_on_sphere[3];
	double normal[3],dx_normal_to_sphere;

	/* "Standard" equations of motion */
	for(i=0;i<n_part;i++){
		/* Save position as previous position */
		for(d=0;d<DIM;d++){
			part[i].prev_pos[d]=part[i].pos[d];
		}

		/* Calculate translational friction tensor */
		dyadic(uu,part[i].or,part[i].or); // calculate value of uu
		for(k=0;k<3;k++){
			for(l=0;l<3;l++){
				WCA_friction[k][l]=force_prefactor_para*uu[k][l]+force_prefactor_perp*(Identity[k][l]-uu[k][l]);
			}
		}

		/* Brownian displacements */
		for(k=0;k<3;k++){
			G_rands_at_t[k]=gaussianrand();
		}
		for(d=0;d<3;d++){
			dx_B[d]=random_force_prefactor_para_rod*(part[i].prevrands_trans[0]+G_rands_at_t[0])*part[i].or[d]+
					random_force_prefactor_perp_rod*(part[i].prevrands_trans[1]+G_rands_at_t[1])*part[i].orper1[d]+
					random_force_prefactor_perp_rod*(part[i].prevrands_trans[2]+G_rands_at_t[2])*part[i].orper2[d];
		}
		for(k=0;k<3;k++){
			part[i].prevrands_trans[k]=G_rands_at_t[k];
		}
		//printf(" t i dx_B: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n\n",time_r,i,dx_B[0],dx_B[1],dx_B[2]);

		// Calculate WCA part and truncate if needed
		mvinprod(dx_F,WCA_friction,part[i].force);
		F_softened += vectruncate(dx_F, max_force_translation_sq);
		F_evaluated++;
		//printf(" t i dx_F: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n",time_r,i,dx_F[0],dx_F[1],dx_F[2]);

		/* Active displacement */
		for(d=0;d<3;d++){
			dx_A[d]=active_prefactor_rod*part[i].or[d];
		}
		//printf(" t i dx_A: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n",time_r,i,dx_A[0],dx_A[1],dx_A[2]);

		/* Get total displacement in full 3D */
		for(d=0;d<3;d++){
			dx_3D[d]=(dx_F[d]+dx_B[d]+dx_A[d]);
		}

		/* Project displacement onto tangent plane of sphere */
		project_tangent_to_sphere(dx_on_sphere,part[i].pos,dx_3D);

		/* Integrate equation of motion of the positions in tangent plane */
		for(d=0;d<3;d++){
			part[i].pos[d]+=dx_on_sphere[d];
		}

		/* Moving in tangent plane slightly shifts particle off of sphere, so correct for that */
		dx_normal_to_sphere=RADIUS-norm(part[i].pos);
		direction(normal,part[i].pos);
		for(d=0;d<3;d++){
			part[i].pos[d]+=normal[d]*dx_normal_to_sphere;
		}

		/* Fix orientation for new position */
		double new_or[3];
		project_tangent_to_sphere(new_or,part[i].pos,part[i].or);
		normalize(new_or);
		memcpy(part[i].or,new_or,3*sizeof(double));

		/* Put particles back into box if the step takes them outside */
		put_particle_back_in_box(i);
	}
}
/* --------- Integrates rotational equations of motion for rods (BAOAB) ---------
 * BAOAB Scheme from DOI: 10.1093/amrx/abs010
 */
void integrate_orientations_rods_on_sphere(void){
	int i,d;
	double du_B[3];						// Brownian reorientation vector
	double du_F[3];						// Reorientation due to interaction forces
	double rvec[3];						// Random vector only used for calculating perpendicular vectors
	double G_rands_at_t[3];				// (para, perp1, perp2) Gaussian random numbers for BAOAB
	double du_3D[3],du_on_sphere[3];

	for(i=0;i<n_part;i++){
		/* Brownian reorientations */
		for(d=0;d<3;d++){G_rands_at_t[d]=gaussianrand();}
		for(d=0;d<3;d++){
			du_B[d]=random_force_prefactor_rota_rod*(part[i].prevrands_rota[0]+G_rands_at_t[0])*part[i].or[d]+
					random_force_prefactor_rota_rod*(part[i].prevrands_rota[1]+G_rands_at_t[1])*part[i].orper1[d]+
					random_force_prefactor_rota_rod*(part[i].prevrands_rota[2]+G_rands_at_t[2])*part[i].orper2[d];
		}
		for(d=0;d<3;d++){part[i].prevrands_rota[d]=G_rands_at_t[d];}
		//printf(" t i du_B: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n",time_r,i,du_B[0],du_B[1],du_B[2]);

		// Calculate WCA part and truncate if needed
		crossprod(du_F,part[i].torque,part[i].or); // du_F = T x u
		for(d=0;d<3;d++){du_F[d]*=force_prefactor_rota;}
		T_softened += vectruncate(du_F, max_torque_rotation_sq);
		T_evaluated++;
		//printf(" t i du_F: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n",time_r,i,du_F[0],du_F[1],du_F[2]);

		/* Get total reorientation in full 3D */
		for(d=0;d<3;d++){
			du_3D[d]=(du_F[d]+du_B[d]);
		}

	// 		/* Project reorientation onto normal of sphere */
	// 		double du_val,rhat[3],rhat_X_u[3];
	// 		project_normal_to_sphere(&du_val,part[i].pos,du_3D);
	//
	// 		/* Rotate around rhat X u to ensure orientation stays tangent to sphere */
	// 		direction(rhat,part[i].pos);
	// 		crossprod(rhat_X_u,rhat,part[i].or);
	// 		for(d=0;d<3;d++){
	// 			du_on_sphere[d]=du_val*rhat_X_u[d];
	// 		}

		project_tangent_to_sphere(du_on_sphere,part[i].pos,du_3D);

		/* Integrate equation of motion of the orientations */
		for(d=0;d<3;d++){
			part[i].or[d]+=du_on_sphere[d];
		}

	// 		printf("%f %f %f\n",du_on_sphere[0],du_on_sphere[1],du_on_sphere[2]);
	// 		printf("%f %f %f\n",part[i].pos[0],part[i].pos[1],part[i].pos[2]);
	// 		printf("inprod: %f\n",inprod(du_on_sphere,part[i].pos));

		/* Random reorientation changed the norm, so we normalize */
		normalize(part[i].or);

		/* Calculate new perpendicular vectors */
		fillrvec(rvec);
		normalize(rvec);
		crossprod(part[i].orper1,part[i].or,rvec);
		normalize(part[i].orper1);
		crossprod(part[i].orper2,part[i].or,part[i].orper1);
		normalize(part[i].orper2);
	}
}
/* --------- Integrates translational equations of motion for spheres (BAOAB) ---------
 * BAOAB Scheme from DOI: 10.1093/amrx/abs010
 */
void integrate_positions_spheres(void){
	int i,d;
	double dx_B[DIM];						// Brownian displacement vector
	double dx_F[DIM];						// Displacement due to interaction forces
	double dx_A[DIM];						// Active displacement
	double G_rands_at_t[DIM];				// Gaussian random vector for BAOAB


	for(i=0;i<n_part;i++){
		/* Save position as previous position */
		for(d=0;d<DIM;d++){
			part[i].prev_pos[d]=part[i].pos[d];
		}

		/* Brownian displacements */
		for(d=0;d<DIM;d++){
			G_rands_at_t[d]=gaussianrand();
			dx_B[d]=random_force_prefactor_t_sph*(part[i].prevrands_trans[d]+G_rands_at_t[d]); // BAOAB
			//dx_B[d]=2*random_force_prefactor_t_sph*G_rands_at_t[d]; // Euler-Maruyama
			part[i].prevrands_trans[d]=G_rands_at_t[d];
		}
		//printf(" t i dx_B: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n\n",time_r,i,dx_B[0],dx_B[1],dx_B[2]);

		// Calculate WCA part and truncate if needed
		for(d=0;d<DIM;d++){
			dx_F[d]=force_prefactor_sph*part[i].force[d];
		}
		F_softened += vectruncate(dx_F, max_force_translation_sq);
		F_evaluated++;
		//printf(" t i dx_F: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n",time_r,i,dx_F[0],dx_F[1],dx_F[2]);

		/* Active displacement */
		for(d=0;d<DIM;d++){
			dx_A[d]=active_prefactor_sph*part[i].or[d];
		}

		/* Integrate equation of motion of the positions */
		for(d=0;d<DIM;d++){
			part[i].pos[d]+=(dx_F[d]+dx_B[d]);
			if(part[i].isactive){part[i].pos[d]+=dx_A[d];}
		}

		/* Integrate equation of motion of the positions */
		for(d=0;d<DIM;d++){
			part[i].rpos[d]+=(dx_F[d]+dx_B[d]);
			if(part[i].isactive){part[i].rpos[d]+=dx_A[d];}
		}

		/* Put particles back into box if the step takes them outside */
		put_particle_back_in_box(i);
	}
}
/* --------- Integrates rotational part equations of motion for spheres (BAOAB) ---------
 * BAOAB Scheme from DOI: 10.1093/amrx/abs010
 * For active Brownian spheres we do not deal with torques, so these are absent from the equations
 * of motion.
 */
void integrate_orientations_spheres(void){
	int i,d;
	double du_B[DIM];						// Brownian reorientation vector
	double G_rands_at_t[DIM];				// Gaussian random vector for BAOAB
	double G_sum_vec[DIM];					//

	for(i=0;i<n_part;i++){
		/* Save orientation as previous orientation */
		for(d=0;d<DIM;d++){
			part[i].prev_or[d]=part[i].or[d];
		}

		/* Brownian reorientations */
		for(d=0;d<DIM;d++){
			G_rands_at_t[d]=gaussianrand();
			G_sum_vec[d]=random_force_prefactor_r_sph*(part[i].prevrands_rota[d]+G_rands_at_t[d]);
			//G_sum_vec[d]=2*random_force_prefactor_r_sph*G_rands_at_t[d];
		}
		crossprod(du_B,G_sum_vec,part[i].or);
		for(d=0;d<DIM;d++){part[i].prevrands_rota[d]=G_rands_at_t[d];}
		// 		printf(" t i du_B: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n",time_r,i,du_B[0],du_B[1],du_B[2]);

		/* Integrate equation of motion of the orientations */
		for(d=0;d<DIM;d++){
			part[i].or[d]+=du_B[d];
		}

		if(FORCETROUBLE){
			printf("du_B={%lf,%lf,%lf}\n",du_B[0],du_B[1],du_B[2]);
		}

		/* Random reorientation changed the norm, so we normalize */
		normalize(part[i].or);
	}
}
/* --------- Integrates translational equations of motion for the mesh (BAOAB) ---------
 *
 */
void integrate_positions_mesh(void){
	int i,d;
	double G_rands_at_t_v[DIM]; 	// (x,y,z) of random gaussian numbers
	double dx_B[DIM];				// Brownian displacement vector
	double dx_F[DIM];				// Displacement due to interaction forces

	double area_scale_factor=pow(mesh.faces[0].area,-1./4.);

	/* First integrate equations of motion for the vertices */
	for(i=0;i<mesh.nvertices;i++){

		/* Interaction part */
		for(d=0;d<DIM;d++){
			dx_F[d]=( force_prefactor_sph/sqrt(mesh.faces[0].area) )*mesh.vertices[i].force[d];
		}
		//printf(" t i dx_F: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n",time_r,i,dx_F[0],dx_F[1],dx_F[2]);

		/* Brownian displacements */
		for(d=0;d<DIM;d++){
			G_rands_at_t_v[d]=gaussianrand();
			dx_B[d]=random_force_prefactor_t_sph*area_scale_factor*(mesh.vertices[i].prevrands[d]+G_rands_at_t_v[d]);
			mesh.vertices[i].prevrands[d]=G_rands_at_t_v[d];
		}
		//printf(" t i dx_B: %6.06lf %.04d %6.06lf %6.06lf %6.06lf \n\n",time_r,i,dx_B[0],dx_B[1],dx_B[2]);

		/* Integrate equation of motion of the positions */
		for(d=0;d<DIM;d++){
			mesh.vertices[i].pos[d]+=(dx_F[d]+0*dx_B[d]);
		}

		/* Put vertices back into the box */
		putvertexinbox(i);
	}

	/* Then update all edges and faces based on the new vertex positions */
	update_mesh_properties();
}

/*========================== Sampling functions ==========================*/

/* --------- Measures the pressure of the system ---------
 * Measures the global dimensionless pressure p * d^3 / kT
 * Pressure calculation for active systems in PBC taken from
 * Winkler et al. 2015, DOI: 10.1039/C5SM01412C
 */
void measure_pressure(void){
	double p_ideal_gas,p_virial,p_swim;
	long int i;

	/* The ideal gas pressure is simple */
	p_ideal_gas = density;
	//double p_id_active = density *(1+self_propulsion_velocity*self_propulsion_velocity/(6*diffusion_r_sph*diffusion_t_sph));
	//printf("  %12.012lf - pressure ideal active gas\n",p_id_active);

	/* The virial contributions have been computed in the force calculations already */
	p_virial = (1.0/(box.volume*DIM)) * virial;
	virial_pressure.current=p_virial;
	//printf("  %12.012lf - Virial pressure from interactions\n",p_virial);

	/* Calculate the swim stress */
	for(i=0;i<n_part;i++){
		swim_virial+=active_force*inprod(part[i].or,part[i].rpos);
	}
	/* The swim pressure is calculated through the swim virial */
	p_swim = (1.0/(box.volume*DIM)) * swim_virial;
	swim_pressure.current=p_swim;
	//printf("  %12.012lf - Swim pressure from virial term\n",p_swim);


	/* Total pressure is then the sum of the three */
	pressure.current = p_ideal_gas + p_virial + p_swim;

	/* Update statistics (mean, variance) */
	update_observable( &pressure );
	update_observable( &virial_pressure );
	update_observable( &swim_pressure );
	//printf("  %12.012lf - Virial pressure from interactions\n",virial_pressure.mean/p_id_active);
	//printf("  %12.012lf - Swim pressure from virial term\n",swim_pressure.mean/p_id_active);
}
/* --------- Measures the effective swim speed of the system ---------
 * This function divides the simulation volume into bins and calculates the average velocity
 * in each bin as if it were a continuum field. It then calculates a vorticity from this velocity
 * field by applying a discretized rotation operator.
 * NOTE: currently the velocity calculated here is v(t-1/2)=|r(t)-r(t-1)|/dt. I don't expect this
 * to make a difference, but it might. If needed, the velocity can be synchronized to the current
 * positions by making sure it gets measured after the forces are updated and the displacements
 * are calculated, but before the particle gets moved. This would also require calculating the
 * velocity with a second-order derivative: v(t)=|r(t+1)-2r(t)+r(t-1)|/(2*dt)
 */
void measure_velocity_and_vorticity_fields(void){
	int d;
	static char first=1;
	long int i,x,y,z;
	double dvec[DIM];
	double inverse_dt=1.0/dt;
	static double binwidth,overlap;
	static double bx,by,bz,ibx,iby,ibz; // actual bin widths
	static long int nx,ny,nz,size;
	long int obin=0;


	/* Initialize the histogram */
	if(first){
		binwidth=lod; // From doi: 10.1073/pnas.1202032109
		overlap=1.31*binwidth; // A larger bin width to overlap the bins, apparantly better res.

		/* Calculate number of bins along each direction */
		nx = floor(box.L[0]/binwidth);
		ny = floor(box.L[1]/binwidth);
		nz = floor(box.L[2]/binwidth);
		/* Prevent bad stuff from happening */
		if(nx==0){nx=1;}
		if(ny==0){ny=1;}
		if(nz==0){nz=1;}
		size = nx*ny*nz*DIM; // size of array
		/* Get actual bin widths */
		bx = box.L[0]/nx;
		by = box.L[1]/ny;
		bz = box.L[2]/nz;
		ibx = 1.0/bx;
		iby = 1.0/by;
		ibz = 1.0/bz;
		first=0;
	}

	double velocity_field[size];
	int velocity_field_counts[nx*ny*nz];
	double vorticity_field[size];

	for(i=0;i<size;i++){
		velocity_field[i]=0.;
		vorticity_field[i]=0.;
	}
	for(i=0;i<nx*ny*nz;i++){velocity_field_counts[i]=0;}

	/* First we measure the velocity field with overlapping bins */
	for(i=0;i<n_part;i++){

		/* Measure the velocity */
		nearestimage(part[i].prev_pos,part[i].pos,dvec);
		//printf("norm(dvec), dt: %lf %lf\n",norm(dvec),dt);

		/* Find the center bin */
		x = floor((part[i].pos[0]+box.hL[0])*ibx);
		y = floor((part[i].pos[1]+box.hL[1])*iby);
		z = floor((part[i].pos[2]+box.hL[2])*ibz);

		/* Add to array */
		for(d=0;d<DIM;d++){ velocity_field[DIM*(z+nz*(y+ny*x))+d] += dvec[d]*inverse_dt; }
		//printf("vfield @ (%d %d %d): %4.04lf %4.04lf %4.04lf)\n",x,y,z,velocity_field[bin][0],velocity_field[bin][1],velocity_field[bin][2]);

		/* Check whether position also falls into overlapping surrounding bins */
		if( part[i].pos[0] < (x-1)*binwidth + overlap ){
			/* First find the right bin index with periodic boundary conditions */
			obin = DIM*(z+nz*(y+ny*  (x-1)<0?(nx-1):(x-1)  ));
			/* Then add to bin */
			velocity_field_counts[obin]++;
			for(d=0;d<DIM;d++){ velocity_field[obin+d] += dvec[d]*inverse_dt; }
		}
		/* Likewise for x+1 */
		if( part[i].pos[0] > (x+1)*binwidth - overlap ){
			obin = DIM*(z+nz*(y+ny*  (x+1)>=nx?0:(x+1)  ));
			velocity_field_counts[obin]++;
			for(d=0;d<DIM;d++){ velocity_field[obin+d] += dvec[d]*inverse_dt; }
		}

		/* Likewise for y-1 */
		if( part[i].pos[1] < (y-1)*binwidth + overlap ){
			obin = DIM*(z+nz*(  (y+1)<0?(ny-1):(y-1)  +ny*x));
			velocity_field_counts[obin]++;
			for(d=0;d<DIM;d++){ velocity_field[obin+d] += dvec[d]*inverse_dt; }
		}
		/* Likewise for y+1 */
		if( part[i].pos[1] > (y+1)*binwidth - overlap ){
			obin = DIM*(z+nz*(  (y+1)>=ny?0:(y+1)  +ny*x));
			velocity_field_counts[obin]++;
			for(d=0;d<DIM;d++){ velocity_field[obin+d] += dvec[d]*inverse_dt; }
		}

		/* Likewise for z-1 */
		if( part[i].pos[2] < (z-1)*binwidth + overlap ){
			obin = DIM*(  (z+1)<0?(nz-1):(z-1)  +nz*(y+ny*x));
			velocity_field_counts[obin]++;
			for(d=0;d<DIM;d++){ velocity_field[obin+d] += dvec[d]*inverse_dt; }
		}
		/* Likewise for z+1 */
		if( part[i].pos[2] > (z+1)*binwidth - overlap ){
			obin = DIM*(  (z+1)>=nz?0:(z+1)  +nz*(y+ny*x));
			velocity_field_counts[obin]++;
			for(d=0;d<DIM;d++){ velocity_field[obin+d] += dvec[d]*inverse_dt; }
		}
	}

	double inverse_swimspeed = 1.0/self_propulsion_velocity;
	for(x=0;x<nx;x++){
		for(y=0;y<ny;y++){
			for(z=0;z<nz;z++){
				for(d=0;d<DIM;d++){
					/* First divide by # of counts to obtain an average velocity */
					velocity_field[obin+d] /= velocity_field_counts[obin];
					/* Then normalize by swim speed */
					velocity_field[obin+d] *= inverse_swimspeed;
				}
			}
		}
	}

	save_vector_field(velocity_field,nx,ny,nz,DIM,"Velocity");

	/* Measure the vorticity by taking the rotation of the coarse-grained velocity field */
	int x_min,x_plus,y_min,y_plus,z_min,z_plus;
	double dz_of_vy,dy_of_vz,dz_of_vx,dx_of_vz,dy_of_vx,dx_of_vy;
	for(x=0;x<nx;x++){
		x_min  = x-1<0   ? nx-1 : x-1; // Periodicity
		x_plus = x+1>=nx ? 0    : x+1;
		for(y=0;y<ny;y++){
			y_min  = y-1<0   ? ny-1 : y-1;
			y_plus = y+1>=ny ? 0    : y+1;
			for(z=0;z<nz;z++){
				z_min  = z-1<0   ? nz-1 : z-1;
				z_plus = z+1>=nz ? 0    : z+1;

				/* 2nd order central derivatives for the rotation */ 	// TODO Find a less messy way.
				dz_of_vy = (0.5*ibz)*(velocity_field[DIM*(z_plus+nz*(y+ny*x))+1]-2*velocity_field[DIM*(z+nz*(y+ny*x))+1]+velocity_field[DIM*(z_min+nz*(y+ny*x))+1]);
				dy_of_vz = (0.5*iby)*(velocity_field[DIM*(z+nz*(y_plus+ny*x))+2]-2*velocity_field[DIM*(z+nz*(y+ny*x))+2]+velocity_field[DIM*(z+nz*(y_min+ny*x))+2]);
				vorticity_field[DIM*(z+nz*(y+ny*x))+0] = dy_of_vz - dz_of_vy;

				dz_of_vx = (0.5*ibz)*(velocity_field[DIM*(z_plus+nz*(y+ny*x))+0]-2*velocity_field[DIM*(z+nz*(y+ny*x))+0]+velocity_field[DIM*(z_min+nz*(y+ny*x))+0]);
				dx_of_vz = (0.5*ibx)*(velocity_field[DIM*(z+nz*(y+ny*x_plus))+2]-2*velocity_field[DIM*(z+nz*(y+ny*x))+2]+velocity_field[DIM*(z+nz*(y+ny*x_min))+2]);
				vorticity_field[DIM*(z+nz*(y+ny*x))+1] = dz_of_vx - dx_of_vz;

				dy_of_vx = (0.5*iby)*(velocity_field[DIM*(z+nz*(y_plus+ny*x))+0]-2*velocity_field[DIM*(z+nz*(y+ny*x))+0]+velocity_field[DIM*(z+nz*(y_min+ny*x))+0]);
				dx_of_vy = (0.5*ibx)*(velocity_field[DIM*(z+nz*(y+ny*x_plus))+1]-2*velocity_field[DIM*(z+nz*(y+ny*x))+0]+velocity_field[DIM*(z+nz*(y+ny*x_min))+1]);
				vorticity_field[DIM*(z+nz*(y+ny*x))+2] = dx_of_vy - dy_of_vx;
			}
		}
	}

	save_vector_field(vorticity_field,nx,ny,nz,DIM,"Vorticity");
}
/* --------- Measures the velocity probability distribution ---------
 *
 */
void measure_velocity_probability(void){
	long int i,bin;
	static double binwidth,inverse_binwidth;
	double one_over_npart=1.0/n_part;
	static char first=1;
	double dvec[DIM],v;
	double inverse_dt=1.0/dt;
	thistogram hist_counts;

	long int BINS = 100;
	// TODO: Set a fixed binwidth (e.g. 0.1*self_propulsion_velocity) and expand histogram dynamically

	/* Initialize the histogram */
	if(first){
		binwidth=2*100*self_propulsion_velocity/BINS;
		inverse_binwidth=1.0/binwidth;
		create_histogram( &hist_velocity_probability , BINS );
		for(i=0;i<hist_velocity_probability.n_bins;i++){
			//hist_velocity_probability.xvalue[i] = self_propulsion_velocity*(((double) i)/BINS-0.5);
			hist_velocity_probability.xvalue[i] = self_propulsion_velocity*(((double) i)/BINS);
		}
		first=0;
	}

	/* Measure and bin the velocities by looking at the current and previous positions */
	create_histogram( &hist_counts , BINS );
	for(i=0;i<n_part;i++){
		nearestimage(part[i].prev_pos,part[i].pos,dvec);
		v=norm(dvec)*inverse_dt;
		/* Add to histogram */
		//bin = 0.5*BINS + (long int) floor( v*inverse_binwidth ); // atm measuring only norm of v
		bin = (long int) floor( v*inverse_binwidth );
		if(bin>=100 || bin<0){continue; /* bad value, probably happens resolving initial overlaps */}
		//printf("bin val: %d %e\n",bin,hist_counts.value[bin].current);
		hist_counts.value[bin].current+=1.;
		//printf("bin val: %d %e\n",bin,hist_counts.value[bin].current);
	}
	/* Normalize histogram */
	for(i=0;i<hist_velocity_probability.n_bins;i++){
		hist_velocity_probability.value[i].current=one_over_npart*hist_counts.value[i].current;
	}
	/* Update statistics (mean, variance) */
	update_histogram( &hist_velocity_probability );
	//printf("v, self_propulsion_velocity: %4.08lf %4.08lf\n",v,self_propulsion_velocity);
	destroy_histogram( &hist_counts );
}
/* --------- Measures the spatial velocity correlation function ---------
 * The spatial velocity correlation function describes the correlation between the velocities
 * of two particles at different distances. Its main use in this simulation is to measure for
 * signatures of meso-scale turbulence, as in the following paper:
 *
 * Meso-scale turbulence in living fluids
 * H. Wensink et al.
 * DOI: 10.1073/pnas.1202032109
 */
void measure_spatial_velocity_correlation(void){
	long int i,j,bin;
	double nxij[DIM];
	static char first=1;
	static double binwidth;
	double v1[DIM],v2[DIM],corr;
	double inverse_dt=1.0/dt;

	int BINS = 200;

	if(first){
		binwidth=maxsep/BINS;
		/* Create histograms */
		create_histogram( &hist_spatial_velocity_correlation , BINS );
		for(i=0;i<hist_spatial_velocity_correlation.n_bins;i++){
			hist_spatial_velocity_correlation.xvalue[i] = (i+.5)*binwidth;
		}
		first=0;
	}
	//printf("BINS, maxsep, binwidth: %d %lf %lf\n",BINS,maxsep,binwidth);

	/* Calculate the histogram */
	for(i=0;i<n_part-1;i++){
		for(j=i+1;j<n_part;j++){
			/* Nearest image convention for CM distance */
			nearestimage(part[i].pos,part[j].pos,nxij);
			bin=(int)floor(norm(nxij)/binwidth); // dist can exceed max sep. by sqrt(3) due to cubic simulation volume
			if(bin<hist_spatial_velocity_correlation.n_bins){
				/* Calculate the equal-time spatial velocity correlation */
				nearestimage(part[i].prev_pos,part[i].pos,v1);
				nearestimage(part[j].prev_pos,part[j].pos,v2);
				corr=inprod(v1,v2)*inverse_dt*inverse_dt;
				/* Update statistics */
				hist_spatial_velocity_correlation.value[bin].current=corr;
				update_observable( &hist_spatial_velocity_correlation.value[bin] );
				//printf("bin nhist: %d %lf\n",bin,hist_spatial_velocity_correlation.value[bin]);
			}
		}
	}
	return;
}
/* --------- Measures the spatial orientation correlation function ---------
 * The spatial orientation correlation function describes the correlation between the orientation
 * of two particles separated by a certain radial distance. Its main use in this simulation is to
 * characterize the occurance or strength of polarity sorting: the tendency of self-propelled
 * particles to organize into clusters with the same polar orientation.
 */
void measure_spatial_orientation_correlation(void){
	long int i,j,bin;
	double nxij[DIM];
	static char first=1;
	static double binwidth;
	double corr;

	int BINS = 200;

	if(first){
		binwidth=maxsep/BINS;
		/* Create histograms */
		create_histogram( &hist_spatial_orientation_correlation , BINS );
		for(i=0;i<hist_spatial_orientation_correlation.n_bins;i++){
			hist_spatial_orientation_correlation.xvalue[i] = (i+.5)*binwidth;
		}
		first=0;
	}
	//printf("BINS, maxsep, binwidth: %d %lf %lf\n",BINS,maxsep,binwidth);

	/* Calculate the histogram */
	for(i=0;i<n_part;i++){
		for(j=i;j<n_part;j++){
			/* Nearest image convention for CM distance */
			nearestimage(part[i].pos,part[j].pos,nxij);
			bin=(int)floor(norm(nxij)/binwidth); // dist can exceed max sep. by sqrt(3) due to cubic simulation volume
			if(bin<hist_spatial_orientation_correlation.n_bins){
				/* Calculate the equal-time spatial orientation correlation */
				corr=inprod(part[i].or,part[j].or);
				/* Update statistics */
				hist_spatial_orientation_correlation.value[bin].current=corr;
				update_observable( &hist_spatial_orientation_correlation.value[bin] );
				//printf("bin nhist: %d %lf\n",bin,hist_spatial_orientation_correlation.value[bin]);
			}
		}
	}
	return;
}
/* --------- Measures the global (nematic and polar) order in the system ---------
 *
 */
void measure_nematic_and_polar_order(void){
	long int i;
	short int d1,d2;

	/* Measure the global nematic order in the system */
	double Q[DIM][DIM];
	for(d1=0;d1<DIM;d1++){
		for(d2=0;d2<DIM;d2++){
			Q[d1][d2]=0.;
			for(i=0;i<n_part;i++){
				Q[d1][d2]+=1.5*part[i].or[d1]*part[i].or[d2]-0.5*Identity[d1][d2];
			}
			Q[d1][d2]/=n_part;
		}
	}
	nematic_order.current = eig(Q);

	// Calculate nematic director: eigenvector of Q corresponding to nematic order parameter
	// eigv(&director, Q)

	// Polar order is inner product of particle orientation with nematic director
	// for(i=0; i<n_part; i++){
	// 	order += inprod(director, part[i].or);
	// }
	// order /= n_part;

	/* Update statistics (mean, variance) */
	update_observable( &nematic_order );
	//update_observable( &polar_order );2/
}
/* --------- Measures the local (nematic) order in the system ---------
 *
 */
void measure_local_nematic_order(void){
	double rmin2,Qi[DIM][DIM],rij[DIM];
	double armi[DIM],armj[DIM];
	int i,ci,pnb,d1,d2,k,cnb,l,j;

	/* Measure the local nematic order based on "nearest neighbours" of particles */
	for(i=0;i<n_part;i++){
		ci=part[i].cell;
		pnb=0;
		for(d1=0;d1<DIM;d1++){
			for(d2=0;d2<DIM;d2++){
				Qi[d1][d2]=0.;
			}
		}
		for(k=neighbouring_cells-1;k>=0;k--){
			cnb=cells[ci].neighbours[k];
			for(l=cells[cnb].n-1;l>=0;l--){
				j=cells[cnb].particles[l];
				nearestimage(part[i].pos,part[j].pos,rij);
				/* Check spherical volume around CM, or */
				//nrm=norm(rij);
				//if(nrm<max_interaction_length_2rods){
				/* check all within a certain surface-to-surface distance */
				rmin2=distsq_rods(&part[i],&part[j],rij,armi,armj);
				if(rmin2<8*max_interaction_length_2rods*max_interaction_length_2rods){
					for(d1=0;d1<DIM;d1++){
						for(d2=0;d2<DIM;d2++){
							Qi[d1][d2]+=1.5*part[j].or[d1]*part[j].or[d2]-0.5*Identity[d1][d2];
						}
					}
					pnb++;
				}
			}
		}
		part[i].nem=eig(Qi)/pnb;
	}
}
/* --------- Measures the local density in the system ---------
 * For every rod, checks within a certain distance of its line segment how many particles are near.
 * From this number it assigns a local density to the particle that can be used in visualizations.
 */
void measure_local_packfrac(void){
	double positions[n_part][3];
	// Copy particle positions to a local array
	for(int i = 0; i < n_part; i++){
		for(int d = 0; d < DIM; d++){
			positions[i][d] = part[i].pos[d];
		}
	}
	// Call on the voro++ library through a wrapper function and get the cell volumes
	double voro_volumes[n_part];
	c_voro_get_volumes(
		 -box.hL[0], +box.hL[0],
		 -box.hL[1], +box.hL[1],
		 -box.hL[2], +box.hL[2],
		 n_part,
		 positions,
		 voro_volumes
	);
	double volume_one_particle = Vpart / (double) n_part;
	for(int i = 0; i < n_part; i++){
		part[i].localpackfrac = volume_one_particle / voro_volumes[i];
	}

}
/* --------- Measures the local density in the system in a 3D grid ---------
 * For every rod, checks within a certain distance of its line segment how many particles are near.
 * From this number it assigns a local density to the particle that can be used in visualizations.
 */
void measure_local_density_3D(void){
	static long int nx,ny,nz;
	static double bwx,bwy,bwz,ibwx,ibwy,ibwz,ibvol;
	long int n_bins;
	double binwidth;
	static char first=1;
	long int x,y,z,i;

	if(first){
		/* Bins have size of 1 particle length */
		binwidth = lod;
		/* Calculate number of bins along each direction */
		nx = floor(box.L[0]/binwidth);
		ny = floor(box.L[1]/binwidth);
		nz = floor(box.L[2]/binwidth);
		/* Prevent bad stuff from happening */
		if(nx==0){nx=1;}
		if(ny==0){ny=1;}
		if(nz==0){nz=1;}
		n_bins = nx*ny*nz; // size of 3D histogram
		/* Get actual bin widths */
		bwx = box.L[0]/nx;
		bwy = box.L[1]/ny;
		bwz = box.L[2]/nz;
		ibwx = 1.0/bwx;
		ibwy = 1.0/bwy;
		ibwz = 1.0/bwz;
		ibvol= 1.0/(bwx*bwy*bwz); // one over the volume of the bin
		/* Create histogram */
		array_hist_create( &arrayhist_3D_density_field , n_bins, 3 , 1 );
		/* Assign bin positions */
		double hb = 0.5*binwidth; // Half a bin
		for(x=0;x<nx;x++){
			for(y=0;y<ny;y++){
				for(z=0;z<nz;z++){
					i = x*(ny*nz) + y*(nz) + z;
					arrayhist_3D_density_field.xvalue[i][0] = x*bwx + hb;
					arrayhist_3D_density_field.xvalue[i][1] = y*bwy + hb;
					arrayhist_3D_density_field.xvalue[i][2] = z*bwz + hb;
				}
			}
		}
		/* Done with initialization */
		first=0;
	}

	/* Clear current values so that we can increment */
	array_hist_clear_current(&arrayhist_3D_density_field);
	/* Count the number of particles in each bin */
	for(i=0;i<n_part;i++){
		/* Find the bin from the positions */
		x = (long int) floor((part[i].pos[0]+box.hL[0]) * ibwx);
		y = (long int) floor((part[i].pos[1]+box.hL[1]) * ibwy);
		z = (long int) floor((part[i].pos[2]+box.hL[2]) * ibwz);

		/* Add a count */
		arrayhist_3D_density_field.value[x*(ny*nz) + y*(nz) + z][0].current += 1;
	}
	/* Once all particles are binned, convert to number density */
	for(x=0;x<nx;x++){
		for(y=0;y<ny;y++){
			for(z=0;z<nz;z++){
				i = x*(ny*nz) + y*(nz) + z;
				arrayhist_3D_density_field.value[i][0].current *= ibvol;
			}
		}
	}

	// 	printf("some element: %lf %lf %lf   %lf %lf %ld\n",
	// 		   arrayhist_3D_density_field.xvalue[13][0],
	// 		   arrayhist_3D_density_field.xvalue[13][1],
	// 		   arrayhist_3D_density_field.xvalue[13][2],
	//
	// 		   arrayhist_3D_density_field.value[13][0].current,
	// 		   arrayhist_3D_density_field.value[13][0].mean,
	// 		   arrayhist_3D_density_field.value[13][0].samples
	// 	);

	/* Update the statistics */
	array_hist_update_all(&arrayhist_3D_density_field);
}
/* --------- Measures the effective swim speed of the system ---------
 * Particle interactions will cause particles not to swim with their bare swim speed. This function
 * measures the effective swim speed as a histogram.
 * NOTE: currently the velocity calculated here is v(t-1/2)=|r(t)-r(t-1)|/dt. I don't expect this
 * to make a difference, but it might. If needed, the velocity can be synchronized to the current
 * positions by making sure it gets measured after the forces are updated and the displacements
 * are calculated, but before the particle gets moved. This would also require calculating the
 * velocity with a second-order derivative: v(t)=|r(t+1)-2r(t)+r(t-1)|/(2*dt)
 */
void measure_velocity_profile_x(void){
	long int i,bin;
	static double binwidth,inverse_binwidth;
	static char first=1;
	double dvec[DIM],v;
	double inverse_dt=1.0/dt;

	/* Initialize the histogram */
	if(first){
		binwidth=box.L[0]/N_SWIMSPEED_BINS;
		inverse_binwidth=1.0/binwidth;
		create_histogram( &hist_effective_velocity , N_SWIMSPEED_BINS );
		for(i=0;i<hist_effective_velocity.n_bins;i++){
			hist_effective_velocity.xvalue[i] = (i+.5)*binwidth;
		}
		first=0;
	}

	/* Measure the effective swimming speed by looking at the current and previous positions */
	for(i=0;i<n_part;i++){
		nearestimage(part[i].prev_pos,part[i].pos,dvec);
		//v=norm(dvec)*inverse_dt;
		v=inprod(dvec,part[i].or)*inverse_dt;
		//printf("norm(dvec), dt: %lf %lf\n",norm(dvec),dt);

		/* Add to histogram */
		bin=(long int) floor( (part[i].pos[0]+box.hL[0])*inverse_binwidth );
		hist_effective_velocity.value[bin].current=v;
		/* Update statistics (mean, variance) */
		update_observable( &hist_effective_velocity.value[bin] );
	}
	//printf("v, self_propulsion_velocity: %4.08lf %4.08lf\n",v,self_propulsion_velocity);
}
/* --------- Measures the density along the x axis ---------
 * Break up the simulation volume into slabs and measure the density in each. Works best for a
 * simulation volume that is elongated along the x direction so that interfaces between phases
 * are typically formed in the y-z plane.
 */
void measure_density_profile_x(void){
	static char first=1;
	static double binwidth,inverse_binwidth,slab_volume,inverse_slab_volume;
	long int i,bin;

	/* Initialize the histogram */
	if(first){
		binwidth=box.L[0]/N_DENSITY_X_BINS;
		inverse_binwidth=1.0/binwidth;
		slab_volume=box.L[1]*box.L[2]*binwidth;
		inverse_slab_volume=1.0/slab_volume;
		create_histogram( &hist_density_x, N_DENSITY_X_BINS );
		for(i=0;i<hist_density_x.n_bins;i++){
			hist_density_x.xvalue[i] = (i+.5)*binwidth - box.hL[0];
		}
		first=0;
	}

	/* Clear current values of histogram for new measurement */
	for(i=0;i<hist_density_x.n_bins;i++){
		hist_density_x.value[i].current=0.0;
	}

	/* Collect particle positions into bins */
	for(i=0;i<n_part;i++){
		bin=(long int) floor( (part[i].pos[0]+box.hL[0])*inverse_binwidth );
		hist_density_x.value[bin].current+=1.0;
	}

	/* Normalize bins by the volume of the slab */
	for(i=0;i<hist_density_x.n_bins;i++){
		hist_density_x.value[i].current*=inverse_slab_volume;
	}

	/* Update the statistics (mean, variance) */
	update_histogram( &hist_density_x );
}
/* --------- Measures the pressure along the x axis ---------
 * Break up the simulation volume into slabs and measure the pressure in each. Works best for a
 * simulation volume that is elongated along the x direction so that interfaces between phases
 * are typically formed in the y-z plane.
 */
void measure_pressure_profile_x(void){
	int d,d2;
	static char first=1;
	static double binwidth,inverse_binwidth,slab_volume;
	// static double const_to_multiply_virial;
	// 	static double const_to_multiply_swim;
	long int i,bin;
	int n_part_per_bin[N_PRESSURE_X_BINS]={0};

	/* Initialize the histogram */
	if(first){
		binwidth=box.L[0]/N_PRESSURE_X_BINS;
		inverse_binwidth=1.0/binwidth;
		slab_volume=box.L[1]*box.L[2]*binwidth;
		// const_to_multiply_virial = 1.0 / ( slab_volume * DIM );
		// 		const_to_multiply_swim = ( friction_gamma * self_propulsion_velocity / (DIM * slab_volume) );
		/* Histogram for virial pressure */
		create_histogram( &hist_virial_pressure_x, N_PRESSURE_X_BINS );
		for(i=0;i<hist_virial_pressure_x.n_bins;i++){
			hist_virial_pressure_x.xvalue[i] = (i+.5)*binwidth - box.hL[0];
		}
		/* Histogram for swim pressure */
		create_histogram( &hist_swim_pressure_x, N_PRESSURE_X_BINS );
		for(i=0;i<hist_swim_pressure_x.n_bins;i++){
			hist_swim_pressure_x.xvalue[i] = (i+.5)*binwidth - box.hL[0];
		}
		first=0;
	}

	/* Clear current values of histogram for new measurement */
	for(i=0;i<N_PRESSURE_X_BINS;i++){
		hist_virial_pressure_x.value[i].current=0.0;
		hist_swim_pressure_x.value[i].current=0.0;
	}

	/* Collect particle virial contributions into bins */
	for(i=0;i<n_part;i++){
		bin=(long int) floor( (part[i].pos[0]+box.hL[0])*inverse_binwidth );
		/* Pressure uses the trace of the virial stress tensor */
		hist_virial_pressure_x.value[bin].current+=mtrace3D(part[i].virialstress);
		/* Construct swim pressure from orientation-position correlations */
		for(d=0;d<DIM;d++){
			for(d2=0;d<DIM;d++){
				part[i].swimstress[d][d2]=active_force*part[i].or[d]*part[i].rpos[d2];
			}
		}
		hist_swim_pressure_x.value[bin].current+=mtrace3D(part[i].swimstress);
		n_part_per_bin[bin]++;
	}

	for(i=0;i<N_PRESSURE_X_BINS;i++){
		// hist_virial_pressure_x.value[i].current*=const_to_multiply_virial;
		// hist_swim_pressure_x.value[i].current*=const_to_multiply_virial;
		//hist_swim_pressure_x.value[i].current*=const_to_multiply_swim;
	// 		if(n_part_per_bin[i]>0){ // prevent 0/0
	// 			hist_swim_pressure_x.value[i].current*=( 2.0/( ((double)n_part_per_bin[i])*friction_gamma) );
	// 			//printf("  %lf %lf\n",self_propulsion_velocity,hist_swim_pressure_x.value[i].current);
	// 		}
	// 		hist_swim_pressure_x.value[i].current+=self_propulsion_velocity;
	// 		hist_swim_pressure_x.value[i].current*=( friction_gamma * (n_part_per_bin[i]/slab_volume) * self_propulsion_velocity / (2 * DIM * diffusion_r_sph) );
	}

	/* Update the statistics (mean, variance) */
	update_histogram( &hist_virial_pressure_x );
	update_histogram( &hist_swim_pressure_x );
}
/* --------- Measures the radial distribution function ---------
 * Calculates the two-body radial distribution function. First calculates what it would be for
 * an ideal gas, then measures for the current positions of all particles in the simulation.
 */
void measure_gr(void){
	long int i,j,bin;
	double nxij[DIM];
	static char first=1;
	static double binwidth;
	double one_over_npart=1.0/n_part;
	thistogram hist_n;

	if(first){
		binwidth=maxsep/N_GR_BINS;
		/* Create histograms */
		create_histogram( &hist_gr_idgas , N_GR_BINS );
		create_histogram( &hist_gr , N_GR_BINS );
		for(i=0;i<hist_gr.n_bins;i++){
			hist_gr.xvalue[i] = (i+.5)*binwidth;
		}
		/* Calculate histogram for ideal gas case */
		for(j=0;j<hist_gr_idgas.n_bins;j++){
			hist_gr_idgas.value[j].current=4.*M_PI*density*(pow(((j+1)*binwidth),3.)-pow(j*binwidth,3.))/3.;
			//printf("j r nid: %d %lf %lf\n",j,(j+.5)*binwidth,nid[j]);
		}
		first=0;
	}
	//printf("N_GR_BINS, maxsep, binwidth: %d %lf %lf\n",N_GR_BINS,maxsep,binwidth);

	/* Calculate the histogram */
	create_histogram( &hist_n , N_GR_BINS );
	for(i=0;i<n_part-1;i++){
		for(j=i+1;j<n_part;j++){
			/* Nearest image convention for CM distance */
			nearestimage(part[i].pos,part[j].pos,nxij);
			bin=(int)floor(norm(nxij)/binwidth); // dist can exceed max sep. by sqrt(3) due to cube to sphere
			// printf("i j r bin npart: %d %d %lf %d %ld\n",i,j,norm(nxij),bin,n_part);
			if( bin >= hist_n.n_bins || bin < 0 ){ continue; } // So skip if outside range
			hist_n.value[bin].current += 2;
			//printf("bin nhist: %d %lf\n",bin,hist_n.value[bin]);
		}
	}

	/* Convert to g(r) */
	for(j=0;j<hist_gr.n_bins;j++){
		hist_gr.value[j].current=one_over_npart*(hist_n.value[j].current/hist_gr_idgas.value[j].current);
	}
	/* Update statistics (mean, variance) */
	update_histogram( &hist_gr );
	/* Destroy temporary histogram */
	destroy_histogram( &hist_n );
	return;
}
/* --------- Measures the mean square displacement ---------
 * This function builds up a histogram for the mean square displacement by tracking a number of
 * particles and measuring their displacement at different time lags. In pseudocode, it does:
 *
 * at time t, having p1->pos @ all previous t'
 * alloc bin for dt=t-0 in MSD histogram
 * for nMSD particles
 * 		store pos @ t
 * 		for all dt=t-t' bins in hist
 * 			dispvec = nearestimage(p1->pos @ t, p1->pos @ t')
 * 			dispsq = normsq( dispvec )
 * 			add dispsq to average in bin dt=t-t'
 * 			add dispsq to variance in bin dt=t-t'
 *
 * Main problem is that we need to store positions for all time bins in RAM, which is a lot.
 * So we should limit that somehow, e.g. by limiting number of samples, by limiting number of
 * particles for which it is done, or limiting for which time lags stuff gets calculated.
 */
void measure_MSD(double time){
	static char first=1;
	static double time_first;
	static char stop_growing_array_and_hist=0;
	int d;
	double dispvec[DIM];
	long int t,tlag,p,pstart;
	static long int delta_t_max=0;

	pstart = (NPARTS_TO_TRACK_MSD>n_part) ? n_part : NPARTS_TO_TRACK_MSD;

	if(first){
		/* Allocate memory for one time lag */
		create_histogram( &hist_MSD , 1 );
		pos_at_t_of_p=calloc(1,sizeof(*pos_at_t_of_p));
		pos_at_t_of_p[0]=calloc(pstart,sizeof( *(pos_at_t_of_p[0]) ));
		//printf("alloc'd %ld bytes for pointer\n",sizeof(*pos_at_t_of_p) );
		//printf("alloc'd %ld bytes for element\n",pstart*sizeof( *(pos_at_t_of_p[0]) ) );
		n_time_lags_in_MSD_array = 1;
		t = 0;
		first = 0;
		time_first = time;
	}else{
		t = n_time_lags_in_MSD_array;
		if(!stop_growing_array_and_hist){
			/* An upper bound to the maximum histogram size prevents memory problems */
			if(t>1E5){stop_growing_array_and_hist=1;}
			/* Allocate memory for one more time lag */
			grow_histogram( &hist_MSD, t+1 );
			pos_at_t_of_p=realloc(pos_at_t_of_p,(t+1)*sizeof(*pos_at_t_of_p));
			pos_at_t_of_p[t]=calloc(pstart,sizeof( *(pos_at_t_of_p[t]) ));
			//printf("alloc'd %ld bytes for pointer\n",(t+1)*sizeof(*pos_at_t_of_p) );
			//printf("alloc'd %ld bytes for element\n",pstart*sizeof( *(pos_at_t_of_p[t]) ) );
			delta_t_max=t;
			n_time_lags_in_MSD_array++;
		}
		for(tlag=delta_t_max;tlag>0;tlag--){
			for(p=pstart-1;p>=0;p--){
				for(d=0;d<DIM;d++){
					pos_at_t_of_p[tlag][p][d] = pos_at_t_of_p[tlag-1][p][d];
				}
			}
		}
	}

	if(!stop_growing_array_and_hist){hist_MSD.xvalue[t] = time - time_first;}
	/* Now calculate displacements */
	for(p=pstart-1;p>=0;p--){
		/* Copy position at time t to first element (zero dt) of static array */
		for(d=0;d<DIM;d++){
			pos_at_t_of_p[0][p][d]=part[p].rpos[d];
		}
		/* For all time lags smaller than max time, calculate MSD */
		for(tlag=delta_t_max;tlag>=0;tlag--){
			/* Get square of displacement at this time lag */
			vecsubtract(dispvec,pos_at_t_of_p[tlag][p],part[p].rpos);
			hist_MSD.value[tlag].current = normsq(dispvec);
			/* Update statistics (mean, variance) */
			update_observable( &hist_MSD.value[tlag] );
		}
	}

	// printf(" hist:\n");
	// for(tlag=0;tlag<delta_t_max;tlag++){
	// 	printf("   %lf %lf\n",hist_MSD.value[tlag].current,hist_MSD.value[tlag].mvariance/hist_MSD.value[tlag].samples);
	// }
}
/* --------- Measures the mean square displacement ---------
 * This function builds up a histogram for the orientational correlation by tracking a number of
 * particles and measuring their orientations at different time lags. In pseudocode, it does:
 *
 * at time t, having p1->or @ all previous t'
 * alloc bin for dt=t-0 in orientation correlation histogram
 * for nORCOR particles
 * 		store or @ t
 * 		for all dt=t-t' bins in hist
 * 			orcorr = nearestimage(p1->or @ t, p1->or @ t')
 * 			add orcorr to average in bin dt=t-t'
 * 			add orcorr to variance in bin dt=t-t'
 *
 * Main problem is that we need to store orientations for all time bins in RAM, which is a lot.
 * So we should limit that somehow, e.g. by limiting number of samples, by limiting number of
 * particles for which it is done, or limiting for which time lags stuff gets calculated.
 */
void measure_orientational_correlation(double time){
	static char first=1;
	static double time_first;
	static char stop_growing_array_and_hist=0;
	int d;
	double orcorr;
	long int t,tlag,p,pstart;
	static long int delta_t_max=0;

	pstart = (NPARTS_TO_TRACK_ORCOR>n_part) ? n_part : NPARTS_TO_TRACK_ORCOR;

	if(first){
		/* Allocate memory for one time lag */
		create_histogram( &hist_or_cor , 1 );
		or_at_dt_of_p=calloc(1,sizeof(*or_at_dt_of_p));
		or_at_dt_of_p[0]=calloc(pstart,sizeof( *(or_at_dt_of_p[0]) ));
		//printf("alloc'd %ld bytes for pointer\n",sizeof(*or_at_dt_of_p) );
		//printf("alloc'd %ld bytes for element\n",pstart*sizeof( *(or_at_dt_of_p[0]) ) );
		n_time_lags_in_orcorr_array = 1;
		t=0;
		first=0;
		time_first = time;
	}else{
		t = n_time_lags_in_orcorr_array;
		if(!stop_growing_array_and_hist && t>100){
			/* At some point, the orientational correlation drops to nearly zero.
			   There is no point in measuring beyond that, so we stop growing the histogram. */
			if(
				hist_or_cor.value[t-100].mean < 0.005 || // t-100 means we have a mean of 99 measurements
				t > 4000 // To make sure that it has to stop growing at some point.
			){
				stop_growing_array_and_hist=1;
			}
		}
		if(!stop_growing_array_and_hist){
			/* Allocate memory for one more time lag */
			grow_histogram( &hist_or_cor, t+1 );
			or_at_dt_of_p=realloc(or_at_dt_of_p,(t+1)*sizeof(*or_at_dt_of_p));
			or_at_dt_of_p[t]=calloc(pstart,sizeof( *(or_at_dt_of_p[t]) ));
			//printf("alloc'd %ld bytes for pointer\n",(t+1)*sizeof(*or_at_dt_of_p) );
			//printf("alloc'd %ld bytes for element\n",pstart*sizeof( *(or_at_dt_of_p[t]) ) );
			delta_t_max=t;
			n_time_lags_in_orcorr_array++;
		}
		for(tlag=delta_t_max;tlag>0;tlag--){
			for(p=pstart-1;p>=0;p--){
				for(d=0;d<DIM;d++){
					or_at_dt_of_p[tlag][p][d]=or_at_dt_of_p[tlag-1][p][d];
				}
			}
		}
	}

	if(!stop_growing_array_and_hist){hist_or_cor.xvalue[t]=time-time_first;}
	/* Now calculate the orientational autocorrelation */
	for(p=pstart-1;p>=0;p--){
		/* Copy orientation at time t to first element (zero dt) of static array */
		for(d=0;d<DIM;d++){
			or_at_dt_of_p[0][p][d]=part[p].or[d];
		}
		/* For all time lags smaller than max time, calculate autocorrelation */
		for(tlag=delta_t_max;tlag>=0;tlag--){
			/* Get square of displacement at this time lag */
			orcorr=inprod(part[p].or,or_at_dt_of_p[tlag][p]);
			hist_or_cor.value[tlag].current = orcorr;
			/* Update statistics (mean, variance) */
			update_observable( &hist_or_cor.value[tlag] );
		}
	}

	// 	printf(" hist:\n");
	// 	for(tlag=0;tlag<delta_t_max;tlag++){
	// 		printf("   %lf %lf\n",hist_or_cor.value[tlag].current,hist_or_cor.value[tlag].mvariance/hist_or_cor.value[tlag].samples);
	// 	}
}
/* --------- Measures effective self-propulsion velocity ---------
 * The effective self-propulsion velocity is given by the equal-time velocity-orientation
 * correlation function. Since we measure the velocity from the difference in position
 * between two time steps, we need to average the orientation of two time steps to evaluate
 * the orientation at the same time as the velocity.
 */
void measure_effective_velocity(void){
	long int i;
	int d;

	double one_over_dt = 1.0 / dt;
	double velocity[DIM],orientation[DIM];
	// v_eff.current = 0.0;
	for(i=0;i<n_part;i++){
		// First calculate velocity of the particle
		nearestimage(part[i].prev_pos, part[i].pos, velocity);
		for(d=0;d<DIM;d++){
			velocity[d] *= one_over_dt;
		}

		// Then orientation
		vecadd(orientation, part[i].or, part[i].prev_or);
		for(d=0;d<DIM;d++){
			orientation[d] *= 0.5;
		}

		// Add this correlation to the average
		v_eff.current = inprod(velocity,orientation);
		update_observable(&v_eff);
	}
}
/* --------- Use voro++ to make a Voronoi tesselation and measure local density ---------
 *
 */
void measure_packfrac_hist_voro(void){
	double positions[n_part][3];
	// Copy particle positions to a local array
	for(int i = 0; i < n_part; i++){
		for(int d = 0; d < DIM; d++){
			positions[i][d] = part[i].pos[d];
		}
	}
	// Call on the voro++ library through a wrapper function and get the cell volumes
	double voro_volumes[n_part];
	c_voro_get_volumes(
		 -box.hL[0], +box.hL[0],
 		 -box.hL[1], +box.hL[1],
 		 -box.hL[2], +box.hL[2],
		 n_part,
		 positions,
		 voro_volumes
	);
	// Now that we have the volumes, calculate a histogram of local packing fractions
	static double binwidth;
	double one_over_n_part = 1.0 / (double) n_part;

	// If this is the first time the function is called, initialize the histogram
	static char first = 1;
	if(first){
		binwidth = 1.0 / (double) N_P_ETA_BINS;
		// Create histogram
		create_histogram(&hist_packfrac_voro, N_P_ETA_BINS);
		for(int i = 0; i < hist_packfrac_voro.n_bins; i++){
			hist_packfrac_voro.xvalue[i] = (i+0.5) * binwidth;
		}
		first = 0;
	}

	// Clear current values
	for(int i = 0; i < hist_packfrac_voro.n_bins; i++){
		hist_packfrac_voro.value[i].current = 0;
	}

	for(int i = 0; i < n_part; i++){
		int bin = floor(hist_packfrac_voro.n_bins * part[i].vol / voro_volumes[i]);
		// In rare cases local packfrac > 1 due to soft potential overlaps, so prevent errors
		if(bin >= hist_packfrac_voro.n_bins){
			bin = hist_packfrac_voro.n_bins - 1;
		}
		// Add to bin (and normalize in the process)
		hist_packfrac_voro.value[bin].current += 1.0 * one_over_n_part;
	}

	// Update statistics (mean, variance)
	update_histogram(&hist_packfrac_voro);
}
/* --------- Divide up all particles into clusters based on distance considerations ---------
 *
 */
void measure_clusters(long int *p_cluster_idx){
	long int i,j;
	// long int cluster[n_part][n_part]; // each array is a cluster with particle indices as elements
	long int **cluster;
	int n_p_cluster[n_part]; // number of particles for each cluster
	int n_clusters = 0;

	// Find an appropriate cutoff at the maximum in radial distribution function
	// double cluster_cutoff = max_interaction_length_2rods, maxgr = 0;
	// for(i = 0; i < hist_gr.n_bins; i++){
	// 	// Look between below interaction cutoff
	// 	if(hist_gr.xvalue[i] < max_interaction_length_2rods){
	// 		if(hist_gr.value[i].mean > maxgr){
	// 			maxgr = hist_gr.value[i].mean;
	// 			cluster_cutoff = hist_gr.xvalue[i];
	// 		}
	// 	}
	// }
	// double cluster_cutoff = pow(2.0, 1.0/6.0);

	double cluster_cutoff = 1.04;
	double cluster_cutoff_sq = cluster_cutoff * cluster_cutoff;

	// Find an appropriate cutoff at the first minimum in radial distribution function
	// double cluster_cutoff = max_interaction_length_2rods, extr_gr = 0;
	// size_t max_bin;
	// // First find the first maximum
	// for(i = 0; i < hist_gr.n_bins; i++){
	// 	// Look below interaction cutoff
	// 	if(hist_gr.xvalue[i] < 2*max_interaction_length_2rods){
	// 		if(hist_gr.value[i].mean > extr_gr){
	// 			extr_gr = hist_gr.value[i].mean;
	// 			max_bin = i;
	// 		}
	// 	}
	// }
	// // Then the next minimum
	// for(i = max_bin; i < hist_gr.n_bins; i++){
	// 	// Look below interaction cutoff
	// 	if(hist_gr.xvalue[i] < 2*max_interaction_length_2rods){
	// 		if(hist_gr.value[i].mean < extr_gr){
	// 			extr_gr = hist_gr.value[i].mean;
	// 			cluster_cutoff = hist_gr.xvalue[i];
	// 		}
	// 	}
	// }
	// double cluster_cutoff_sq = cluster_cutoff * cluster_cutoff;

	// Start with each particle being its own cluster
	for(i = 0; i < n_part; i++){
		p_cluster_idx[i] = i;
	}

	// Loop over particle pairs and make clusters (similar to Wolff algorithm)
	long int cellp, nbcell, pi_idx, pj_idx;
	int cl_idx;
	double distance_sq;
	long int stack[n_part];
	long int stack_max = 0;
	cluster = calloc(1, sizeof(*cluster) );
	for(pi_idx = 0; pi_idx < n_part; pi_idx++){
		// If particle p is alone in its cluster, build its cluster
		if( p_cluster_idx[pi_idx] == pi_idx ){
			cl_idx = n_clusters; // index of the cluster being looked at
			p_cluster_idx[pi_idx] = cl_idx;
			// Allocate memory for a new cluster of max n_part particles
			cluster         = realloc(cluster, (cl_idx+1)*sizeof(*cluster) );
			cluster[cl_idx] = calloc(n_part, sizeof(*(cluster[cl_idx])) );
			n_p_cluster[cl_idx] = 1;
			n_clusters++;
			// Put p onto a stack for particles in the cluster
			stack[stack_max] = pi_idx;
			stack_max++;
			// While any particle in the cluster has neighbours, check them for adding to cluster
			while( stack_max > 0 ){
				// Grab a particle from the stack to check its neighbours
				tpart *pi = &part[ stack[stack_max-1] ];
				stack_max--;
				// Use the existing cell list to speed up the search significantly
				cellp = pi->cell;
				for(i = 0; i < neighbouring_cells; i++){
					nbcell = cells[cellp].neighbours[i];
					for(j = 0; j < cells[nbcell].n; j++){
						pj_idx = cells[nbcell].particles[j];
						// If this pj is not already in the cluster
						if( p_cluster_idx[pj_idx] != cl_idx ){
							tpart *pj = &part[pj_idx];
							// Calculate based on distance whether to add pj to cluster
							if(SPHERES){
								distance_sq = nearestimagedistsq(pi->pos, pj->pos);
							}else{
								double nearvec[3], lambda_i, lambda_j;
								nearestimage(pi->pos, pj->pos, nearvec);
								distance_sq = distsq_rods(pi, pj, nearvec, &lambda_i, &lambda_j);
							}
							if(distance_sq < cluster_cutoff_sq){
								// Add pj to cluster and set pj's cluster idx to the right number
								p_cluster_idx[pj_idx] = cl_idx;
								cluster[cl_idx][ n_p_cluster[cl_idx] ] = pj_idx;
								n_p_cluster[cl_idx]++;
								// Also add to the stack so pj's neighbours get checked too
								stack[stack_max] = pj_idx;
								stack_max++;
							}
						}
					}
				}
			}
		}
	}

	// Calculate the fraction of particles that are part of the largest cluster
	double max = 0;
	for(i = 0; i < n_clusters; i++){
		double cluster_fraction = n_p_cluster[i] / ((double) n_part);
		if(cluster_fraction > max){
			max = cluster_fraction;
		}
	}
	// Add this measurement to the average
	max_cluster_fraction.current = max;
	update_observable(&max_cluster_fraction);

	// Calculate the degree of clustering: 1-Nc/N
	degree_of_clustering.current = 1.0 - ((double) n_clusters / (double) n_part);
	update_observable(&degree_of_clustering);

	// Free memory used
	for(i = 0; i < n_clusters; i++){
		free(cluster[i]);
	}
	free(cluster);
}
/* --------- Divide up all particles into clusters based on distance considerations ---------
 *
 */
void measure_clusters_density(long int *p_cluster_idx){
	long int **cluster;
	int n_p_cluster[n_part]; // number of particles for each cluster
	int n_clusters = 0;

	// double cluster_cutoff = pow(2.0, 1.0/6.0);
	// double cluster_cutoff_sq = cluster_cutoff * cluster_cutoff;
	// double packfrac_cutoff = 0.015;
	double offset = 0.025;

	// Start with each particle being its own cluster
	for(size_t i = 0; i < n_part; i++){
		p_cluster_idx[i] = i;
	}

	// Copy particle positions to a local array
	double positions[n_part][3];
	for(int i = 0; i < n_part; i++){
		for(int d = 0; d < DIM; d++){
			positions[i][d] = part[i].pos[d];
		}
	}
	// Get cell volumes and particle neighbours from Voronoi tesselation
	double vols[n_part];
	int n_neighbours[n_part];
	int **neighbours = calloc(n_part, sizeof(*neighbours));
	for(size_t i = 0; i < n_part; i++){
		// Particles shouldn't ever have more than 128 neighbours anyway
		neighbours[i] = calloc(128, sizeof(*neighbours[i]));
	}
	c_voro_get_volumes_and_neighbours(
		 -box.hL[0], +box.hL[0],
 		 -box.hL[1], +box.hL[1],
 		 -box.hL[2], +box.hL[2],
		 n_part,
		 positions,
		 vols,
		 n_neighbours,
		 neighbours
	);

	// Loop over particle pairs and make clusters (similar to Wolff algorithm)
	long int pi_idx, pj_idx, cl_idx;
	double r2;
	long int stack[n_part];
	long int stack_max = 0;
	cluster = calloc(1, sizeof(*cluster) );
	for(pi_idx = 0; pi_idx < n_part; pi_idx++){
		// If particle p is alone in its cluster, build its cluster
		if( p_cluster_idx[pi_idx] == pi_idx ){
			cl_idx = n_clusters; // index of the cluster being looked at
			p_cluster_idx[pi_idx] = cl_idx;
			// Allocate memory for a new cluster of max n_part particles
			cluster         = realloc(cluster, (cl_idx+1)*sizeof(*cluster) );
			cluster[cl_idx] = calloc(n_part, sizeof(*(cluster[cl_idx])) );
			n_p_cluster[cl_idx] = 1;
			n_clusters++;
			// Put p onto a stack for particles in the cluster
			stack[stack_max] = pi_idx;
			stack_max++;
			// While any particle in the cluster has neighbours, check them for adding to cluster
			while( stack_max > 0 ){
				// Grab a particle from the stack to check its neighbours
				pi_idx = stack[stack_max-1];
				tpart *pi = &part[pi_idx];
				stack_max--;
				double packfrac_pi = pi->vol / vols[pi_idx];
				// Check Voronoi neighbours whether they can be added to cluster
				for(int j = 0; j < n_neighbours[pi_idx]; j++){
					pj_idx = neighbours[pi_idx][j];
					// If this pj is not already in the cluster
					if( p_cluster_idx[pj_idx] != cl_idx ){
						tpart *pj = &part[pj_idx];
						// calculate local density of pj
						double packfrac_pj = pj->vol / vols[pj_idx];
						// Calculate distance too
						// if(SPHERES){
						// 	r2 = nearestimagedistsq(pi->pos, pj->pos);
						// }else{
						// 	double nearvec[3], lambda_i, lambda_j;
						// 	nearestimage(pi->pos, pj->pos, nearvec);
						// 	r2 = distsq_rods(pi, pj, nearvec, &lambda_i, &lambda_j);
						// }
						// if( fabs(packfrac_pj - packfrac_pi) < packfrac_cutoff && r2 < cluster_cutoff_sq ){
						// If Voronoi neighbours and similar in density, add pj to cluster of pi
						if(
							(packfrac_pi > packfrac + offset && packfrac_pj > packfrac + offset) || // High density cluster
							(packfrac_pi < packfrac - offset && packfrac_pj < packfrac - offset) // Low density cluster
						){
							// Add pj to cluster and set pj's cluster idx to the right number
							p_cluster_idx[pj_idx] = cl_idx;
							cluster[cl_idx][ n_p_cluster[cl_idx] ] = pj_idx;
							n_p_cluster[cl_idx]++;
							// Also add to the stack so pj's neighbours get checked too
							stack[stack_max] = pj_idx;
							stack_max++;
						}
					}
				}
			}
		}
	}

	// Calculate the fraction of particles that are part of the largest cluster
	double max = 0;
	for(size_t i = 0; i < n_clusters; i++){
		double cluster_fraction = n_p_cluster[i] / ((double) n_part);
		if(cluster_fraction > max){
			max = cluster_fraction;
		}
	}
	// Add this measurement to the average
	max_cluster_fraction.current = max;
	update_observable(&max_cluster_fraction);

	// Calculate the degree of clustering: 1-Nc/N
	degree_of_clustering.current = 1.0 - ((double) n_clusters / (double) n_part);
	update_observable(&degree_of_clustering);

	// Free memory used
	for(size_t i = 0; i < n_clusters; i++){
		free(cluster[i]);
	}
	free(cluster);
	for(size_t i = 0; i < n_part; i++){
		free(neighbours[i]);
	}
	free(neighbours);
}



/*========================== File output functions ==========================*/

/* --------- Appends a snapshot to a time series file ---------
 * This function constructs a time series of snapshots in a single file.
 * It's not called as often as save_timeseries() and so doesn't create huge files.
 */
void save_timeseries_sparse(void){
	long int i,j;
	int d;
	static int save_count=0;
	static char filename[255];
	FILE* output;

	long int p_cluster_idx[n_part];
	measure_clusters_density(p_cluster_idx);
	measure_local_packfrac();
	measure_local_nematic_order();

	if(save_count==0){
		if(HAVE_MESH && CROSSLINKS){sprintf(filename,"Sparse_%04d.meshrodcl",run);}
		else if(CROSSLINKS){sprintf(filename,"Sparse_%04d.rodcl",run);}
		else if(HAVE_MESH){sprintf(filename,"Sparse_%04d.meshrod",run);}
		else{sprintf(filename,"Sparse_%04d.rod",run);}
		output=fopen(filename,"w");
		/* Header */
		fprintf(output,"# --BD Simulation input parameters--\n");
		fprintf(output,"#   (Initial) packing fraction:             %8.08lf\n",packfrac);
		fprintf(output,"#   time step dt used:                      %8.08lf\n",dt);
		fprintf(output,"#   WCA energy scale epsilon / kT:          %8.08lf\n",wca_prefactor);
		if(SPHERES){
			fprintf(output,"#   Translational diffusion coefficient:    %8.08lf\n",diffusion_t_sph);
			fprintf(output,"#   Rotational diffusion coefficient:       1.0\n");
		}else{
			fprintf(output,"#   Parallel diffusion coefficient:         %8.08lf\n",diffusion_para_rod);
			fprintf(output,"#   Perpendicular diffusion coefficient:    %8.08lf\n",diffusion_perp_rod);
			fprintf(output,"#   Rotational diffusion coefficient:       1.0\n");
			fprintf(output,"#   Aspect ratio of spherocylinders:        %3.03lf\n",lod);
		}
		fprintf(output,"#   Active force:                           %8.08lf\n",active_force);
		fprintf(output,"#   Bare self_propulsion_velocity:          %8.08lf\n",self_propulsion_velocity);
		fprintf(output,"#   Rotational Peclet number:               %8.08lf\n",Peclet);
	}else{
		output=fopen(filename,"a");
	}
	/* Box */
	fprintf(output,"&%ld\n",n_part);												//number of particles
	fprintf(output,"%lf %lf %lf\n",box.L[0],box.L[1],box.L[2]);						//size of the box
	/* Particles */
	for(i=0;i<n_part;i++){
		fprintf(output,"a %4.04lf %4.04lf %4.04lf ",part[i].pos[0],part[i].pos[1],part[i].pos[2]);	// positions
		fprintf(output,"%4.04lf %4.04lf %4.04lf ",part[i].or[0],part[i].or[1],part[i].or[2]);		// orientations
		fprintf(output,"%2.02lf %2.02lf ",part[i].d,part[i].l);							// rod diameter, rod length
		fprintf(output,"%3.03lf %3.03lf ",part[i].nem,part[i].localpackfrac);			// local nematic order, local density
		fprintf(output,"%d\n",p_cluster_idx[i]);	// cluster the particle belongs to
	}
	/* Mesh */
	if(HAVE_MESH){
		fprintf(output,"%ld\n",mesh.nvertices);
		for(i=0;i<mesh.nvertices;i++){
			fprintf(output,"%4.04f %4.04f %4.04f\n",mesh.vertices[i].pos[0],mesh.vertices[i].pos[1],mesh.vertices[i].pos[2]);
		}
		fprintf(output,"%ld\n",mesh.nfaces);
		for(i=0;i<mesh.nfaces;i++){
			fprintf(output,"%hd ",mesh.faces[i].nvertices);
			for(j=0;j<mesh.faces[i].nvertices;j++){
				fprintf(output,"%ld ",mesh.faces[i].vertices[j]);
			}
			fprintf(output,"\n");
		}
	}
	/* Crosslinkers */
	if(CROSSLINKS){
		fprintf(output,"%ld\n",n_crosslinks);
		tpart *p1,*p2;
		tcrosslink *cl;
		for(i=0;i<n_crosslinks;i++){
			cl=&crosslink[i];
			p1=cl->p1;
			p2=cl->p2;
			fprintf(output,"%ld %ld",p1->index,p2->index);
			for(d=0;d<DIM;d++){fprintf(output," %4.04f",p1->pos[d]+p1->or[d]*cl->l1);}
			for(d=0;d<DIM;d++){fprintf(output," %4.04f",p2->pos[d]+p2->or[d]*cl->l2);}
			fprintf(output,"\n");
		}
	}
	//printf("Saved!\n");
	save_count++;
	fclose(output);
}
/* --------- Appends a snapshot to a time series file ---------
 * Saves the current particle/mesh configuration to a file.
 */
void save_timeseries(void){
	long int i,j;
	int d;
	static int save_count=0;
	static char filename[255];
	FILE* output;

	long int p_cluster_idx[n_part];
	measure_clusters_density(p_cluster_idx);
	measure_local_packfrac();
	measure_local_nematic_order();

	if(save_count==0){
		if(HAVE_MESH && CROSSLINKS){sprintf(filename,"Coords_%04d.meshrodcl",run);}
		else if(CROSSLINKS){sprintf(filename,"Coords_%04d.rodcl",run);}
		else if(HAVE_MESH){sprintf(filename,"Coords_%04d.meshrod",run);}
		else{sprintf(filename,"Coords_%04d.rod",run);}
		output=fopen(filename,"w");
		/* Header */
		fprintf(output,"# --BD Simulation input parameters--\n");
		fprintf(output,"#   (Initial) packing fraction:             %8.08lf\n",packfrac);
		fprintf(output,"#   time step dt used:                      %8.08lf\n",dt);
		fprintf(output,"#   WCA energy scale epsilon / kT:          %8.08lf\n",wca_prefactor);
		if(SPHERES){
			fprintf(output,"#   Translational diffusion coefficient:    %8.08lf\n",diffusion_t_sph);
			fprintf(output,"#   Rotational diffusion coefficient:       1.0\n");
		}else{
			fprintf(output,"#   Parallel diffusion coefficient:         %8.08lf\n",diffusion_para_rod);
			fprintf(output,"#   Perpendicular diffusion coefficient:    %8.08lf\n",diffusion_perp_rod);
			fprintf(output,"#   Rotational diffusion coefficient:       1.0\n");
			fprintf(output,"#   Aspect ratio of spherocylinders:        %3.03lf\n",lod);
		}
		fprintf(output,"#   Active force:                           %8.08lf\n",active_force);
		fprintf(output,"#   Bare self_propulsion_velocity:          %8.08lf\n",self_propulsion_velocity);
		fprintf(output,"#   Rotational Peclet number:               %8.08lf\n",Peclet);
	}else{
		output=fopen(filename,"a");
	}
	/* Box */
	fprintf(output,"&%ld\n",n_part);												//number of particles
	fprintf(output,"%lf %lf %lf\n",box.L[0],box.L[1],box.L[2]);						//size of the box
	/* Particles */
	for(i=0;i<n_part;i++){
		fprintf(output,"a %4.04lf %4.04lf %4.04lf ",part[i].pos[0],part[i].pos[1],part[i].pos[2]);	// positions
		fprintf(output,"%4.04lf %4.04lf %4.04lf ",part[i].or[0],part[i].or[1],part[i].or[2]);		// orientations
		fprintf(output,"%2.02lf %2.02lf ",part[i].d,part[i].l);							// rod diameter, rod length
		fprintf(output,"%3.03lf %3.03lf ",part[i].nem,part[i].localpackfrac);			// local nematic order, local density
		fprintf(output,"%d\n",p_cluster_idx[i]);	// cluster the particle belongs to
	}
	/* Mesh */
	if(HAVE_MESH){
		fprintf(output,"%ld\n",mesh.nvertices);
		for(i=0;i<mesh.nvertices;i++){
			fprintf(output,"%4.04f %4.04f %4.04f\n",mesh.vertices[i].pos[0],mesh.vertices[i].pos[1],mesh.vertices[i].pos[2]);
		}
		fprintf(output,"%ld\n",mesh.nfaces);
		for(i=0;i<mesh.nfaces;i++){
			fprintf(output,"%hd ",mesh.faces[i].nvertices);
			for(j=0;j<mesh.faces[i].nvertices;j++){
				fprintf(output,"%ld ",mesh.faces[i].vertices[j]);
			}
			fprintf(output,"\n");
		}
	}
	/* Crosslinkers */
	if(CROSSLINKS){
		fprintf(output,"%ld\n",n_crosslinks);
		tpart *p1,*p2;
		tcrosslink *cl;
		for(i=0;i<n_crosslinks;i++){
			cl=&crosslink[i];
			p1=cl->p1;
			p2=cl->p2;
			fprintf(output,"%ld %ld",p1->index,p2->index);
			for(d=0;d<DIM;d++){fprintf(output," %4.04f",p1->pos[d]+p1->or[d]*cl->l1);}
			for(d=0;d<DIM;d++){fprintf(output," %4.04f",p2->pos[d]+p2->or[d]*cl->l2);}
			fprintf(output,"\n");
		}
	}
	//printf("Saved!\n");
	save_count++;
	fclose(output);
}
/* --------- Makes a snapshot at the end of the simulation ---------
 * This function makes a file that always contains only the most recent snapshot.
 */
void save_last(void){
	long int i,j;
	int d;
	static char filename[255];
	FILE* output;

	long int p_cluster_idx[n_part];
	measure_clusters_density(p_cluster_idx);
	measure_local_packfrac();
	measure_local_nematic_order();

	if(HAVE_MESH && CROSSLINKS){sprintf(filename,"LastCoords.meshrodcl");}
	else if(CROSSLINKS){sprintf(filename,"LastCoords.rodcl");}
	else if(HAVE_MESH){sprintf(filename,"LastCoords.meshrod");}
	else{sprintf(filename,"LastCoords.rod");}
	output=fopen(filename,"w");
	/* Header */
	fprintf(output,"# --BD Simulation input parameters--\n");
	fprintf(output,"#   (Initial) packing fraction:             %8.08lf\n",packfrac);
	fprintf(output,"#   time step dt used:                      %8.08lf\n",dt);
	fprintf(output,"#   WCA energy scale epsilon / kT:          %8.08lf\n",wca_prefactor);
	if(SPHERES){
		fprintf(output,"#   Translational diffusion coefficient:    %8.08lf\n",diffusion_t_sph);
		fprintf(output,"#   Rotational diffusion coefficient:       1.0\n");
	}else{
		fprintf(output,"#   Parallel diffusion coefficient:         %8.08lf\n",diffusion_para_rod);
		fprintf(output,"#   Perpendicular diffusion coefficient:    %8.08lf\n",diffusion_perp_rod);
		fprintf(output,"#   Rotational diffusion coefficient:       1.0\n");
		fprintf(output,"#   Aspect ratio of spherocylinders:        %3.03lf\n",lod);
	}
	fprintf(output,"#   Active force:                           %8.08lf\n",active_force);
	fprintf(output,"#   Bare self_propulsion_velocity:          %8.08lf\n",self_propulsion_velocity);
	fprintf(output,"#   Rotational Peclet number:               %8.08lf\n",Peclet);
	/* Box */
	fprintf(output,"&%ld\n",n_part);												//number of particles
	fprintf(output,"%lf %lf %lf\n",box.L[0],box.L[1],box.L[2]);						//size of the box
	/* Particles */
	for(i=0;i<n_part;i++){
		fprintf(output,"a %4.04lf %4.04lf %4.04lf ",part[i].pos[0],part[i].pos[1],part[i].pos[2]);	// positions
		fprintf(output,"%4.04lf %4.04lf %4.04lf ",part[i].or[0],part[i].or[1],part[i].or[2]);		// orientations
		fprintf(output,"%2.02lf %2.02lf ",part[i].d,part[i].l);							// rod diameter, rod length
		fprintf(output,"%3.03lf %3.03lf ",part[i].nem,part[i].localpackfrac);			// local nematic order, local density
		fprintf(output,"%d\n",p_cluster_idx[i]);	// cluster the particle belongs to
	}
	/* Mesh */
	if(HAVE_MESH){
		fprintf(output,"%ld\n",mesh.nvertices);
		for(i=0;i<mesh.nvertices;i++){
			fprintf(output,"%4.04f %4.04f %4.04f\n",mesh.vertices[i].pos[0],mesh.vertices[i].pos[1],mesh.vertices[i].pos[2]);
		}
		fprintf(output,"%ld\n",mesh.nfaces);
		for(i=0;i<mesh.nfaces;i++){
			fprintf(output,"%hd ",mesh.faces[i].nvertices);
			for(j=0;j<mesh.faces[i].nvertices;j++){
				fprintf(output,"%ld ",mesh.faces[i].vertices[j]);
			}
			fprintf(output,"\n");
		}
	}
	/* Crosslinkers */
	if(CROSSLINKS){
		fprintf(output,"%ld\n",n_crosslinks);
		tpart *p1,*p2;
		tcrosslink *cl;
		for(i=0;i<n_crosslinks;i++){
			cl=&crosslink[i];
			p1=cl->p1;
			p2=cl->p2;
			fprintf(output,"%ld %ld",p1->index,p2->index);
			for(d=0;d<DIM;d++){fprintf(output," %4.04f",p1->pos[d]+p1->or[d]*cl->l1);}
			for(d=0;d<DIM;d++){fprintf(output," %4.04f",p2->pos[d]+p2->or[d]*cl->l2);}
			fprintf(output,"\n");
		}
	}
	//printf("Saved!\n");
	fclose(output);
}
/* --------- Saves pressure to file ---------
 *
 */
void save_pressure(double time){
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"Pressure_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"a");
	}
	/* Write stuff */
	fprintf(output,"%lf %lf %lf %12.012lf\n",time,pressure.current,pressure.mean,pressure.mvariance/pressure.samples);
	save_count++;
	fclose(output);
}
/* --------- Saves nematic order parameter to file ---------
 *
 */
void save_orderparameter(double time){
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"NematicOrder_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"a");
	}
	/* Write stuff */
	fprintf(output,"%lf %lf %lf %12.012lf\n",time,nematic_order.current,nematic_order.mean,nematic_order.mvariance/nematic_order.samples);
	save_count++;
	fclose(output);
}
/* --------- Saves packing fraction histogram to file ---------
 *
 */
void save_packfrac_hist(void){
	int i;
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"p_eta_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"w");
	}
	/* Write stuff */
	for(i=0;i<hist_p_eta.n_bins;i++){
		fprintf(output,"%lf %12.012lf %12.012lf\n",
				hist_p_eta.xvalue[i],
				hist_p_eta.value[i].mean,
				hist_p_eta.value[i].mvariance/hist_p_eta.value[i].samples
		);
	}
	//printf("p(eta) saved!\n");
	save_count++;
	fclose(output);
}
/* --------- Saves a vector field to file ---------
 *
 */
void save_vector_field(double *field, int nx, int ny, int nz, int dim, char *name){
	int x,y,z,d;
	char filename[255];
	FILE* output;
	sprintf(filename,"%s_field_%04d.dat",name,run);
	output=fopen(filename,"w");
	/* Write stuff */
	fprintf(output,"%.05e %.05e %.05e\n",box.L[0],box.L[1],box.L[2]);
	fprintf(output,"%d %d %d\n",nx,ny,nz);
	for(x=0;x<nx;x++){
		for(y=0;y<ny;y++){
			for(z=0;z<nz;z++){
				for(d=0;d<dim;d++){
					//printf("%.05e ",field[DIM*(z+nz*(y+ny*x))+d]);
					fprintf(output,"%.05e ",field[DIM*(z+nz*(y+ny*x))+d]);
				}
				fprintf(output,"\n");
			}
		}
	}
	fclose(output);
}
/* --------- Saves velocity profile to file ---------
 *
 */
void save_velocity_profile_x(void){
	int i;
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"velocity_profile_x_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"w");
	}
	/* Write stuff */
	for(i=0;i<hist_effective_velocity.n_bins;i++){
		fprintf(output,"%lf %12.012lf %12.012lf\n",
				hist_effective_velocity.xvalue[i],
				hist_effective_velocity.value[i].mean,
				hist_effective_velocity.value[i].mvariance/hist_effective_velocity.value[i].samples
		);
	}
	//printf("v(x) saved!\n");
	save_count++;
	fclose(output);
}
/* --------- Saves density profile to file ---------
 *
 */
void save_density_profile_x(void){
	int i;
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"density_profile_x_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"w");
	}
	/* Write stuff */
	for(i=0;i<hist_density_x.n_bins;i++){
		fprintf(output,"%lf %12.012lf %12.012lf\n",
				hist_density_x.xvalue[i],
				hist_density_x.value[i].mean,
				hist_density_x.value[i].mvariance/hist_density_x.value[i].samples
		);
	}
	//printf("rho(x) saved!\n");
	save_count++;
	fclose(output);
}
/* --------- Saves pressure profile to file ---------
 *
 */
void save_pressure_profile_x(void){
	int i;
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"pressure_profile_x_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"w");
	}
	/* Write all pressure contributions to file:
	 * We have p = p_idealgas + p_virial + p_swim
	 * For dimensionless pressure p d^3 / kT, the ideal gas part is just the density. Has no variance.
	 * The others have histograms.
	 */
	for(i=0;i<N_PRESSURE_X_BINS;i++){
		fprintf(output,"%lf %12.012lf %12.012lf %12.012lf %12.012lf %12.012lf\n",
				hist_virial_pressure_x.xvalue[i],
				density,
				hist_virial_pressure_x.value[i].mean,
				hist_virial_pressure_x.value[i].mvariance/hist_virial_pressure_x.value[i].samples,
				hist_swim_pressure_x.value[i].mean,
				hist_swim_pressure_x.value[i].mvariance/hist_swim_pressure_x.value[i].samples
		);
	}
	//printf("p(x) saved!\n");
	save_count++;
	fclose(output);
}
/* --------- Saves velocity distribution to file ---------
 *
 */
void save_velocity_probability(void){
	int i;
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"velocity_probability_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"w");
	}
	/* Write stuff */
	for(i=0;i<hist_velocity_probability.n_bins;i++){
		if(hist_velocity_probability.value[i].samples!=0){
			fprintf(output,"%.08e %.08e %.08e \n",
					hist_velocity_probability.xvalue[i],
					hist_velocity_probability.value[i].mean,
					hist_velocity_probability.value[i].mvariance/hist_velocity_probability.value[i].samples
			);
		}
	}
	save_count++;
	fclose(output);
}
/* --------- Saves radial distribution function to file ---------
 *
 */
void save_gr(void){
	int i;
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"gr_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"w");
	}
	/* Write stuff */
	for(i=0;i<hist_gr.n_bins;i++){
		fprintf(output,"%lf %12.012lf %12.012lf\n",
				hist_gr.xvalue[i],
				hist_gr.value[i].mean,
				hist_gr.value[i].mvariance/hist_gr.value[i].samples
		);
	}
	//printf("g(r) saved!\n");
	save_count++;
	fclose(output);
}
/* --------- Saves spatial velocity correlation function to file ---------
 *
 */
void save_spatial_velocity_correlation(void){
	int i;
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"velocity_correlation_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"w");
	}
	/* Write stuff */
	for(i=0;i<hist_spatial_velocity_correlation.n_bins;i++){
		if(hist_spatial_velocity_correlation.value[i].samples!=0){
			fprintf(output,"%.08e %.08e %.08e\n",
					hist_spatial_velocity_correlation.xvalue[i],
					hist_spatial_velocity_correlation.value[i].mean,
					hist_spatial_velocity_correlation.value[i].mvariance/hist_spatial_velocity_correlation.value[i].samples
			);
		}
	}
	save_count++;
	fclose(output);
}
/* --------- Saves spatial orientation correlation function to file ---------
 *
 */
void save_spatial_orientation_correlation(void){
	int i;
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"orientation_correlation_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"w");
	}
	/* Write stuff */
	for(i=0;i<hist_spatial_orientation_correlation.n_bins;i++){
		if(hist_spatial_orientation_correlation.value[i].samples!=0){
			fprintf(output,"%.08e %.08e %.08e\n",
					hist_spatial_orientation_correlation.xvalue[i],
					hist_spatial_orientation_correlation.value[i].mean,
					hist_spatial_orientation_correlation.value[i].mvariance/hist_spatial_orientation_correlation.value[i].samples
			);
		}
	}
	save_count++;
	fclose(output);
}
/* --------- Saves MSD histogram to file ---------
 *
 */
void save_MSD(void){
	int i;
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"MSD_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"w");
	}
	/* Write stuff */
	for(i=0;i<hist_MSD.n_bins;i++){
		fprintf(output,"%.08e %.08e %.08e %ld\n",
				hist_MSD.xvalue[i],
				hist_MSD.value[i].mean,
				hist_MSD.value[i].mvariance/hist_MSD.value[i].samples,
				hist_MSD.value[i].samples
		);
	}
	//printf("MSD saved!\n");
	save_count++;
	fclose(output);
}
/* --------- Saves orientational correlation histogram to file ---------
 *
 */
void save_orientational_correlation(void){
	int i;
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"OrCorr_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"w");
	}
	/* Write stuff */
	for(i=0;i<hist_or_cor.n_bins;i++){
		fprintf(output,"%12.012lf %12.012lf %12.012lf %ld\n",
				hist_or_cor.xvalue[i],
				hist_or_cor.value[i].mean,
				hist_or_cor.value[i].mvariance/hist_or_cor.value[i].samples,
				hist_or_cor.value[i].samples
		);
	}
	//printf("Orientational correlation saved!\n");
	save_count++;
	fclose(output);
}
/* --------- Saves effective velocity to file ---------
 *
 */
void save_effective_velocity(double time){
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"Effective_Velocity_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"a");
	}
	/* Write stuff */
	fprintf(output,"%8.08e %8.08e %8.08e %8.08e %ld\n",
		time,
		v_eff.current,
		v_eff.mean,
		v_eff.mvariance/v_eff.samples,
		v_eff.samples
	);
	save_count++;
	fclose(output);
}
/* --------- Saves packing fraction histogram from Voro++ to file ---------
 *
 */
void save_packfrac_hist_voro(void){
	int i;
	static int save_count=0;
	static char filename[255];
	FILE* output;
	if(save_count==0){
		sprintf(filename,"p_eta_%04d.dat",run);
		output=fopen(filename,"w");
	}else{
		output=fopen(filename,"w");
	}
	/* Write stuff */
	for(i=0;i<hist_packfrac_voro.n_bins;i++){
		fprintf(output,"%lf %12.012lf %12.012lf\n",
				hist_packfrac_voro.xvalue[i],
				hist_packfrac_voro.value[i].mean,
				hist_packfrac_voro.value[i].mvariance/hist_packfrac_voro.value[i].samples
		);
	}
	//printf("p(eta) saved!\n");
	save_count++;
	fclose(output);
}

/*========================== Initialization functions ==========================*/

/* --------- Initialize the cell list ---------
 * This function constructs the cell list used in speeding up the pair interaction calculation.
 * It first calculates the minimum size of the cells in order to cover all interactions. Next,
 * it calculates the number of neighbours any cell can have. After allocating the memory needed
 * for the cells, it then constructs the lists of neighbours for every cell (which should include
 * cell itself because of how we wrote the evaluate_forces() function). When the neighbours are
 * defined, it sorts all the particles into the right cells.
 */
void init_cells(void){
	n_cells=0;
	int x,y,z,i,j,k;
	int dx,dy,dz,a,b,c;
	int max_neighbouring_cells;
	printf("Max interaction length for cell list: %lf\n",max_interaction_length_2rods);
	nx = floor(0.95*box.L[0]/max_interaction_length_2rods);	//how many cells "fit" in one direction
	ny = floor(0.95*box.L[1]/max_interaction_length_2rods);
	nz = floor(0.95*box.L[2]/max_interaction_length_2rods);
	if(nx==0){nx=1;}
	if(ny==0){ny=1;}
	if(nz==0){nz=1;}
	current_cell_size=box.L[0]/nx;
	if(box.L[1]/ny>current_cell_size){current_cell_size=box.L[1]/ny;}
	if(box.L[2]/nz>current_cell_size){current_cell_size=box.L[2]/nz;}
	n_cells=nx*ny*nz;
	printf("number of cells is %ld, ",n_cells);
	if(n_cells>100000){
		printf("Huh. %ld cells. That'll take at least %lu MB of memory. ",n_cells,n_cells*sizeof(tcell)/(1024*1024));
		printf("But probably much more. Might freeze ya computar. Stopping now instead.\n");
		exit(42);
	}
	if(n_cells<27){max_neighbouring_cells=n_cells;}
	else{max_neighbouring_cells=27;}
	printf("so max neighbours is %d.\n",max_neighbouring_cells);

	/* Allocate memory for cell list */
	cells = calloc(n_cells,sizeof(*cells));

	/* Construct neighbouring cell list */
	int unique,l;
	for(x=0;x<nx;x++) for(y=0;y<ny;y++) for(z=0;z<nz;z++){
		i=x*ny*nz+y*nz+z;
		cells[i].index = i;
		cells[i].particles = calloc(MAX_PART_CELL,sizeof(*cells[i].particles));
		if(CELLTROUBLE){printf("\nCell %d neighbours: ",i);}
		cells[i].n=0;
		/* Clear neighbour list first */
		for(j=max_neighbouring_cells-1;j>=0;j--){
			cells[i].neighbours[j]=n_cells+1; // used as a marker as n_cells+1 is out of range
		}
		/* Now add neighbours (including itself) */
		k=0;
		for(a=-1;a<2;a++) for(b=-1;b<2;b++) for(c=-1;c<2;c++){
			unique=1;
			dx=a;dy=b;dz=c;
			if(x+dx < 0) dx=nx-1; if(x+dx > nx-1) dx=-nx+1;
			if(y+dy < 0) dy=ny-1; if(y+dy > ny-1) dy=-ny+1;
			if(z+dz < 0) dz=nz-1; if(z+dz > nz-1) dz=-nz+1;
			j=(x+dx)*ny*nz+(y+dy)*nz+(z+dz);
			/* Check all neigbours to see whether cell is already there (happens for n_cells<27) */
			for(l=max_neighbouring_cells-1;l>=0;l--){
				if(j==cells[i].neighbours[l]){unique=0;break;}
			}
			/* If the neighbouring cell is not yet there, add it to the list */
			if(unique){
				cells[i].neighbours[k]=j;
				if(CELLTROUBLE){printf("%d ",j);}
				k++;
			}
		}
		neighbouring_cells=k;
	}

	/* Put particles into cells */
	for(i=0;i<n_part;i++){
		j=coords2cell(part[i].pos[0],part[i].pos[1],part[i].pos[2],nx,ny,nz);
		if(cells[j].n+1 >= MAX_PART_CELL){printf("ERROR: Too many particles in a cell!\n"); exit(666);}
		// 		cells[j].particles = realloc(cells[j].particles,(cells[j].n+1)*sizeof(*cells[j].particles)); // Acts as malloc for first call
		cells[j].particles[cells[j].n] = i;
		part[i].cell=j;
		cells[j].n++;
	}

	/* Print the cell list. (%d!) indicates particle in cell is not cell of particle */
	if(CELLTROUBLE){
		int partincell;
		for(k=0;k<n_cells;k++){
			printf("\nCell %d contains particles: ",k);
			for(j=cells[k].n-1;j>=0;j--){
				partincell=cells[k].particles[j];
				if(part[partincell].cell!=k){
					printf("(%d!) ",partincell);
				}else{
					printf("%d ",partincell);
				}
			}
		}
		printf("\n");
	}
}
/* --------- Initialize the cell list ---------
 * This function constructs the cell list used in speeding up the pair interaction calculation.
 * It first calculates the minimum size of the cells in order to cover all interactions. Next,
 * it calculates the number of neighbours any cell can have. After allocating the memory needed
 * for the cells, it then constructs the lists of neighbours for every cell (which should include
 * cell itself because of how we wrote the evaluate_forces() function). When the neighbours are
 * defined, it sorts all the particles and mesh objects into the right cells.
 */
void init_cells_wall(void){
	n_cells_wall=0;
	long int i,j;
	int x,y,z,k;
	int dx,dy,dz,a,b,c;
	int max_neighbouring_cells;
	double max_interaction_length;
	max_interaction_length=max_interaction_length_rod_mesh;
	if(n_part==0){max_interaction_length=1.01*(0.5*max_vertex_spacing*pow(2.,1./6.));}
	printf("Max interaction length for wall cell list: %lf\n",max_interaction_length);
	nx_w = floor(0.95*box.L[0]/max_interaction_length);	//how many cells "fit" in one direction
	ny_w = floor(0.95*box.L[1]/max_interaction_length);
	nz_w = floor(0.95*box.L[2]/max_interaction_length);
	if(nx_w==0){nx_w=1;}
	if(ny_w==0){ny_w=1;}
	if(nz_w==0){nz_w=1;}
	current_wallcell_size=box.L[0]/nx_w;
	if(box.L[1]/ny_w>current_wallcell_size){current_wallcell_size=box.L[1]/ny_w;}
	if(box.L[2]/nz_w>current_wallcell_size){current_wallcell_size=box.L[2]/nz_w;}
	n_cells_wall=nx_w*ny_w*nz_w;
	printf("number of boundary cells is %ld, ",n_cells_wall);
	if(n_cells<27){max_neighbouring_cells=n_cells_wall;}
	else{max_neighbouring_cells=27;}
	printf("so max boundary cell neighbours is %d.\n",max_neighbouring_cells);

	/* Allocate memory for cell list */
	wallcells = calloc(n_cells_wall,sizeof(*wallcells));

	/* Construct neighbouring cell list */
	int unique,l;
	for(x=0;x<nx_w;x++) for(y=0;y<ny_w;y++) for(z=0;z<nz_w;z++){
		i=x*ny_w*nz_w+y*nz_w+z;
		wallcells[i].index=i;
		if(CELLTROUBLE){printf("\nBoundary cell %ld neighbours: ",i);}
		wallcells[i].np=0;
		wallcells[i].nv=0;
		wallcells[i].ne=0;
		wallcells[i].nf=0;
		/* Clear neighbour list first */
		for(l=max_neighbouring_cells-1;l>=0;l--){
			wallcells[i].neighbours[l]=n_cells_wall+1; // used as a marker as n_cells_wall+1 is out of range
		}
		/* Now add neighbours (including itself) */
		k=0;
		for(a=-1;a<2;a++) for(b=-1;b<2;b++) for(c=-1;c<2;c++){
			unique=1;
			dx=a;dy=b;dz=c;
			if(x+dx < 0) dx=nx_w-1; if(x+dx > nx_w-1) dx=-nx_w+1;
			if(y+dy < 0) dy=ny_w-1; if(y+dy > ny_w-1) dy=-ny_w+1;
			if(z+dz < 0) dz=nz_w-1; if(z+dz > nz_w-1) dz=-nz_w+1;
			j=(x+dx)*ny_w*nz_w+(y+dy)*nz_w+(z+dz);
			/* Check all neigbours to see whether cell is already there (happens for n_cells_wall<27) */
			for(l=max_neighbouring_cells-1;l>=0;l--){
				if(j==wallcells[i].neighbours[l]){unique=0;break;}
			}
			/* If the neighbouring cell is not yet there, add it to the list */
			if(unique){
				wallcells[i].neighbours[k]=j;
				if(CELLTROUBLE){printf("%ld ",j);}
				k++;
			}
		}
		neighbouring_cells_wall=k;
	}
	/* Put particles into cells */
	for(i=0;i<n_part;i++){
		j=coords2cell(part[i].pos[0],part[i].pos[1],part[i].pos[2],nx_w,ny_w,nz_w);
		if(wallcells[j].np+1 >= MAX_PART_CELL){printf("ERROR: Too many particles in a bcell!\n"); save_timeseries(); exit(666);}
		wallcells[j].particles = realloc(wallcells[j].particles,(wallcells[j].np+1)*sizeof(*wallcells[j].particles));
		// NOTE realloc is identical to malloc if called on NULL pointer. So no need to malloc first!
		wallcells[j].particles[ wallcells[j].np ] = i;
		part[i].cell_wall = j;
		wallcells[j].np++;
	}

	/* If confinement is defined through a triangulated mesh, add mesh objects into wall cell list */
	if(HAVE_MESH){
		/* Put vertices into cells */
		for(i=0;i<mesh.nvertices;i++){
			//printf("Vertex %ld has position (%lf %lf %lf)\n",i,mesh.vertices[i].pos[0],mesh.vertices[i].pos[1],mesh.vertices[i].pos[2]);
			j=coords2cell(mesh.vertices[i].pos[0],mesh.vertices[i].pos[1],mesh.vertices[i].pos[2],nx_w,ny_w,nz_w);
			//printf("Cell %ld got vertex %ld, ",j,i);
			if(wallcells[j].nv+1 >= MAX_PART_CELL){printf("ERROR: Too many vertices in a bcell!\n"); save_timeseries(); exit(666);}
			wallcells[j].vertices = realloc(wallcells[j].vertices,(wallcells[j].nv+1)*sizeof(*wallcells[j].vertices));
			wallcells[j].vertices[ wallcells[j].nv ] = i;
			mesh.vertices[i].cell = j;
			wallcells[j].nv++;
			//printf(" now has %ld vertices\n",wallcells[j].nv);
		}

		/* Put edges into cells */
		for(i=0;i<mesh.nedges;i++){
			//printf("Edge %ld has position (%lf %lf %lf)\n",i,mesh.edges[i].centerpos[0],mesh.edges[i].centerpos[1],mesh.edges[i].centerpos[2]);
			j=coords2cell(mesh.edges[i].centerpos[0],mesh.edges[i].centerpos[1],mesh.edges[i].centerpos[2],nx_w,ny_w,nz_w);
			//printf("Cell %ld got edge %ld, \n",j,i);
			if(wallcells[j].ne+1 >= MAX_PART_CELL){printf("ERROR: Too many edges in a bcell!\n"); save_timeseries(); exit(666);}
			wallcells[j].edges = realloc(wallcells[j].edges,(wallcells[j].ne+1)*sizeof(*wallcells[j].edges));
			wallcells[j].edges[ wallcells[j].ne ] = i;
			mesh.edges[i].cell = j;
			wallcells[j].ne++;
			//printf(" now has %ld edges\n",wallcells[j].ne);
		}

		/* Put faces into cells */
		for(i=0;i<mesh.nfaces;i++){
			//printf("Face %ld has position (%lf %lf %lf)\n",i,mesh.faces[i].centerpos[0],mesh.faces[i].centerpos[1],mesh.faces[i].centerpos[2]);
			j=coords2cell(mesh.faces[i].centerpos[0],mesh.faces[i].centerpos[1],mesh.faces[i].centerpos[2],nx_w,ny_w,nz_w);
			//printf("Cell %ld got face %ld, \n",j,i);
			if(wallcells[j].nf+1 >= MAX_PART_CELL){printf("ERROR: Too many faces in a bcell!\n"); save_timeseries(); exit(666);}
			wallcells[j].faces = realloc(wallcells[j].faces,(wallcells[j].nf+1)*sizeof(*wallcells[j].faces));
			wallcells[j].faces[ wallcells[j].nf ] = i;
			mesh.faces[i].cell = j;
			wallcells[j].nf++;
			//printf(" now has %ld faces\n",wallcells[j].nf);
		}
	}

	/* If confinement is defined through an analytical sphere surface, find cells near this surface */
	else if(INSIDE_SPHERE){
		sphere_cell_list.n_cells=0;
		double cell_size_x=box.L[0]/nx_w;
		double cell_size_y=box.L[1]/ny_w;
		double cell_size_z=box.L[2]/nz_w;
		double pos[3];
		for(x=0;x<nx_w;x++){
			for(y=0;y<ny_w;y++){
				for(z=0;z<nz_w;z++){
					/* Get center position of cell */
					pos[0]=x*cell_size_x-box.hL[0]+0.5*cell_size_x;
					pos[1]=y*cell_size_y-box.hL[1]+0.5*cell_size_y;
					pos[2]=z*cell_size_z-box.hL[2]+0.5*cell_size_z;
					/* Find the corresponding cell index */
					i=x*ny_w*nz_w+y*nz_w+z;

					/* If cell is near spherical surface, add to sphere_cell_list */
					/* Note: 2 times is technically more than enough, can be 1.5<...<2.0 */
					if( RADIUS-norm(pos) < 2*max_interaction_length_rod_point ){
						sphere_cell_list.cell[ sphere_cell_list.n_cells ]=&(wallcells[i]);
						sphere_cell_list.n_cells++;
					}
				}
			}
		}
	}

	/* Print the cell list. (%ld!) indicates particle in cell is not cell of particle */
	if(CELLTROUBLE){
		long int partincell;
		for(i=0;i<n_cells_wall;i++){
			printf("\nCell %ld (size %ld) contains particles: ",i,wallcells[i].np*sizeof(wallcells[i].particles));
			for(j=wallcells[i].np-1;j>=0;j--){
				partincell=wallcells[i].particles[j];
				if(part[partincell].cell_wall!=i){
					printf("(%ld!) ",partincell);
				}else{
					printf("%ld ",partincell);
				}
			}
			if(HAVE_MESH){
				printf("\nCell %ld (size %ld) contains vertices: ",i,wallcells[i].nv*sizeof(wallcells[i].vertices));
				for(j=wallcells[i].nv-1;j>=0;j--){
					partincell=wallcells[i].vertices[j];
					if(mesh.vertices[partincell].cell!=i){
						printf("(%ld!) ",partincell);
					}else{
						printf("%ld ",partincell);
					}
				}
				printf("\nCell %ld (size %ld) contains edges: ",i,wallcells[i].ne*sizeof(wallcells[i].edges));
				for(j=wallcells[i].ne-1;j>=0;j--){
					partincell=wallcells[i].edges[j];
					if(mesh.edges[partincell].cell!=i){
						printf("(%ld!) ",partincell);
					}else{
						printf("%ld ",partincell);
					}
				}
				printf("\nCell %ld (size %ld) contains faces: ",i,wallcells[i].nf*sizeof(wallcells[i].faces));
				for(j=wallcells[i].nf-1;j>=0;j--){
					partincell=wallcells[i].faces[j];
					if(mesh.faces[partincell].cell!=i){
						printf("(%ld!) ",partincell);
					}else{
						printf("%ld ",partincell);
					}
				}
			}
		}
		if(INSIDE_SPHERE){
			printf("\nSphere cell list contains %ld pointers to cells.\n",sphere_cell_list.n_cells);
			for(i=sphere_cell_list.n_cells-1;i>=0;i--){
				long int partincell;
				printf("\nCell contains %ld particles: ",sphere_cell_list.cell[i]->np);
				for(j=sphere_cell_list.cell[i]->np-1;j>=0;j--){
					partincell=sphere_cell_list.cell[i]->particles[j];
					printf("%ld ",partincell);
				}
			}
		}
		printf("\n");
	}
}
/* --------- Sets the initial properties of all the particles ---------
 * Relevent at the moment (for spherocylinders) are sphere/cylinder diameter d (should be 1, is used
 * as overall length scale for the simulation), total rod length l, and total volume occupied by all
 * particles.
 */
void init_particle_properties(void){
	long int i;
	int d;
	double r,l;		// radius, length, for readability's sake.
	Vpart=0.;
	for(i=0;i<n_part;i++){
		part[i].index=i;
		part[i].d=1.;	// This is the length scale! Change this and you will break nondimensionalization!
		part[i].l=lod*part[i].d;
		// if(genrand()<0.5){part[i].l = part[i].d;}
		part[i].hll=0.5*(part[i].l-part[i].d);
		r=0.5*part[i].d;
		l=part[i].l;
		part[i].vol = M_PI*4.0/3.0*r*r*r+M_PI*r*r*(l-2*r);
		Vpart += part[i].vol;
		for(d=0;d<DIM;d++){
			part[i].force[d]=0.;
			part[i].torque[d]=0.;
		}
		//if(i<n_part/4){part[i].isactive=0;}else{part[i].isactive=1;}
		part[i].isactive=1;
	}
}
/* --------- Calculates various interaction lengths based on particle properties ---------
 *
 */
void init_interaction_lengths(void){
	int i;
	double lmax,lmaxw,lmaxbb,lmaxmesh;
	//printf("--------------- calculating interaction lengths --------------\n");
	max_interaction_length_2rods=0.;
	max_interaction_length_bbox=0.;
	max_interaction_length_rod_point=0.;
	max_interaction_length_rod_mesh=0.;
	for(i=0;i<n_part;i++){
		lmax=(part[i].l-part[i].d+part[i].d*pow(2.,1./6.))*1.01;
		lmaxw=(part[i].hll+part[i].d*pow(2.,1./6.))*1.01;
		//lmaxw=(0.5*(part[i].l-part[i].d)+max_vertex_spacing+part[i].d*pow(2.,1/6.))*1.01;
		lmaxbb=part[i].d*(1.0+pow(2.,1./6.));
		lmaxmesh=(part[i].hll+0.5*max_vertex_spacing+part[i].d*pow(2.,1./6.))*1.01;
		if(lmax>max_interaction_length_2rods){
			max_interaction_length_2rods=lmax;
		}
		if(lmaxbb>max_interaction_length_bbox){
			max_interaction_length_bbox=lmaxbb;
		}
		if(lmaxw>max_interaction_length_rod_point){
			max_interaction_length_rod_point=lmaxw;
		}
		if(lmaxmesh>max_interaction_length_rod_mesh){
			max_interaction_length_rod_mesh=lmaxmesh;
		}
	}
	maxsep = 3 * max_interaction_length_2rods;
	if(maxsep > box.shL){ maxsep = box.shL; }
	//max_interaction_length_2rods=pow(2.,1/6.)*1.01; // for spheres
	//printf("------------ done calculating interaction lengths ------------\n");
}
/* --------- Initializes the crosslinks between rods ---------
 *
 */
void init_crosslinks(void){
	short int d;
	long int i,j,k;
	double pos1[DIM],pos2[DIM],lvec[DIM];
	tcrosslink *cl;

	n_crosslinks=n_part/2;
	MAX_CROSSLINKS=n_part*n_part;

	/* Allocate memory for the crosslinks */
	if(n_crosslinks>MAX_CROSSLINKS){
		printf("Error: number of crosslinks exceeds maximum array size. Exiting.\n");
		exit(666);
	}
	crosslink = calloc(MAX_CROSSLINKS,sizeof(*crosslink));

	for(i=0;i<n_crosslinks;i++){
		cl=&crosslink[i];
		cl->index=i;
		j=(long int) floor(genrand()*n_part);
		k=(long int) floor(genrand()*n_part);
		while(k==j){k=(long int) floor(genrand()*n_part);}
		cl->p1=&part[j];
		cl->p2=&part[k];
		cl->l1=(genrand()-0.5)*part[j].l;
		cl->l2=(genrand()-0.5)*part[k].l;
		for(d=0;d<DIM;d++){
			pos1[d]=cl->p1->pos[d]+cl->p1->or[d]*cl->l1;
			pos2[d]=cl->p2->pos[d]+cl->p2->or[d]*cl->l2;
		}
		nearestimage(pos1,pos2,lvec);
		cl->length=norm(lvec);
	}
}
/* --------- Puts particles in the box during initialization ---------
 * Applies periodic boundary conditions, but doesn't sort into cells.
 */
void putininitialbox(int p){
	int d;
	for(d=0;d<DIM;d++){
		while(part[p].pos[d]>box.hL[d]){part[p].pos[d]-=box.L[d];}
		while(part[p].pos[d]<-box.hL[d]){part[p].pos[d]+=box.L[d];}
		part[p].rpos[d]=part[p].pos[d];
	}
}
/* --------- Sets all the box dimensions ---------
 * Defines all the appropriate values for the box struct.
 */
void set_box_size(double x_box, double y_box, double z_box){
	box.L[0]=x_box;				box.L[1]=y_box;				box.L[2]=z_box;
	box.hL[0]=box.L[0]*0.5;		box.hL[1]=box.L[1]*0.5;		box.hL[2]=box.L[2]*0.5;
	box.iL[0]=1.0/box.L[0];		box.iL[1]=1.0/box.L[1];		box.iL[2]=1.0/box.L[2];
	box.ihL[0]=1.0/box.hL[0];	box.ihL[1]=1.0/box.hL[1];	box.ihL[2]=1.0/box.hL[2];
	box.shL = box.hL[0];
	if(box.hL[1]<box.shL){box.shL=box.hL[1];}
	if(box.hL[2]<box.shL){box.shL=box.hL[2];}
	box.bhL = box.hL[0];
	if(box.hL[1]>box.bhL){box.bhL=box.hL[1];}
	if(box.hL[2]>box.bhL){box.bhL=box.hL[2];}
	box.volume=box.L[0]*box.L[1]*box.L[2];
}
/* --------- Initializes the simulation volume (cubic box) ---------
 * Defines the simulation volume as being a cubic box with periodic boundardy conditions. Takes the
 * global parameter packfrac and n_part as input, so make sure those are properly defined before
 * this function runs.
 */
void init_cubic_box(void){

	printf("Defining a cubic simulation volume...\n");

	density=n_part*packfrac/Vpart;
	double lbox=pow(n_part/density,1.0/3.0);
	set_box_size(lbox,lbox,lbox);
	// maxsep=box.shL;
	printf(" n_part                       :  %ld\n",n_part);
	printf(" volume occupied by particles :  %f\n",Vpart);
	printf(" packing fraction             :  %f\n",Vpart/(box.L[0]*box.L[1]*box.L[2]));
	printf(" density                      :  %f\n",n_part/(box.L[0]*box.L[1]*box.L[2]));
	printf(" total volume                 :  %f\n",box.L[0]*box.L[1]*box.L[2]);
	printf(" and box                      :  (%f,%f,%f).\n",box.L[0],box.L[1],box.L[2]);
	printf("Finished defining simulation volume.\n");
}
/* --------- Initializes the simulation volume (elongated box) ---------
 * Defines the simulation volume as being a rectangular box with periodic boundardy conditions,
 * where the longest axis (z) is longer than shortest (x and y). Takes the global
 * parameter packfrac and n_part as input, so make sure those are properly defined before this
 * function runs.
 */
void init_elongated_box(void){

	printf("Defining an elongated simulation volume...\n");

	/* Set density as a global variable */
	density=n_part*packfrac/Vpart;

	/* Define a long and short axis with some length, then resize to obtain correct density */
	double sbox=1.,lbox=sbox*aspect_ratio_box;
	double resizefactor=pow(n_part/(density*lbox*sbox*sbox),1.0/3.0);
	lbox*=resizefactor;
	sbox*=resizefactor;
	set_box_size(lbox,sbox,sbox);
	// maxsep=box.shL; // Max separation that makes sense for g(r) is half the shortest length of the periodic volume.
	printf(" n_part                       :  %ld\n",n_part);
	printf(" volume occupied by particles :  %f\n",Vpart);
	printf(" packing fraction             :  %f\n",Vpart/(box.L[0]*box.L[1]*box.L[2]));
	printf(" density                      :  %f\n",n_part/(box.L[0]*box.L[1]*box.L[2]));
	printf(" total volume                 :  %f\n",box.L[0]*box.L[1]*box.L[2]);
	printf(" and box                      :  (%f,%f,%f).\n",box.L[0],box.L[1],box.L[2]);
	printf("Finished defining simulation volume.\n");
}
/* --------- Initializes the simulation volume (planar box) ---------
 * Defines the simulation volume as being a rectangular box with periodic boundardy conditions,
 * where the longest axis (z) is twice as long as the shortest (x and y). Takes the global
 * parameter packfrac and n_part as input, so make sure those are properly defined before this
 * function runs.
 */
void init_planar_box(double height){

	printf("Defining a planar simulation volume...\n");

	/* Set density as a global variable */
	density=n_part*packfrac/Vpart;

	/* Define a long and short axis with some length, then resize to obtain correct density */
	double sbox=height,lbox=1.;
	double resizefactor=pow(n_part/(density*lbox*lbox),1.0/2.0);
	printf("resizefactor density height %lf %lf %lf\n",resizefactor,density,sbox);
	lbox*=resizefactor;
	set_box_size(lbox,sbox,lbox);
	// maxsep=lbox*0.5; // Max separation that makes sense for g(r) is half the shortest length of the periodic volume.
	printf("n_part=                         %ld\n",n_part);
	printf("volume occupied by particles =  %f\n",Vpart);
	printf("volume fraction =               %f\n",Vpart/(box.L[0]*box.L[1]*box.L[2]));
	printf("area fraction =                 %f\n",( n_part * (0.25*M_PI+lod) )/(box.L[0]*box.L[2]));
	printf("density =                       %f\n",n_part/(box.L[0]*box.L[1]*box.L[2]));
	printf("total volume =                  %f\n",box.L[0]*box.L[1]*box.L[2]);
	printf("and box =                       (%f,%f,%f).\n",box.L[0],box.L[1],box.L[2]);
	printf("Finished defining simulation volume.\n");
}
/* --------- Rescales the mesh and sets box size ---------
 * Assuming all particles are confined within the mesh, rescales all the vertex coordinates
 * so that the packing fraction inside the mesh matches the packing fraction that is requested
 * for simulation. If the confinement is static, snugly fits the simulation volume around the
 * confining volume. If it is dynamic, makes the box larger.
 */
void init_mesh_and_box(void){
	int i,j,d;
	double packfrac_old;
	double oldmeshvolume=mesh.volume;
	double min[DIM]={0.},max[DIM]={0.};
	printf("---------------- rescaling mesh for right eta ----------------\n");

	/* Calculate radius of spherical confinement ? */
	RADIUS=pow(3.0*Vpart/(4.0*M_PI*packfrac),1.0/3.0);
	RADIUSSQ=RADIUS*RADIUS;

	/* Calculate the packing fraction within the input confinement */
	packfrac_old=Vpart/oldmeshvolume;

	/* In case we have no particles, just a mesh */
	if(Vpart==0. && n_part==0){packfrac_old=0.5;packfrac=0.001;}

	/* Rescale vertex coordinates to obtain the correct packing fraction */
	double rescalefactor=pow(packfrac_old/packfrac,1.0/3.0);
	for(i=0;i<mesh.nvertices;i++){
		for(d=0;d<DIM;d++){
			mesh.vertices[i].pos[d]*=rescalefactor;
			if(mesh.vertices[i].pos[d]<min[d]){min[d]=mesh.vertices[i].pos[d];}
			if(mesh.vertices[i].pos[d]>max[d]){max[d]=mesh.vertices[i].pos[d];}
		}
		for(j=0;j<mesh.vertices[i].nnb;j++){
			mesh.vertices[i].eqr[j]*=rescalefactor;
			for(d=0;d<DIM;d++){
				mesh.vertices[i].eqv[j][d]*=rescalefactor;
			}
		}
	}
	/* Prevent sheets or planes from messing stuff up */
	for(d=0;d<DIM;d++){
		if(min[d]==0.){min[d]=-10.;}
		if(max[d]==0.){max[d]=10.;}
	}
	/* Rescale all resultant variables such as center positions */
	for(i=0;i<mesh.nfaces;i++){
		for(d=0;d<DIM;d++){
			mesh.faces[i].centerpos[d]*=rescalefactor;
		}
	}
	for(i=0;i<mesh.nedges;i++){
		for(d=0;d<DIM;d++){
			mesh.edges[i].vec[d]*=rescalefactor;
			mesh.edges[i].centerpos[d]*=rescalefactor;
		}
		mesh.edges[i].elen*=rescalefactor;
		mesh.edges[i].eq_elen*=rescalefactor;
		mesh.edges[i].hll*=rescalefactor;
	}
	max_vertex_spacing*=rescalefactor;
	max_vertex_spacing_initial*=rescalefactor;
	mesh.volume=(packfrac_old/packfrac)*mesh.volume;

	/* Set equilibrium bending angles */
	tedge *e;
	tface *f1,*f2;
	double tempvec[3],sin_th;
	for(i=0;i<mesh.nedges;i++){
		e=&mesh.edges[i];
		f1=&mesh.faces[ e->faces[0] ];
		f2=&mesh.faces[ e->faces[1] ];
		crossprod(tempvec,f1->outwardnormal,f2->outwardnormal);
		sin_th=inprod(tempvec,e->or); // n1 x n2 . e
		e->eq_sin_h_th=samesign( sqrt(0.5*(1-inprod(f1->outwardnormal,f2->outwardnormal))) , sin_th );
		//printf("eq bend angle: %f\n",e->eq_sin_h_th);
	}

	/* Define simulation volume */
	if(!DYNAMIC_MESH){
		// 		set_box_size(1.35*(max[0]-min[0]),1.35*(max[1]-min[1]),1.35*(max[2]-min[2]));
		set_box_size(1.5*(max[0]-min[0])+max_vertex_spacing,1.5*(max[1]-min[1])+max_vertex_spacing,1.5*(max[2]-min[2])+max_vertex_spacing);
	}
	else{
		set_box_size(3.0*(max[0]-min[0])+max_vertex_spacing,3.0*(max[1]-min[1])+max_vertex_spacing,3.0*(max[2]-min[2])+max_vertex_spacing);
		max_vertex_spacing*=1.2;
	}

	/* Precalculate some face properties for later use */
	for(i=0;i<mesh.nfaces;i++){
		update_face_properties( &(mesh.faces[i]) );
	}
	for(i=0;i<mesh.nedges;i++){
		update_edge_properties( &(mesh.edges[i]) );
	}

	/* Set size for g(r) calculation */
	// maxsep=box.shL; // Max separation for g(r).

	/* Print info */
	printf(" n_part                        :  %ld\n",n_part);
	printf(" volume occupied by particles  :  %f\n",Vpart);
	printf(" max vertex spacing            :  %f\n",max_vertex_spacing);
	printf(" packing fraction box          :  %f\n",Vpart/(box.L[0]*box.L[1]*box.L[2]));
	printf(" packing fraction confinement  :  %f\n",Vpart/mesh.volume);
	printf(" density                       :  %f\n",n_part/(box.L[0]*box.L[1]*box.L[2]));
	printf(" confinement volume            :  %f\n",mesh.volume);
	printf(" total volume                  :  %f\n",box.L[0]*box.L[1]*box.L[2]);
	printf(" and box                       :  (%f,%f,%f).\n",box.L[0],box.L[1],box.L[2]);
	printf("----------------      done rescaling mesh     ----------------\n");

	return;
}
/* --------- Initializes the simulation volume and surface (sphere) ---------
 *
 */
void init_onsphere_and_box(void){
	int i;
	double Apart=0;	// area occupied by particles.
	double r,l;		// radius, length, for readability's sake.

	/* Calculate volume occupied by particles (rods) */
	for(i=0;i<n_part;i++){
		r=0.5*part[i].d;
		l=part[i].l;
		Apart+=(l-2*r)*(2*r)+M_PI*r*r;
	}
	RADIUS=pow(Apart/(4.*M_PI*areafrac),1.0/2.0);
	RADIUSSQ=RADIUS*RADIUS;

	packfrac=0.8*Vpart/(pow(2*RADIUS,3.0));
	density=n_part*packfrac/Vpart;
	double lbox=pow(n_part/density,1.0/3.0);
	set_box_size(1.5*lbox,1.5*lbox,1.5*lbox);
	// maxsep=lbox*0.5;

	/* Print info */
	printf("n_part =                        %ld\n",n_part);
	printf("volume occupied by particles =  %f\n",Vpart);
	printf("area occupied by particles =    %f\n",Apart);
	printf("packing fraction box =          %f\n",Vpart/(box.L[0]*box.L[1]*box.L[2]));
	printf("area fraction on sphere =       %f\n",Apart/(4.*M_PI*pow(RADIUS,2.0)));
	printf("density =                       %f\n",n_part/(box.L[0]*box.L[1]*box.L[2]));
	printf("total area sphere =             %f\n",(4.*M_PI*RADIUS));
	printf("total volume =                  %f\n",box.L[0]*box.L[1]*box.L[2]);
	printf("and box =                       (%f,%f,%f).\n",box.L[0],box.L[1],box.L[2]);
}
/* --------- Initializes the simulation volume and surface (sphere) ---------
 *
 */
void init_insphere_and_box(void){
	RADIUS = pow( (3.0/(4.0*M_PI)) * Vpart / startpackfrac,1.0/3.0);
	RADIUSSQ = RADIUS*RADIUS;
	set_box_size(2.0*RADIUS,2.0*RADIUS,2.0*RADIUS);
	packfrac = Vpart / box.volume; // Packing fraction of cubic simulation box
	density = n_part * packfrac / Vpart;
	maxsep = box.shL;

	/* Print info */
	printf("n_part =                        %ld\n",n_part);
	printf("RADIUS =                        %f\n",RADIUS);
	printf("volume occupied by particles =  %f\n",Vpart);
	printf("packing fraction box =          %f\n",Vpart/(box.L[0]*box.L[1]*box.L[2]));
	printf("volume fraction in sphere =     %f\n",Vpart*3./(4.*M_PI*pow(RADIUS,3.0)));
	printf("density =                       %f\n",n_part/(box.L[0]*box.L[1]*box.L[2]));
	printf("total volume sphere =           %f\n",(4.*M_PI*pow(RADIUS,3.0))/3.);
	printf("total volume =                  %f\n",box.L[0]*box.L[1]*box.L[2]);
	printf("and box =                       (%f,%f,%f).\n",box.L[0],box.L[1],box.L[2]);
}
/* --------- Initializes the simulation volume and surface (sphere) ---------
 *
 */
void init_incylinder_and_box(void){
	double sbox=5*diameter_cylinder,lbox=1.;

	/* Set density as a global variable, correct for the fact it's in a cylinder */
	double packfracfactor=(M_PI*pow(0.5*diameter_cylinder,2.0)) / (sbox*lbox*sbox);
	density=n_part*(packfrac*packfracfactor)/Vpart;

	/* Define a long and short axis with some length, then resize to obtain correct density */
	double resizefactor=n_part/(density*sbox*lbox*sbox);
	lbox*=resizefactor;
	set_box_size(sbox,lbox,sbox);
	// maxsep=sbox*0.5; // Max separation that makes sense for g(r) is half the shortest length of the periodic volume.
	printf("n_part=                         %ld\n",n_part);
	printf("volume occupied by particles =  %f\n",Vpart);
	printf("packing fraction =              %f\n",Vpart/(box.L[0]*box.L[1]*box.L[2]));
	printf("density =                       %f\n",n_part/(box.L[0]*box.L[1]*box.L[2]));
	printf("total volume =                  %f\n",box.L[0]*box.L[1]*box.L[2]);
	printf("and box =                       (%f,%f,%f).\n",box.L[0],box.L[1],box.L[2]);
}
/* --------- Loads simulation parameters from a file ---------
 * This function reads simulation parameters from a file. This file can contain a header:
 * lines indicated by '#' at the start of the document will not be read.
 *
 * The rest of the file will contain any of the following parameters:
 * - Packing fraction "eta" or "packfrac"	-> sets packfrac
 * - Number of particles "N" or "n_part"	-> sets n_part
 * - Rod aspect ratio "l/d" or "lod"		-> sets lod
 * - Active force "F_A" or "Peclet"			-> sets active_force
 *
 * The input for this function is the name of the text file containing the simulation
 * parameters, and the number of the line it should take the parameters from (these files
 * can contain parameters for multiple simulations).
 */
void load_parameters(char *filename, int linenum){
	char par[10][50],val[10][50],*trail;
	char line[500];
	int i,numparams;

	/* Open file in read-only mode */
	FILE *inputfp=fopen(filename,"r");
	if(!inputfp){printf("Error: could not open parameter file '%s' specified by '-p'!\n",filename);exit(42);}

	/* Skip any headers we encounter */
	skipheaders(inputfp);

	/* Skip to the correct line of parameters */
	for(i=0;i<=linenum;i++){
		if(!fgets(line,sizeof(line),inputfp)){
			printf("Error skipping to line in parameter file!\n");
			exit(42);
		}
	}

	/* Scan the line */
	numparams=sscanf(line,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",
		   par[0],val[0],par[1],val[1],par[2],val[2],par[3],val[3],par[4],val[4],
		   par[5],val[5],par[6],val[6],par[7],val[7],par[8],val[8],par[9],val[9]);

	/* Process the arguments and set parameters */
	for(i=0;i<numparams/2;i++){
		//printf("Parameter is '%s', value is '%s'\n",par[i],val[i]);
		if(strcmp(par[i],"N")==0 || strcmp(par[i],"n_part")==0){
			n_part=(int) strtol(val[i],&trail,10);
			printf("From -p: set number of particles to %ld.\n",n_part);
		}
		else if(strcmp(par[i],"eta")==0 || strcmp(par[i],"packfrac")==0){
			packfrac=strtod(val[i],&trail);
			printf("From -p: set packing fraction to %lf.\n",packfrac);
		}
		else if(strcmp(par[i],"l/d")==0 || strcmp(par[i],"lod")==0){
			lod=strtod(val[i],&trail);
			printf("From -p: set aspect ratio to %lf.\n",lod);
		}
		else if(strcmp(par[i],"Pe")==0 || strcmp(par[i],"Peclet")==0){
			Peclet=strtod(val[i],&trail);
			printf("From -p: set (rotational) Peclet number to %lf.\n",Peclet);
		}
		else{
			printf("Error: unrecognized parameter '%s' encountered in file! Exiting!\n",par[i]);
			exit(42);
		}
	}
	if(SPHERES && lod!=1.0){
		printf("Error: particles defined as spheres cannot have lod!=1.0\n");
		exit(42);
	}
	fclose(inputfp);
	return;
}
/* --------- Loads in a particle configuration from a file ---------
 * Loads in a particle configuration from a file that has the same structure as the snapshots this
 * program saves. That is to say, multiple snapshots can be contained in a single file separated by
 * the '&' sign. The start of the file might have a header, indicated by lines starting with '#'.
 * We first move the file pointer past those. Then, we then scan through the input file to find the
 * number of snapshots contained within, and move the file pointer to the start of the last
 * snapshot in the file. Then we read in a snapshot according to the following structure:
 *
 * &numberofparticles
 * length_box_x length_box_y length_box_z
 * (numberofparticles times this:) rod_x rod_y rod_z rod_u_x rod_u_y rod_u_z rod_diameter rod_length
 */
void load_snapshot(char *filename){
	int d,i,ampersands=0;

	printf("Loading particle configuration from file '%s'...\n",filename);

	/* Open file in read-only mode */
	FILE *inputfp=fopen(filename,"r");

	/* Obtain run number */
	char *trailingstr;
	char *ptr_to_run=strchr(filename,'_');
	if(ptr_to_run){
		run=1+(int) strtol(ptr_to_run,&trailingstr,10);
		//printf("Run number: %04d\n",run);
	}

	/* Check the file is opened correctly */
	if(inputfp==NULL){fprintf(stderr,"Error opening particle config file!\n"); exit(666);}

	/* Skip the headers */
	skipheaders(inputfp);

	/* Because we can save multiple frames in one file, we check for that first */
	char firstchar;
	if(!fscanf(inputfp,"%c",&firstchar)){printf("Fatal error while reading inputfp file. Exiting.\n");exit(666);}
	if(firstchar=='&'){ampersands=1;}

	/* Get number of frames in file (every & indicates a frame) */
	char ch;
	int nframesinfile=1;
	if(ampersands){
		while((ch=getc(inputfp))!=EOF){
			if(ch=='&'){nframesinfile++;}
		}
	}
	rewind(inputfp);
	skipheaders(inputfp);

	/* If there are multiple frames in the inputfp, we want the last one */
	if(ampersands){

		/* so we have to scan the frame sizes to know where it starts */
		unsigned int *framesizearray;
		framesizearray=calloc(nframesinfile,sizeof(*framesizearray));
		ch=getc(inputfp);
		unsigned int framesize=1; // we already scanned one '&' in the line above
		unsigned int framescounted=0;
		while(1){
			ch=getc(inputfp);
			if(ch==EOF){
				framesizearray[framescounted]=framesize;
				break;
			}
			if(ch=='&'){
				framesizearray[framescounted]=framesize;
				framesize=0;
				framescounted++;
			}
			framesize++;
		}

		/* Small check to see if anything went wrong */
		if(nframesinfile!=framescounted+1){
			printf("Error: scanned frames (%d) != total frames (%d).\n",nframesinfile,framescounted);
			exit(666);
		}

		/* Now rewind the file pointer to the start of the last frame */
		rewind(inputfp);
		skipheaders(inputfp);
		for(i=0;i<nframesinfile-1;i++){
			fseek(inputfp,framesizearray[i],SEEK_CUR);
		}

		/* Free the array, we don't need it anymore */
		free(framesizearray);
	}

	/* Now we can read in data, start with npart and allocate array for particles */
	double box_x,box_y,box_z;
	if(ampersands){ch=getc(inputfp);}
	if(!fscanf(inputfp,"%ld\n",&n_part)){
		printf("Error: could not read number of particles.\n");
		exit(666);
	}
	printf(" Number of particles       : %ld\n",n_part);
	part = calloc(n_part,sizeof(*part));

	/* Now scan in the box dimensions */
	if(fscanf(inputfp,"%lf %lf %lf\n",&box_x,&box_y,&box_z)!=DIM){
		printf("Error: input file box dimensions do not match dimensions of simulation.\n");
		exit(666);
	}
	set_box_size(box_x,box_y,box_z); // Initialize simulation volume
	if(ON_SPHERE){
		RADIUS=0.5*pow(0.8,1.0/3.0)*box.L[0];
		RADIUSSQ=RADIUS*RADIUS;
	}else if(INSIDE_SPHERE){
		RADIUS=0.5*pow(0.8,1.0/3.0)*box.L[0]+1.0;
		RADIUSSQ=RADIUS*RADIUS;
	}
	printf(" Loaded box dimensions     : %lf %lf %lf\n",box.L[0],box.L[1],box.L[2]);

	/* Now we load in the particles */
	double p1,p2,p3,p4,p5,p6,p7,p8,p9,p10;
	char p0;
	int num_pvars;
	double rvec[DIM];
	Vpart=0;
	for(i=0;i<n_part;i++){
		num_pvars=fscanf(inputfp,"%c %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",&p0,&p1,&p2,&p3,&p4,&p5,&p6,&p7,&p8,&p9,&p10);
		if(num_pvars<9){
			printf("Error: not enough particle parameters! ");
			printf("Needs type (1), position (3), orientation (3), total rod length (1) and rod diameter (1)\n");
		}
		if(num_pvars>=9){
			part[i].pos[0]=p1;
			part[i].pos[1]=p2;
			part[i].pos[2]=p3;
			part[i].or[0]=p4;
			part[i].or[1]=p5;
			part[i].or[2]=p6;
			normalize(part[i].or);
			part[i].d=p7;
			part[i].l=p8;
			part[i].hll=0.5*(part[i].l-part[i].d);
		}
		if(num_pvars>=10){
			part[i].nem=p9;
			part[i].localpackfrac=p10;
		}
		/* Calculate perpendicular vectors */
		fillrvec(rvec);
		normalize(rvec);
		crossprod(part[i].orper1,part[i].or,rvec);
		normalize(part[i].orper1);
		crossprod(part[i].orper2,part[i].or,part[i].orper1);
		normalize(part[i].orper2);
		/* Set forces and torques to zero for safety */
		for(d=0;d<DIM;d++){
			part[i].force[d]=0.;
			part[i].torque[d]=0.;
		}
		/* Check for unsupported features */
		if(i>0){
			if(lod!=p8/p7){
				printf("Error: aspect ratio polydispersity detected, is unsupported!\n");
				exit(666);
			}
		}
		lod=p8/p7;
		if(fabs(1-part[i].d)>1E-10){
			printf("Error: diameter (%lf) used as length scale is not 1, so nondimensionalization is invalid!\n",part[i].d);
			exit(666);
		}
		/* We don't store whether a particle is active or not, so this is set by input parameter */
		if(Peclet==0){part[i].isactive=0;}else{part[i].isactive=1;}
		/* We'll need the total particle volume for the density calculation */
		Vpart+=pow(part[i].d/2.0,3)*M_PI*4.0/3.0+pow(part[i].d/2.0,2)*M_PI*(part[i].l-part[i].d);
	}

	/* Now we adjust the global variables */
	density=n_part/(box.L[0]*box.L[1]*box.L[2]);
	packfrac=Vpart/(box.L[0]*box.L[1]*box.L[2]);
	printf(" Total volume of particles :  %lf\n",Vpart);
	printf(" Number density            :  %lf\n",density);
	printf(" Packing fraction          :  %lf\n",packfrac);
	// maxsep=box.shL;

	fclose(inputfp);
	printf("Done loading from file '%s'.\n",filename);
	return;
}
/* --------- Loads in a confining volume as a mesh from a .ply file ---------
 * This function loads in 3D mesh that is saved in the (ASCII) .ply file format. It currently
 * loads in vertices, faces and the volume of the supplied mesh if those are specified. Once
 * all is loaded in, it constructs a neighbour list for the vertices on the basis of the
 * connections specified by the faces.
 */
void load_mesh_from_ply(char *filename){
	long int i,j;
	int d;

	/* Open file in read-only mode */
	FILE *fp=fopen(filename,"r");
	if(!fp){printf("Error: could not open .ply file '%s' specified by '-b'.\n",filename);exit(42);}

	char plytag[3];
	if(!fscanf(fp,"%s\n",plytag)){printf("Error loading start of file. You sure you input one?\n");exit(42);}
	if(strcmp(plytag,"ply")!=0){printf("Error: file specified not a .ply file!\n");exit(42);}
	else{printf("Tag 'ply' found: .ply file successfully identified. Loading.\n");}

	char line[400],firstword[50],name_elem[50],val_elem[50];
	char *trailingstr;
	int IN_HEADER=1,FOUND_VERTICES=0,FOUND_FACES=0;
	int header_lines_read=0;

	printf("-----------------     Reading .ply header     ----------------\n");
	while(IN_HEADER){

		/* Read a line */
		if(!fgets(line,sizeof(line),fp)){printf("Error while reading header.\n");}

		/* Read a word to identify what element we have */
		sscanf(line,"%s\n",firstword);

		/* Check for the format and version tag */
		if(strcmp(firstword,"format")==0){
			printf("%s",line);
		}
		/* Check for comments */
		else if(strcmp(firstword,"comment")==0){
			printf("%s",line);
		}

		/* Identify elements */
		else if(strcmp(firstword,"element")==0){

			sscanf(line,"%s %s %s\n",firstword,name_elem,val_elem);

			/* If the file contains vertices */
			if(strcmp(name_elem,"vertex")==0){

				/* Set number of vertices and allocate memory for them */
				mesh.nvertices=(long int) strtol(val_elem,&trailingstr,10);
				mesh.vertices=calloc(mesh.nvertices,sizeof(*mesh.vertices));
				printf("Found %ld vertices for loading.\n",mesh.nvertices);

				/* Check number of dimensions by checking for properties x y z */
				for(i=0;i<DIM;i++){
					if(!fgets(line,sizeof(line),fp)){printf("Error while reading header.\n");}
					sscanf(line,"%s\n",firstword);
					if(strcmp(firstword,"property")!=0){
						printf("Number of vertex coordinates in .ply file incompatible with dimension (!=%d)!\n",DIM);
						printf("In line: '%s'\n",line);
						exit(42);
					}
				}
				FOUND_VERTICES=1;
			}

			/* If the file contains faces */
			else if(strcmp(name_elem,"face")==0){

				/* Set number of vertices and allocate memory for them */
				mesh.nfaces=(long int) strtol(val_elem,&trailingstr,10);
				mesh.faces=calloc(mesh.nfaces,sizeof(*mesh.faces));
				printf("Found %ld faces for loading.\n",mesh.nfaces);

				/* Skip the face property line (NOTE: limits compatibility) */
				if(!fgets(line,sizeof(line),fp)){printf("Error while reading header.\n");}
				FOUND_FACES=1;
			}

			/* If the file specifies the volume of the mesh */
			else if(strcmp(name_elem,"volume")==0){
				mesh.volume=strtod(val_elem,&trailingstr);
				printf("Found mesh volume of %lf.\n",mesh.volume);
			}
		}

		/* If we reach the end of the header, stop the while loop */
		else if(strcmp(firstword,"end_header")==0){
			IN_HEADER=0;
		}

		/* Safety statement */
		header_lines_read++;
		if(header_lines_read>1000){
			printf("Read >1000 header lines. Can't be right. Check for 'end_header' in .ply file? Exiting.\n");
			exit(42);
		}
	}
	printf("----------------   done reading .ply header   ----------------\n");

	printf("---------------- now loading contents of mesh ----------------\n");
	/* Load vertices if found */
	if(FOUND_VERTICES){
		for(i=0;i<mesh.nvertices;i++){
			mesh.vertices[i].index=i;
			for(d=0;d<DIM;d++){
				if(!fscanf(fp,"%lf ", &(mesh.vertices[i].pos[d]) )){printf("Error loading a vertex! Exiting.\n");exit(42);}
			}
			if(MESHTROUBLE){printf("%lf %lf %lf\n",mesh.vertices[i].pos[0],mesh.vertices[i].pos[1],mesh.vertices[i].pos[2]);}
		}
	}

	/* Load faces if found */
	if(FOUND_FACES){
		char first=1;
		unsigned long int squares_turned_to_triangles=0;
		tface *f;
		for(i=0;i<mesh.nfaces;i++){
			f=&mesh.faces[i];
			f->index=i;
			if(!fscanf(fp,"%hu ", &(f->nvertices) )){printf("Error loading number of faces! Exiting.\n");exit(42);}
			f->vertices=calloc(f->nvertices,sizeof(*f->vertices));
			f->nedges=0; // Set to zero now, will be updated later when we link edges to faces
			for(j=0;j<f->nvertices;j++){
				if(!fscanf(fp,"%ld ", &(f->vertices[j]) )){printf("Error loading a face! Exiting.\n");exit(42);}
				if(MESHTROUBLE){printf("%ld ",f->vertices[j]);}
			}
			if(MESHTROUBLE){printf("\n");}
		}
		/* We need triangular faces, so if we find non-triangular ones we transform them into triangles */
		for(i=0;i<mesh.nfaces;i++){
			f = &mesh.faces[i];
			if(f->nvertices==4){
				if(first){printf("Faces found with 4 vertices: transforming them into triangles!\n");}
				first=0;
				tface *f_new;
				/* Allocate memory for another face to the list and get pointer to it */
				mesh.faces = realloc(mesh.faces,(mesh.nfaces+1)*sizeof(*mesh.faces));
				f = &mesh.faces[i]; // Have to reset pointer since realloc might have moved it
				f_new = &mesh.faces[mesh.nfaces];
				memset(f_new,0,sizeof(*f_new)); // Initialize new face memory since realloc doesn't
				mesh.nfaces++;
				/* Allocate memory for the vertices of this face */
				f_new->nvertices = 3;
				f_new->vertices = calloc(f_new->nvertices,sizeof(*f_new->vertices));
				/* Keep order of vertices to preserve normal direction */
				f_new->vertices[0] = f->vertices[0];
				f_new->vertices[1] = f->vertices[2];
				f_new->vertices[2] = f->vertices[3];
				f_new->nedges = 0;
				/* Shrink the old face down to 3 vertices */
				f->vertices = realloc(f->vertices,(f->nvertices-1)*sizeof(*f->vertices));
				f->nvertices--;
				squares_turned_to_triangles++;
			}else if(f->nvertices>4){printf("Face found with >4 vertices! Exiting.\n");exit(42);}
		}
		printf("Loaded %ld faces. Found %lu squares and turned them into triangles.\n",mesh.nfaces,squares_turned_to_triangles);
	}

	/* Set number of neighbours to zero, neighbours to impossible value */
	for(i=0;i<mesh.nvertices;i++){
		mesh.vertices[i].nnb=0;
		for(j=0;j<MAX_VERTEX_NB;j++){mesh.vertices[i].nb[j]=mesh.nvertices;}
	}

	/* Center the mesh at (0,0,0) if it isn't on load */
	double avpos[DIM]={0.};
	double min[3]={1E6,1E6,1E6},max[3]={0.,0.,0.};
	for(i=0;i<mesh.nvertices;i++){
		for(d=0;d<DIM;d++){
			if(mesh.vertices[i].pos[d]<min[d]){
				min[d]=mesh.vertices[i].pos[d];
			}
			if(mesh.vertices[i].pos[d]>max[d]){
				max[d]=mesh.vertices[i].pos[d];
			}
		}
	}
	//printf("min: %f %f %f\n",min[0],min[1],min[2]);
	//printf("max: %f %f %f\n",max[0],max[1],max[2]);
	for(d=0;d<DIM;d++){avpos[d]=0.5*(max[d]+min[d]);}
	for(i=0;i<mesh.nvertices;i++){
		for(d=0;d<DIM;d++){
			mesh.vertices[i].pos[d]-=avpos[d];
		}
	}

	/* Construct vertex neighbour lists and edges */
	long int duplicate,vertex_nr_j,vertex_nr_k,k;
	unsigned int m;
	tvertex *ptr_j,*ptr_k;
	tedge *ptr_e;
	long int nedge=0;
	for(i=0;i<mesh.nfaces;i++){
		for(j=0;j<mesh.faces[i].nvertices;j++){
			for(k=0;k<mesh.faces[i].nvertices;k++){

				vertex_nr_j=mesh.faces[i].vertices[j];
				vertex_nr_k=mesh.faces[i].vertices[k];

				if(vertex_nr_j!=vertex_nr_k){

					ptr_j=&(mesh.vertices[vertex_nr_j]);
					ptr_k=&(mesh.vertices[vertex_nr_k]);

					duplicate=0;
					for(m=0;m<ptr_j->nnb;m++){
						if(ptr_j->nb[m]==vertex_nr_k){duplicate=1;break;}
					}
					if(!duplicate){
						ptr_j->nb[ptr_j->nnb]=vertex_nr_k;
						vecsubtract(ptr_j->eqv[ptr_j->nnb], ptr_k->pos, ptr_j->pos );
						ptr_j->eqr[ptr_j->nnb]=norm(ptr_j->eqv[ptr_j->nnb]);
						//printf("%d neighbours %d:\n",vertex_nr_j,vertex_nr_k);
						//printf("pos %d = %f %f %f\n",vertex_nr_j,ptr_j->pos[0],ptr_j->pos[1],ptr_j->pos[2]);
						//printf("pos %d = %f %f %f\n",vertex_nr_k,ptr_k->pos[0],ptr_k->pos[1],ptr_k->pos[2]);
						//printf("eqv = %f %f %f\n",ptr_j->eqv[ptr_j->nnb][0],ptr_j->eqv[ptr_j->nnb][1],ptr_j->eqv[ptr_j->nnb][2]);
						//printf("eqr = %f\n",ptr_j->eqr[ptr_j->nnb]);
						ptr_j->nnb++;
						/* Edges */
						if(vertex_nr_j>vertex_nr_k){
							mesh.edges=realloc(mesh.edges,(nedge+1)*sizeof(*mesh.edges));
							ptr_e=&(mesh.edges[nedge]);
							ptr_e->index=nedge;
							ptr_e->vertices[0]=vertex_nr_j;
							ptr_e->vertices[1]=vertex_nr_k;
							vecsubtract(ptr_e->vec, ptr_k->pos, ptr_j->pos);
							direction(ptr_e->or,ptr_e->vec);
							ptr_e->elen=norm(ptr_e->vec);
							ptr_e->eq_elen=ptr_e->elen;
							ptr_e->hll=0.5*ptr_e->elen;
							for(d=0;d<3;d++){
								ptr_e->centerpos[d]=ptr_j->pos[d]+0.5*ptr_e->vec[d];
							}
							nedge++;
							//printf("Edge %d connects vertices %d and %d:\n",nedge,ptr_e->vertices[0],ptr_e->vertices[1]);
							//printf("  vec: %f %f %f\n",ptr_e->vec[0],ptr_e->vec[1],ptr_e->vec[2]);
							//printf("   or: %f %f %f\n",ptr_e->or[0],ptr_e->or[1],ptr_e->or[2]);
							//printf("  hll: %f\n",ptr_e->hll);
							//printf(" cpos: %f %f %f\n\n",ptr_e->centerpos[0],ptr_e->centerpos[1],ptr_e->centerpos[2]);
						}
					}
				}
			}
		}
	}
	mesh.nedges=nedge;
	printf("Number of edges: %ld\n",mesh.nedges);

	/* Calculate the max distance between neighbouring vertices for use in cell list */
	double dist;
	max_vertex_spacing=0.;
	for(i=0;i<mesh.nvertices;i++){
		for(j=0;j<mesh.vertices[i].nnb;j++){
			dist=mesh.vertices[i].eqr[j];
			if(dist>max_vertex_spacing){
				max_vertex_spacing=dist;
			}
		}
	}
	max_vertex_spacing_initial=max_vertex_spacing;

	/* Add vertex to face references */
	long int vertex_of_face;
	for(i=0;i<mesh.nvertices;i++){
		mesh.vertices[i].innfaces=0;
		for(j=0;j<mesh.nfaces;j++){
			for(k=0;k<mesh.faces[j].nvertices;k++){
				vertex_of_face=mesh.faces[j].vertices[k];
				if(i==vertex_of_face){
					duplicate=0;
					for(m=mesh.vertices[i].innfaces;m>0;m--){
						if(mesh.vertices[i].infaces[m]==j){duplicate=1;break;}
					}
					if(!duplicate){
						if(mesh.vertices[i].innfaces>MAX_FACES_PER_VERTEX){
							printf("Error: MAX_FACES_PER_VERTEX too low. Please increase.\n");
							exit(42);
						}
						mesh.vertices[i].infaces[ mesh.vertices[i].innfaces ] = j;
						mesh.vertices[i].innfaces++;
					}
				}
			}
		}
	}

	/* Construct edge/face relations */
	tedge *e;
	tvertex *v1;
	//tvertex *v2;
	tface *f;
	long int v1i,v2i,fi; // indices
	unsigned int n_faces_found;
	for(i=0;i<mesh.nedges;i++){ // for every edge
		e=&mesh.edges[i];
		n_faces_found=0;
		e->is_boundary=0;

		/* Get pointers to its two vertices */
		v1i=e->vertices[0];
		v1=&mesh.vertices[ v1i ];
		v2i=e->vertices[1];
		//v2=&mesh.vertices[ v2i ];

		/* Check all faces that vertex 1 is part of */
		for(j=0;j<v1->innfaces;j++){
			fi=v1->infaces[j];
			f=&mesh.faces[ v1->infaces[j] ];

			/* for whether vertex 2 is also in that face */
			for(k=0;k<f->nvertices;k++){
				if(f->vertices[k]==v2i){ // if it is, we found an edge face
					e->faces[ n_faces_found ]=fi;
					n_faces_found++;
					f->edges[ f->nedges ]=i;
					f->nedges++;
				}
				if(n_faces_found==2){break;}
			}
			if(n_faces_found==2){break;}
		}
		if(n_faces_found!=2){e->is_boundary=1;}
		//printf("edge %d is hinge for faces %d and %d\n",i,e->faces[0],e->faces[1]);
		//printf("edge %d is boundary? %d\n",i,e->is_boundary);
	}

	/* Construct hinge connections for edges */
	tface *f1,*f2;
	long int vf1i,vf2i; // indices of vertices of faces 1&2
	long int hv1i=0,hv2i=0,hv3i=0,hv4i=0; // indices of vertices of hinge
	for(i=0;i<mesh.nedges;i++){
		hv3i=mesh.edges[i].vertices[0];
		hv4i=mesh.edges[i].vertices[1];
		f1=&mesh.faces[ mesh.edges[i].faces[0] ];
		f2=&mesh.faces[ mesh.edges[i].faces[1] ];
		for(j=0;j<3;j++){
			vf1i=f1->vertices[j];
			if(vf1i!=hv3i && vf1i!=hv4i){ // Third vertex
				hv1i=vf1i;
				//printf("Found third vertex: %d\n",vf1i);
			}
			vf2i=f2->vertices[j];
			if(vf2i!=hv3i && vf2i!=hv4i){ // Fourth vertex
				hv2i=vf2i;
				//printf("Found fourth vertex: %d\n",vf2i);
			}
		}
		//printf("%d %d %d %d\n",hv1i,hv2i,hv3i,hv4i);
		mesh.edges[i].hinge_v_i[0]=hv1i;
		mesh.edges[i].hinge_v_i[1]=hv2i;
		mesh.edges[i].hinge_v_i[2]=hv3i;
		mesh.edges[i].hinge_v_i[3]=hv4i;
		mesh.edges[i].hinge_v[0]=&mesh.vertices[hv1i];
		mesh.edges[i].hinge_v[1]=&mesh.vertices[hv2i];
		mesh.edges[i].hinge_v[2]=&mesh.vertices[hv3i];
		mesh.edges[i].hinge_v[3]=&mesh.vertices[hv4i];
	}


	/* Calculate outward normals of all faces */
	//tface *f;
	for(i=0;i<mesh.nfaces;i++){
		f=&(mesh.faces[i]);
		planenormal(f->outwardnormal,
					mesh.vertices[f->vertices[0]].pos,
					mesh.vertices[f->vertices[1]].pos,
					mesh.vertices[f->vertices[2]].pos);
		//printf("face %d is has %d edges: %d, %d, and %d\n",i,f->nedges,f->edges[0],f->edges[1],f->edges[2]);
	}

	/* Face center positions */
	long int nv;
	double sum_of_pos[DIM];
	for(i=0;i<mesh.nfaces;i++){
		nv=0;
		for(d=0;d<DIM;d++){
			sum_of_pos[d]=0.;
		}
		for(j=0;j<mesh.faces[i].nvertices;j++){
			for(d=0;d<DIM;d++){
				sum_of_pos[d]+=mesh.vertices[ mesh.faces[i].vertices[j] ].pos[d];
			}
			nv++;
		}
		for(d=0;d<DIM;d++){
			mesh.faces[i].centerpos[d]=sum_of_pos[d]/nv;
		}
	}

	/* Prints debugging information if enabled */
	if(MESHTROUBLE){
		for(i=0;i<mesh.nvertices;i++){
			printf("Vertex %ld has %d neighbours: ",i,mesh.vertices[i].nnb);
			for(j=0;j<mesh.vertices[i].nnb;j++){
				printf("%ld ",mesh.vertices[i].nb[j]);
			}
			printf("\n");
		}
		for(i=0;i<mesh.nvertices;i++){
			printf("Vertex %ld is in %d faces: ",i,mesh.vertices[i].innfaces);
			for(j=0;j<mesh.vertices[i].innfaces;j++){
				printf("%ld ",mesh.vertices[i].infaces[j]);
			}
			printf("\n");
		}
		for(i=0;i<mesh.nfaces;i++){
			printf("Face %ld has centerpos: %f %f %f\n",i,
				mesh.faces[i].centerpos[0],mesh.faces[i].centerpos[1],mesh.faces[i].centerpos[2]);
		}
	}

	fclose(fp);
	printf("----------------    mesh loading complete     ----------------\n");
	return;
}
/* --------- Create an fcc configuration ---------
 * Only works for cubic periodic box and a number of particles proportional to 4*a^3, with a=1,2,3
 */
void config_fcc(void){
	int n_layers_xy,n_layers_z;
	double spacing_xy,spacing_z,offset;
	int x,y,z;

	// 	/* Calculate number of layers and particle spacing */
	// 	n_layers_z=(int) floor(box.L[2]/(lod-2*sqrt(2)));
	// 	n_layers_xy=(int) floor( sqrt(((double)n_part)/n_layers_z) );
	//
	// 	/* some checks to see whether this is possible */
	// 	if(n_layers_xy*n_layers_xy!=n_part/n_layers_z){
	// 		int lessparticles=n_layers_z*n_layers_xy*n_layers_xy;
	// 		int xyp1=n_layers_z*(n_layers_xy+1)*(n_layers_xy+1);
	// 		int zp1=(n_layers_z+1)*(n_layers_xy)*(n_layers_xy);
	// 		printf("Error: number of particles %ld is incompatible with initial structure. ",n_part);
	// 		printf("Closest numbers: %d or %d \n",lessparticles,xyp1>zp1?zp1:xyp1);
	// 		exit(666);
	// 	}
	// 	// 	printf("l bx by bz: %lf %lf %lf %lf\n",lod,box.L[0],box.L[1],box.L[2]);
	// 	// 	printf("%d %d %d\n",n_layers_xy,n_layers_z);
	// 	/* Calculate lattice spacing */
	// 	spacing_xy=box.L[0]/n_layers_xy;
	// 	spacing_z=box.L[2]/n_layers_z;
	// 	offset=1E-4;
	//
	// 	/* Place particles */
	// 	int i=0;
	// 	for (z=0;z<n_layers_z;z++){
	// 		for (y=0;y<n_layers_xy;y++){
	// 			for (x=0;x<n_layers_xy;x++){
	// 				if((x+y+z)%2==0 && i<n_part){
	// 					/* Positions */
	// 					part[i].pos[0]=-0.5*box.L[0]+x*spacing_xy+0.5*spacing_xy+offset;
	// 					part[i].pos[1]=-0.5*box.L[1]+y*spacing_xy+0.5*spacing_xy+offset;
	// 					part[i].pos[2]=-0.5*box.L[2]+z*spacing_z+0.5*spacing_z+offset;
	// 					/* Parallel orientation vector */
	// 					part[i].or[0]=0.;
	// 					part[i].or[1]=0.;
	// 					part[i].or[2]=1.;
	// 					/* Perpendicular orientation vectors */
	// 					part[i].orper1[0]=1.;
	// 					part[i].orper1[1]=0.;
	// 					part[i].orper1[2]=0.;
	// 					part[i].orper2[0]=0.;
	// 					part[i].orper2[1]=1.;
	// 					part[i].orper2[2]=0.;
	// 					/* Put particle into box if outside */
	// 					putininitialbox(i);
	// 					i++;
	// 					if(i>=n_part) break;
	// 				} if(i>=n_part) break;
	// 			} if(i>=n_part) break;
	// 		} if(i>=n_part) break;
	// 	}

	/* Calculate number of layers and particle spacing */
	n_layers_z=(int) floor(box.L[2]/(2*(sqrt(2)+lod-1)));
	n_layers_xy=(int) floor( sqrt(((double)n_part)/n_layers_z) );

	/* some checks to see whether this is possible */
	if(n_layers_xy*n_layers_xy!=n_part/n_layers_z){
		int lessparticles=n_layers_z*n_layers_xy*n_layers_xy;
		int xyp1=n_layers_z*(n_layers_xy+1)*(n_layers_xy+1);
		int zp1=(n_layers_z+1)*(n_layers_xy)*(n_layers_xy);
		printf("Error: number of particles %ld is incompatible with initial structure. ",n_part);
		printf("Closest numbers: %d or %d \n",lessparticles,xyp1>zp1?zp1:xyp1);
		exit(666);
	}
	// 	printf("l bx by bz: %lf %lf %lf %lf\n",lod,box.L[0],box.L[1],box.L[2]);
	// 	printf("%d %d %d\n",n_layers_xy,n_layers_z);
	/* Calculate lattice spacing */
	spacing_xy=box.L[0]/n_layers_xy;
	spacing_z=box.L[2]/n_layers_z;
	offset=1E-4;

	/* Place particles */
	int i=0;
	for (z=0;z<n_layers_z;z++){
		for (y=0;y<n_layers_xy;y++){
			for (x=0;x<n_layers_xy;x++){
				if((x+y+z)%2==0 && i<n_part){
					/* Positions */
					part[i].pos[0]=-0.5*box.L[0]+x*spacing_xy+0.5*spacing_xy+offset;
					part[i].pos[1]=-0.5*box.L[1]+y*spacing_xy+0.5*spacing_xy+offset;
					part[i].pos[2]=-0.5*box.L[2]+z*spacing_z+0.5*spacing_z+offset;
					/* Parallel orientation vector */
					part[i].or[0]=0.;
					part[i].or[1]=0.;
					part[i].or[2]=1.;
					/* Perpendicular orientation vectors */
					part[i].orper1[0]=1.;
					part[i].orper1[1]=0.;
					part[i].orper1[2]=0.;
					part[i].orper2[0]=0.;
					part[i].orper2[1]=1.;
					part[i].orper2[2]=0.;
					/* Put particle into box if outside */
					putininitialbox(i);
					i++;
					if(i>=n_part) break;
				} if(i>=n_part) break;
			} if(i>=n_part) break;
		} if(i>=n_part) break;
	}
}
/* --------- Create a random fluid configuration (only works for low density!) ---------
 *
 */
void config_fluid(void){
	long int i;
	int d;
	double rvec[DIM];
	for(i=0;i<n_part;i++){
		/* Random positions */
		for(d=0;d<DIM;d++){
			part[i].pos[d]=2*(genrand()-0.5)*box.L[d];
		}
		/* Random parallel orientation */
		fillrvec(part[i].or);
		normalize(part[i].or);
		/* Calculate perpendicular vectors to orientation */
		fillrvec(rvec);
		normalize(rvec);
		crossprod(part[i].orper1,part[i].or,rvec);
		normalize(part[i].orper1);
		crossprod(part[i].orper2,part[i].or,part[i].orper1);
		normalize(part[i].orper2);
		/* Put particle into box if outside */
		putininitialbox(i);
	}
}
/* --------- Puts all particles at the center ---------
 * The least physical starting configuration. Still useful for some purposes.
 */
void config_center(void){
	long int i;
	double rvec[DIM];
	for(i=0;i<n_part;i++){
		/* Random positions */
		part[i].pos[0]=2*(genrand()-0.5)*0.10*box.hL[0];
		part[i].pos[1]=2*(genrand()-0.5)*0.10*box.hL[1];
		part[i].pos[2]=2*(genrand()-0.5)*0.10*box.hL[2];
	// 		part[i].pos[0]=2*(genrand()-0.5)*lod;
	// 		part[i].pos[1]=2*(genrand()-0.5)*lod;
	// 		part[i].pos[2]=2*(genrand()-0.5)*lod;
	// 		part[i].pos[0]=0;
	// 		part[i].pos[1]=0;
	// 		part[i].pos[2]=2;
		/* Random parallel orientation */
		fillrvec(part[i].or);
		normalize(part[i].or);
	// 		part[i].or[0]=0;
	// 		part[i].or[1]=0;
	// 		part[i].or[2]=-1;
	// 		normalize(part[i].or);
		//if(genrand()>0.5){part[i].or[0]=1.;}else{part[i].or[0]=-1.;}
		//part[i].or[1]=0.;
		//part[i].or[2]=0.;
		/* Calculate perpendicular vectors to orientation */
		fillrvec(rvec);
		normalize(rvec);
		crossprod(part[i].orper1,part[i].or,rvec);
		normalize(part[i].orper1);
		crossprod(part[i].orper2,part[i].or,part[i].orper1);
		normalize(part[i].orper2);
		/* Put particle into box if outside */
		putininitialbox(i);
	}
}
/* --------- Puts all particles in a shell ---------
 *
 */
void config_shell(void){
	long int i;
	double rvec[DIM],dir[DIM];
	for(i=0;i<n_part;i++){
		fillrvec(rvec);
		normalize(rvec);
		/* Random positions */
		part[i].pos[0]=rvec[0]*(1.00*RADIUS);
		part[i].pos[1]=rvec[1]*(1.00*RADIUS);
		part[i].pos[2]=rvec[2]*(1.00*RADIUS);
		/* Random orientation  parallel to shell */
		direction(dir,part[i].pos);
		fillrvec(rvec);
		normalize(rvec);
		crossprod(part[i].or,dir,rvec);
		//if(genrand()>0.5){part[i].or[0]=1.;}else{part[i].or[0]=-1.;}
		//part[i].or[1]=0.;
		//part[i].or[2]=0.;
		/* Calculate perpendicular vectors to orientation */
		fillrvec(rvec);
		normalize(rvec);
		crossprod(part[i].orper1,part[i].or,rvec);
		normalize(part[i].orper1);
		crossprod(part[i].orper2,part[i].or,part[i].orper1);
		normalize(part[i].orper2);
		/* Put particle into box if outside */
		putininitialbox(i);
	}
}
/* --------- Puts all particles in a plane ---------
 *
 */
void config_plane(void){
	long int i;
	double rvec[DIM];
	for(i=0;i<n_part;i++){
		/* Random positions */
		part[i].pos[0]=2*(genrand()-0.5)*box.hL[0];
		part[i].pos[1]=0.;
		part[i].pos[2]=2*(genrand()-0.5)*box.hL[2];
	// 		part[i].pos[0]=2*(genrand()-0.5)*lod;
	// 		part[i].pos[1]=2*(genrand()-0.5)*lod;
	// 		part[i].pos[2]=2*(genrand()-0.5)*lod;
	// 		part[i].pos[0]=0;
	// 		part[i].pos[1]=0;
	// 		part[i].pos[2]=2;
		/* Random orientation in plane */
		part[i].or[0]=(genrand()-0.5);
		part[i].or[1]=0;
		part[i].or[2]=(genrand()-0.5);
		normalize(part[i].or);
		//if(genrand()>0.5){part[i].or[0]=1.;}else{part[i].or[0]=-1.;}
		//part[i].or[1]=0.;
		//part[i].or[2]=0.;
		/* Calculate perpendicular vectors to orientation */
		fillrvec(rvec);
		normalize(rvec);
		crossprod(part[i].orper1,part[i].or,rvec);
		normalize(part[i].orper1);
		crossprod(part[i].orper2,part[i].or,part[i].orper1);
		normalize(part[i].orper2);
		/* Put particle into box if outside */
		putininitialbox(i);
	}
}
/* --------- Puts all particles in a line in the center ---------
 *
 */
void config_line(void){
	long int i;
	double rvec[DIM];
	double posfrac;
	for(i=0;i<n_part;i++){
		posfrac=((double) i) / ((double) n_part);
		/* Random positions */
		part[i].pos[0]=0.;
		part[i].pos[1]=0.70*(2*posfrac-1)*box.hL[1];
		// 		part[i].pos[1]=2*(genrand()-0.5)*box.hL[1];
		part[i].pos[2]=0.;
		if(INSIDE_CYLINDER){
			part[i].pos[0]+=2*(genrand()-0.5)*0.2*diameter_cylinder;
			part[i].pos[1]+=0.;
			part[i].pos[2]+=2*(genrand()-0.5)*0.2*diameter_cylinder;
		}
		/* Random orientation */
		part[i].or[0]=0.;
		part[i].or[1]=2*(genrand()-0.5);
		part[i].or[2]=0.;
		normalize(part[i].or);
		/* Calculate perpendicular vectors to orientation */
		fillrvec(rvec);
		normalize(rvec);
		crossprod(part[i].orper1,part[i].or,rvec);
		normalize(part[i].orper1);
		crossprod(part[i].orper2,part[i].or,part[i].orper1);
		normalize(part[i].orper2);
		/* Put particle into box if outside */
		putininitialbox(i);
	}
}
/* --------- Create a simple smectic configuration ---------
 * Particles are placed cubic in plane and stretched out of plane. Basically a rectangular lattice.
 */
void config_smectic(void){
	int n_layers_xy,n_layers_z;
	double spacing_xy,spacing_z;
	int x,y,z;
	double rvec[DIM];

	/* Calculate number of layers and particle spacing */
	n_layers_z=(int) floor(box.L[2]/lod);
	n_layers_xy=(int) floor( sqrt(((double)n_part)/n_layers_z) );

	/* some checks to see whether this is possible */
	if( fabs( ((double) n_layers_xy*n_layers_xy) - (double) n_part/ (double) n_layers_z ) > 1E-5){
		int lessparticles=n_layers_z*n_layers_xy*n_layers_xy;
		int moreparticles=n_layers_z*(n_layers_xy+1)*(n_layers_xy+1);
		printf("Error: number of particles %ld is incompatible with initial structure. ",n_part);
		printf("Closest numbers: %d or %d \n",lessparticles,moreparticles);
		exit(666);
	}

	/* Calculate lattice spacing */
	spacing_xy=box.L[0]/n_layers_xy;
	spacing_z=box.L[2]/n_layers_z;

	/* Place particles */
	long int i=0;
	for(z=0;z<n_layers_z;z++){
		for(y=0;y<n_layers_xy;y++){
			for(x=0;x<n_layers_xy;x++){
				/* Positions */
				part[i].pos[0]=-0.5*box.L[0]+x*spacing_xy+0.5*spacing_xy;
				part[i].pos[1]=-0.5*box.L[1]+y*spacing_xy+0.5*spacing_xy;
				part[i].pos[2]=-0.5*box.L[2]+z*spacing_z+0.5*spacing_z;
				if(DISTURB_INITIAL){
					part[i].pos[0]+=0.1*(genrand()-0.5);
					part[i].pos[1]+=0.1*(genrand()-0.5);
					part[i].pos[2]+=0.1*(genrand()-0.5);
				}
				/* Parallel orientation vector */
				part[i].or[0]=0.;
				part[i].or[1]=0.;
				if(genrand()>0.5){part[i].or[2]=1.;}
				else{part[i].or[2]=-1.;}
				if(DISTURB_INITIAL){
					part[i].or[0]+=0.04*(genrand()-0.5);
					part[i].or[1]+=0.04*(genrand()-0.5);
					part[i].or[2]+=0.04*(genrand()-0.5);
				}
				normalize(part[i].or);
				/* Calculate perpendicular vectors */
				fillrvec(rvec);
				normalize(rvec);
				crossprod(part[i].orper1,part[i].or,rvec);
				normalize(part[i].orper1);
				crossprod(part[i].orper2,part[i].or,part[i].orper1);
				normalize(part[i].orper2);
				/* Put particle into box if outside */
				putininitialbox(i);
				i++;
			}
		}
	}
}
/* --------- Create a smectic configuration in the middle of the box ---------
 * Particles are placed on a cubic lattice with AAA stacking in the middle of the box. The
 * particle orientation is in the short plane. If placed in an elongated box, this should promote
 * coexistence.
 */
void config_elongated_middle_smectic(void){
	int n_layers_x,n_layers_y,n_layers_z;
	double spacing_x,spacing_y,spacing_z;
	int x,y,z;
	double rvec[DIM];

	/* Calculate number of layers and particle spacing */
	n_layers_y=(int) floor(box.L[1]/part[0].l);
	spacing_y=box.L[1]/n_layers_y;
	n_layers_z=(int) floor(box.L[2]/part[2].d);
	spacing_z=box.L[2]/n_layers_z;
	n_layers_x=(int) floor( ((double) n_part)/(n_layers_y*n_layers_z) );
	spacing_x=1.0*box.L[0]/n_layers_x;

	/* some checks to see whether this is possible */
	if(box.L[1]<2*spacing_y){
		printf("Error: too few or too many particles for current y size of box.\n");
		exit(42);
	}
	if(n_layers_x*n_layers_y*n_layers_z!=n_part){
		int less=n_layers_z*n_layers_y*n_layers_x;
		int more_x=(n_layers_x+1)*n_layers_y*n_layers_z;
		int more_y=(n_layers_y+1)*n_layers_z*n_layers_x;
		int more_z=(n_layers_z+1)*n_layers_x*n_layers_y;
		printf("Error: number of particles %ld is incompatible with initial structure. ",n_part);
		printf("Closest numbers: %d, %d, %d, or %d \n",less,more_x,more_y,more_z);
		exit(42);
	}

	/* Place particles */
	long int i=0;
	for(z=0;z<n_layers_z;z++){
		for(y=0;y<n_layers_y;y++){
			for(x=0;x<n_layers_x;x++){
				/* Positions */
				part[i].pos[0]=-0.25*box.L[0]+x*spacing_x;
				part[i].pos[1]=-0.50*box.L[1]+y*spacing_y+0.5*spacing_y;
				part[i].pos[2]=-0.50*box.L[2]+z*spacing_z+0.5*spacing_z;
				if(DISTURB_INITIAL){
					part[i].pos[0]+=0.1*(genrand()-0.5);
					part[i].pos[1]+=0.1*(genrand()-0.5);
					part[i].pos[2]+=0.1*(genrand()-0.5);
				}
				/* Parallel orientation vector */
				//part[i].or[0]=0.;
				//if(genrand()>=0.5){part[i].or[0]=1.;}else{part[i].or[0]=-1.;}
				//if(y%2==0){part[i].or[1]=1.;}else{part[i].or[1]=-1.;}
				if(genrand()>=0.5){part[i].or[1]=1.;}else{part[i].or[1]=-1.;}
				part[i].or[0]=0.;
				part[i].or[2]=0.;
				if(DISTURB_INITIAL){
					part[i].or[0]+=0.04*(genrand()-0.5);
					part[i].or[1]+=0.04*(genrand()-0.5);
					part[i].or[2]+=0.04*(genrand()-0.5);
				}
				normalize(part[i].or);
				/* Calculate perpendicular vectors */
				fillrvec(rvec);
				normalize(rvec);
				crossprod(part[i].orper1,part[i].or,rvec);
				normalize(part[i].orper1);
				crossprod(part[i].orper2,part[i].or,part[i].orper1);
				normalize(part[i].orper2);
				/* Put particle into box if outside */
				putininitialbox(i);
				i++;
			}
		}
	}
}
/* --------- Create a ball of inward facing rods ---------
 *
 */
void config_ball(void){
	long int i;
	int d;
	double spacing_radial=0.6*part[0].l;
	double outward[3],rvec[3];
	double diam=part[0].d;
	double th,ph,sinth,costh;
	int nr_th_steps_at_r,nr_ph_steps_at_th;
	double th_stepsize,ph_stepsize;

	i=0;
	double r_center_pos=spacing_radial;
	double r_inner=r_center_pos-part[0].hll;
	while(i<n_part){

		/* Calculate how many theta per r */
		nr_th_steps_at_r=(int) floor( 0.5*M_PI/asin(diam/(2*r_inner)) );
		th_stepsize=M_PI/nr_th_steps_at_r;
		for(th=0.;th<M_PI;th+=0.9*th_stepsize){
			sinth=sin(th);
			costh=cos(th);
			//printf("Theta: %f\n",th);

			/* Calculate how many per phi per theta per r */
			if(r_inner*sin(th)>0.5*diam){
				nr_ph_steps_at_th=(int) floor( 0.5*2.*M_PI/asin(diam/(2.*r_inner*sin(th))) );
				ph_stepsize=2.*M_PI/nr_ph_steps_at_th;
			}else{ph_stepsize=3.*M_PI;}
			for(ph=0.;ph<2.*M_PI;ph+=0.9*ph_stepsize){
				//printf(" Phi: %f\n",ph);

				/* Positions */
				part[i].pos[0]=r_center_pos*sinth*cos(ph);
				part[i].pos[1]=r_center_pos*sinth*sin(ph);
				part[i].pos[2]=r_center_pos*costh;
				//printf(" Pos:     %f %f %f\n",part[i].pos[0],part[i].pos[1],part[i].pos[2]);
				/* Orientation is inward */
				direction(outward,part[i].pos);
				for(d=0;d<3;d++){
					part[i].or[d]=-outward[d];
				}
				//printf(" Or:      %f %f %f\n",part[i].or[0],part[i].or[1],part[i].or[2]);
				/* Calculate perpendicular vectors to orientation */
				fillrvec(rvec);
				normalize(rvec);
				crossprod(part[i].orper1,part[i].or,rvec);
				normalize(part[i].orper1);
				//printf(" Orper1:   %f %f %f\n",part[i].orper1[0],part[i].orper1[1],part[i].orper1[2]);
				crossprod(part[i].orper2,part[i].or,part[i].orper1);
				normalize(part[i].orper2);
				//printf(" Orper2:   %f %f %f\n",part[i].orper2[0],part[i].orper2[1],part[i].orper2[2]);
				/* Put particle into box if outside */
				putininitialbox(i);

				/* Next */
				i++;
				if(i>=n_part){break;}
			}
			if(i>=n_part){break;}
		}
		if(i>=n_part){break;}

		/* Increase radius r if one shell is done */
		r_center_pos+=spacing_radial;
		r_inner=r_center_pos-part[0].hll;
	}
}
/* --------- Sets a bunch of integration constants ---------
 *
 */
void set_constants(void){
	printf("\nDefining constants: \n");

	// Print how we've nondimensionalized this code.
	printf("\n Nondimensionalized by setting (largest) particle diameter d to 1, thermal\n");
	printf(" energy kT to 1 and expressing time in units of one over the rotational\n");
	printf(" diffusion (effectively 1/D_r = 1).\n");

	// In these dimensionless units, the rotational Peclet number (Pe_r = v d / D_r) is equal to
	// the self-propulsion velocity since d = 1 and 1/D_r = 1.
	self_propulsion_velocity = Peclet;
	printf(" Self-propulsion velocity in simulation units is:           v0 = %lf\n",self_propulsion_velocity);
	printf(" Rotational Peclet number is thus:                        Pe_r = %lf\n",self_propulsion_velocity);

	/* Diffusion constants for rods:
	* NOTE: These rod diffusion coefficients are technically those for cylinders, not
	* spherocylinders. However, the difference should be negligible, and using these values for
	* spherocylinders agrees with experimental results in the range 2<=l/d<=30.
	* Source DOI's: 10.1063/1.447827, 10.1103/PhysRevE.50.1232
	* Also, defining the diffusion coefficients here assumes that all rods have equal l/d.
	*/
	if(DIFFUSION_CONST_FROM_LOD){
		double parallel_diffusion_factor      = (1./(2*M_PI))*(log(lod)-0.207+0.980/lod-0.133/(lod*lod));
		double perpendicular_diffusion_factor = (1./(4*M_PI))*(log(lod)+0.839+0.185/lod+0.233/(lod*lod));
		double rotational_diffusion_factor    = (3./(M_PI*lod*lod))*(log(lod)-0.662+0.917/lod-0.050/(lod*lod));
		// Diffusion strengths in simulation units then become:
		diffusion_para_rod = parallel_diffusion_factor      / rotational_diffusion_factor;
		diffusion_perp_rod = perpendicular_diffusion_factor / rotational_diffusion_factor;
		// diffusion_rota_rod = 1 because of nondimensionalization choice
		printf(" For given l/d, rod parallel diffusion const. is:      Dt,para = %lf\n",diffusion_para_rod);
		printf(" For given l/d, rod perpendicular diffusion const. is: Dt,perp = %lf\n",diffusion_perp_rod);
		printf(" For given l/d, rod rotational diffusion const. is:     Dr,rod = %lf\n",1.0);
		// For spheres, we have simply that D_r d^2 = 3 D_t (known literature result) => D_t = 1/3
		diffusion_t_sph = 1.0 / 3.0;
		printf(" Sphere translational diffusion constant is:            Dt,sph = %lf\n",diffusion_t_sph);

		// Noise prefactors for BAOAB scheme (2nd order-ish stochastic integration scheme)
		random_force_prefactor_para_rod = sqrt(dt * diffusion_para_rod / 2.0);
		random_force_prefactor_perp_rod = sqrt(dt * diffusion_perp_rod / 2.0);
		random_force_prefactor_rota_rod = sqrt(dt / 2.0);
		random_force_prefactor_t_sph = sqrt(dt * diffusion_t_sph / 2.0);
		random_force_prefactor_r_sph = sqrt(dt / 2.0);

	}else{
		// printf(" No translational noise enabled, setting noise prefactors to 0.\n");
		// printf(" Please note that doing this violates the fluctuation-dissipation theorem.\n");
		// // No translational noise enabled
		// diffusion_para_rod = 0.0;
		// diffusion_perp_rod = 0.0;
		// diffusion_t_sph = 0.0;

		// Disregard aspect ratio influence, just set all diffusion constants to 1
		printf(" All translational diffusion constants set to 1/3.\n");
		printf(" Please note that this is technically wrong for rods.\n");
		diffusion_para_rod = 1.0 / 3.0;
		diffusion_perp_rod = 1.0 / 3.0;
		diffusion_t_sph = 1.0 / 3.0;


		// Noise prefactors for BAOAB scheme (2nd order-ish stochastic integration scheme)
		random_force_prefactor_para_rod = sqrt(dt * diffusion_para_rod / 2.0);
		random_force_prefactor_perp_rod = sqrt(dt * diffusion_perp_rod / 2.0);
		random_force_prefactor_rota_rod = sqrt(dt / 2.0);
		random_force_prefactor_t_sph = sqrt(dt * diffusion_t_sph / 2.0);
		random_force_prefactor_r_sph = sqrt(dt / 2.0);
	}

	// Force and torque prefactors
	force_prefactor_para = dt * diffusion_para_rod;
	force_prefactor_perp = dt * diffusion_perp_rod;
	force_prefactor_rota = dt;
	force_prefactor_sph  = dt * diffusion_t_sph;

	// printf(" Brownian force integration factors:\n");
	// printf("                                                      para_rod = %e\n",random_force_prefactor_para_rod);
	// printf("                                                      perp_rod = %e\n",random_force_prefactor_perp_rod);
	// printf("                                                      rota_rod = %e\n",random_force_prefactor_rota_rod);
	// printf("                                                      t_sphere = %e\n",random_force_prefactor_t_sph);
	// printf("                                                      r_sphere = %e\n",random_force_prefactor_r_sph);
	// printf("\n");
	// printf(" Interaction force integration factors (excluding WCA energy parameter epsilon!):\n");
	// printf("                                                      para_rod = %e\n",force_prefactor_para);
	// printf("                                                      perp_rod = %e\n",force_prefactor_perp);
	// printf("                                                      rota_rod = %e\n",force_prefactor_rota);
	// printf("                                                      t_sphere = %e\n",force_prefactor_sph);
	// printf("\n");

	// Then the active force prefactors
	active_prefactor_rod = dt * self_propulsion_velocity;
	active_prefactor_sph = dt * self_propulsion_velocity;
	// printf(" Active force integration factors:\n");
	// printf("                                                           rod = %e\n",active_prefactor_rod);
	// printf("                                                        sphere = %e\n",active_prefactor_sph);
	// printf("\n");

	// Some constants related to more experimental features e.g. confining mesh, crosslinks
	wca_prefactor              = 48.0 * 24.0;
	// wca_prefactor              = self_propulsion_velocity / 24.0; // Stenhammar, DOI: 10.1039/C3SM52813H
	boundary_force_prefactor   = 1.0;
	stretching_force_prefactor = stretch;
	bending_force_prefactor    = bend;
	crosslink_displacement=crosslink_speed*dt;

	// Misc. constants
	if(SPHERES){
		active_force = self_propulsion_velocity / diffusion_t_sph;
	}else{
		active_force = self_propulsion_velocity / diffusion_para_rod;
	}

	// Constants to track instabilities arising from huge forces
	max_force_translation_sq  = pow(0.1, 2);        // So that max displacement is d/10
	max_torque_rotation_sq    = pow(0.2 / lod, 2);  // So that max rotation is 2d/10l radians
	// printf(" Max value of translation due to forces before truncation:  F_max = %e\n",max_force_translation_sq);
	// printf(" Max value of rotation due to torques before truncation:    T_max = %e\n",max_torque_rotation_sq);


	// Print some useful numbers
	double plength_sph = (active_prefactor_sph/dt)/(DIM-1);
	double plength_rod = (active_prefactor_rod/dt)/(DIM-1);
	if(SPHERES){
		persistence_length          = plength_sph;
	}else{
		persistence_length          = plength_rod;
	}
	// printf(" Persistence length:                                       l_p = %lf\n",persistence_length);


	printf("Constants set.\n\n");


	// A small check:
	if(persistence_length > 2*box.shL){
		printf("Warning: Bare persistence length exceeds simulation volume dimensions. Significant finite size effects might occur!\n\n");
	}
	return;
}
/* --------- Main initialization function ---------
 * Processes the input to main to set the program to the correct modes, enabling e.g. the functions
 * necessary for confinement or crosslinks between the particles. Depending on which modes are set
 * and whether any input snapshots are provided it allocates memory for the particle arrays, sets
 * the correct size and dimensions of the simulation volume, creates an initial configuration and
 * constructs the cell list(s).
 */
void initialize(int argc, char **argv){
	int LOAD_SNAPSHOT=0,LOAD_PARAMETERS=0,GO_SET_BOX=1;
	int arg,line=0;
	char *boundaryfilename=NULL;
	char *snapshotfilename=NULL;
	char *parameterfilename=NULL;
	char *trail;

	printf("================================= Initializing... =================================\n");

	/* Read in the arguments to main and set correct mode */
	for(arg=1;arg<argc;arg++){
		if(strcmp(argv[arg],"--help")==0){
			printhelp();
			exit(1);
		}
		else if(strcmp(argv[arg],"-fromlod")==0){
			DIFFUSION_CONST_FROM_LOD=1;
			printf("Argument '-fromlod': calculating diffusion constants from rod aspect ratio.\n");
		}
		else if(strcmp(argv[arg],"-load")==0){
			snapshotfilename=argv[arg+1];
			arg++;
			LOAD_SNAPSHOT=1;
			printf("Argument '-load': loading '%s' as input particle configuration.\n",snapshotfilename);
		}
		else if(strcmp(argv[arg],"-b")==0){
			boundaryfilename=argv[arg+1];
			arg++;
			HAVE_MESH=1;
			printf("Argument '-b': loading '%s' as mesh defining confinement.\n",boundaryfilename);
		}
		else if(strcmp(argv[arg],"-p")==0){
			if(!argv[arg+1] || !argv[arg+2]){printf("Error: not enough arguments for '-p'!\n");exit(42);}
			parameterfilename=argv[arg+1];
			line=strtol(argv[arg+2],&trail,10);
			arg+=2;
			LOAD_PARAMETERS=1;
			printf("Argument '-p': loading '%s' as parameter file.\n",parameterfilename);
		}
		else if(strcmp(argv[arg],"-insph")==0){
			if( !argv[arg+1] || argv[arg+1][0]=='-' || !argv[arg+2] || argv[arg+2][0]=='-'){
				printf("Please enter two numbers for the starting and final packing fraction.\n");
				exit(42);
			}
			else{
				INSIDE_SPHERE=1;
				startpackfrac = strtod(argv[arg+1],NULL);
				endpackfrac = strtod(argv[arg+2],NULL);
				printf("Argument '%s %s %s': confining particles within spherical geometry, \
				, shrinking from a packing fraction of %lf to %lf.\n\
				",argv[arg],argv[arg+1],argv[arg+2],startpackfrac,endpackfrac);
				arg+=2;
			}
		}
		else if(strcmp(argv[arg],"-cl")==0){
			CROSSLINKS=1;
			printf("Argument '-cl': enabling crosslinks.\n");
		}
		else if(strcmp(argv[arg],"-d")==0){
			DYNAMIC_MESH=1;
			printf("Argument '-d': enabling dynamics for mesh object.\n");
		}
		else if(strcmp(argv[arg],"-sphereparticles")==0){
			SPHERES=1;
			lod=1;
			printf("Argument '-sphereparticles': all particles are spheres.\n");
		}
		else if(strcmp(argv[arg],"-onsph")==0){
			ON_SPHERE=1;
			printf("Argument '-onsph': fixing particles onto a spherical geometry.\n");
		}
		else if(strcmp(argv[arg],"-insph")==0){
			INSIDE_SPHERE=1;
			printf("Argument '-insph': confining particles within spherical geometry.\n");
		}
		else if(strcmp(argv[arg],"-quasi2D")==0){
			QUASI_2D=1;
			HAVE_PLANAR_WALLS=1;
			if( !argv[arg+1] || argv[arg+1][0]=='-' ){
				printf("Using standard Q2D height: %lf particle diameters.\n",height_Q2D);
			}
			else{height_Q2D=strtod(argv[arg+1],NULL);arg++;}
			printf("Argument '-quasi2D': confining particles between planar walls.\n");
		}
		else if(strcmp(argv[arg],"-cylinder")==0){
			INSIDE_CYLINDER=1;
			if(!argv[arg+1] || argv[arg+1][0]=='-' ){
				printf("Using standard cylinder diameter: %lf particle diameters.\n",diameter_cylinder);
			}
			else{diameter_cylinder=strtod(argv[arg+1],NULL);}
			arg++;
			printf("Argument '-cylinder': confining particles in a cylinder.\n");
		}
		else if(strcmp(argv[arg],"-longbox")==0){
			LONG_BOX=1;
			if( !argv[arg+1] || argv[arg+1][0]=='-' ){
				printf("Using standard box elongation: longest dimension is %2.02lf times shortest.\n",aspect_ratio_box);
			}
			else{aspect_ratio_box=strtod(argv[arg+1],NULL);arg++;}
			printf("Argument '-longbox': elongating simulation volume to encourage phase separation.\n");
		}
		else if(strcmp(argv[arg],"-fix_dt")==0){
			FIX_TIME_STEP=1;
			if( !argv[arg+1] || argv[arg+1][0]=='-'){
				printf("Please enter a number for the time step size.\n");
				exit(42);
			}
			else{
				fixed_time_step=strtod(argv[arg+1],NULL);
				printf("Argument '%s %s': setting time step to %e.\n",argv[arg],argv[arg+1],fixed_time_step);
				arg++;
			}
		}
		else if(strcmp(argv[arg],"-lod")==0){
			if( !argv[arg+1] || argv[arg+1][0]=='-'){
				printf("Please enter a number for the aspect ratio.\n");
				exit(42);
			}
			else{
				lod = strtod(argv[arg+1],NULL);
				printf("Argument '%s %s': setting rod aspect ratio to %e.\n",argv[arg],argv[arg+1],lod);
				arg++;
			}
		}
		else if(strcmp(argv[arg],"-packfrac")==0){
			if( !argv[arg+1] || argv[arg+1][0]=='-'){
				printf("Please enter a number for the packing fraction.\n");
				exit(42);
			}
			else{
				packfrac = strtod(argv[arg+1],NULL);
				printf("Argument '%s %s': setting packing fraction to %e.\n",argv[arg],argv[arg+1],packfrac);
				arg++;
			}
		}
		else if(strcmp(argv[arg],"-Peclet")==0){
			if( !argv[arg+1] || argv[arg+1][0]=='-'){
				printf("Please enter a number for the (rotational) Peclet number.\n");
				exit(42);
			}
			else{
				Peclet = strtod(argv[arg+1],NULL);
				printf("Argument '%s %s': setting (rotational) Peclet number to %e.\n",argv[arg],argv[arg+1],Peclet);
				arg++;
			}
		}
		else if(strcmp(argv[arg],"-N")==0){
			if( !argv[arg+1] || argv[arg+1][0]=='-'){
				printf("Please enter a number for the number of particles.\n");
				exit(42);
			}
			else{
				n_part = strtod(argv[arg+1],NULL);
				printf("Argument '%s %s': setting number of particles to %ld.\n",argv[arg],argv[arg+1],n_part);
				arg++;
			}
		}
		else{
			printf("Error: unknown command '%s'! Exiting.\n",argv[arg]);
			exit(42);
		}
	}
	printf("\n");

	/* Check for conflicts */
	if(HAVE_MESH + ON_SPHERE + INSIDE_SPHERE > 1){
		printf("Error: incompatible confinement arguments specified (e.g. '-onsph' and '-b')!\n");
		//printf("Error: simulation on spherical surface and explicit confinement are mutually exclusive!\n");
		exit(42);
	}
	if(SPHERES + HAVE_MESH + ON_SPHERE > 1){
		printf("Error: spheres + confinement not currently implemented!\n");
		exit(42);
	}
	if(QUASI_2D + LONG_BOX > 1){
		printf("Error: quasi-2D and elongated box conflict!\n");
		exit(42);
	}
	if(SPHERES && lod != 1.0){
		printf("Error: -lod X and -sphereparticle conflict.\n");
		exit(42);
	}

	/* Load input parameters from file if requested with '-p' */
	if(LOAD_PARAMETERS){
		load_parameters(parameterfilename,line);
	}else{
		printf("No parameter file specified. If not overwritten, parameters are:\n");
		printf(" Number of particles : %ld\n",n_part);
		printf(" Packing fraction    : %lf\n",packfrac);
		printf(" Aspect ratio        : %lf\n",lod);
		printf(" Peclet number       : %lf\n",Peclet);
	}
	printf("\n");

	/* If an initial configuration file is provided with '-load' */
	if(LOAD_SNAPSHOT){
		if(snapshotfilename==NULL){
			printf("Error: invalid or no input snapshot '%s' defined after argument '-load'!\n",snapshotfilename);
			exit(666);
		}
		load_snapshot(snapshotfilename);
		GO_SET_BOX=0;
	}else{
		part = calloc(n_part,sizeof(*part));
		init_particle_properties();
	}

	/* If any confining boundary is specified with '-b' */
	if(HAVE_MESH){
		if(boundaryfilename==NULL){
			printf("Error: invalid or no boundary file '%s' defined after argument '-b'!\n",boundaryfilename);
			exit(666);
		}
		load_mesh_from_ply(boundaryfilename);
		init_mesh_and_box();
		GO_SET_BOX=0;
	}

	/* If we confine particles onto an analytical spherical surface with '-onsph' */
	if(ON_SPHERE && !LOAD_SNAPSHOT){
		init_onsphere_and_box();
		GO_SET_BOX=0;
	}

	/* If we confine particles within a repulsive spherical surface with '-insph' */
	if(INSIDE_SPHERE && !LOAD_SNAPSHOT){
		init_insphere_and_box();
		GO_SET_BOX=0;
	}

	/* If we confine particles in a cylinder with '-cylinder' */
	if(INSIDE_CYLINDER && !LOAD_SNAPSHOT){
		init_incylinder_and_box();
		GO_SET_BOX=0;
	}

	/* If the simulation volume is elongated by '-longbox' */
	if(LONG_BOX && !LOAD_SNAPSHOT){
		init_elongated_box();
		GO_SET_BOX=0;
	}

	/* If the simulation is set to quasi-2D with '-quasi2D' and needs a planar box */
	if(QUASI_2D && !LOAD_SNAPSHOT){
		init_planar_box(height_Q2D);
		GO_SET_BOX=0;
	}

	/* Define the simulation volume, if it isn't done already */
	if(GO_SET_BOX){
		init_cubic_box();
	}
	printf("\n");

	/* Set particles to an initial configuration */
	if(!LOAD_SNAPSHOT){
		if(HAVE_MESH || INSIDE_SPHERE){
			config_center();
		}else if(ON_SPHERE){
			config_shell();
		}else if(LONG_BOX){
			config_elongated_middle_smectic();
			// config_fluid();
		}else if(QUASI_2D){
			config_plane();
		}else if(INSIDE_CYLINDER){
			config_line();
		}else{
			//config_fcc();
			config_fluid();
			//config_center();
			//config_ball();
			// config_smectic();
		}
	}

	/* Calculate the interaction lengths based on particle l and d */
	init_interaction_lengths();

	/* Create cell lists */
	init_cells();
	if(HAVE_MESH){
		printf("--------------- initializing cell list for mesh --------------\n");
		init_cells_wall();
		printf("----------------- completed cell list for mesh ---------------\n");
	}

	/* If crosslinks are turned on with '-c' */
	if(CROSSLINKS){init_crosslinks();}

	/* Set all sorts of constants e.g. diffusion_t_sph constants for rods */
	set_constants();

	printf("============================= Initialization Complete =============================\n");
}
/* --------- Sets the time step to a fixed number ---------
 *
 */
void set_time_step_size(double dt_input){
	// Set the value
	dt = dt_input;
	// Update all dt-dependent parameters
	random_force_prefactor_para_rod = sqrt(dt * diffusion_para_rod / 2.0);
	random_force_prefactor_perp_rod = sqrt(dt * diffusion_perp_rod / 2.0);
	random_force_prefactor_rota_rod = sqrt(dt / 2.0);
	random_force_prefactor_t_sph = sqrt(dt * diffusion_t_sph / 2.0);
	random_force_prefactor_r_sph = sqrt(dt / 2.0);
	active_prefactor_rod = dt * self_propulsion_velocity;
	active_prefactor_sph = dt * self_propulsion_velocity;
	// if(DIFFUSION_CONST_FROM_LOD){
		force_prefactor_para = dt * diffusion_para_rod;
		force_prefactor_perp = dt * diffusion_perp_rod;
		force_prefactor_sph  = dt * diffusion_t_sph;
	// }else{
		force_prefactor_para = dt;
		force_prefactor_perp = dt;
		force_prefactor_sph  = dt;
	// }
	force_prefactor_rota = dt;
	crosslink_displacement = crosslink_speed * dt;
	printf(" Set time step to: dt = %e\n",dt);

	// printf(" Brownian force integration factors:\n");
	// printf("                                                      para_rod = %e\n",random_force_prefactor_para_rod);
	// printf("                                                      perp_rod = %e\n",random_force_prefactor_perp_rod);
	// printf("                                                      rota_rod = %e\n",random_force_prefactor_rota_rod);
	// printf("                                                      t_sphere = %e\n",random_force_prefactor_t_sph);
	// printf("                                                      r_sphere = %e\n",random_force_prefactor_r_sph);
	// printf("\n");
	// printf(" Interaction force integration factors (excluding WCA energy parameter epsilon!):\n");
	// printf("                                                      para_rod = %e\n",force_prefactor_para);
	// printf("                                                      perp_rod = %e\n",force_prefactor_perp);
	// printf("                                                      rota_rod = %e\n",force_prefactor_rota);
	// printf("                                                      t_sphere = %e\n",force_prefactor_sph);
	// printf("\n");
	return;
}
/* --------- Equilibration loop that finds an appropriate time step ---------
 * The size of the time step is found iteratively by starting from a high value
 * and decreasing it every time force softening is needed. This continues until
 * no more softening is needed.
 */
void find_time_step_size(void){
	double F_soft_frac = 0.0, T_soft_frac = 0.0;
	double F_soft_frac_prev = 1.0, T_soft_frac_prev = 1.0;
	long int step2 = 0;

	// Prevent a NaN error
	if(SPHERES){ T_softened = 0; T_evaluated = 1; }

	// Set the initial time step rather high and update related parameters
	set_time_step_size(1E-3);

	// Do some initial equilibration to eliminate starting overlaps
	while(step < 5000){
		/* Main integration steps */
		evaluate_forces();
		if(SPHERES){
			integrate_positions_spheres();
			integrate_orientations_spheres();
		}else if(ON_SPHERE){
			integrate_positions_rods_on_sphere();
			integrate_orientations_rods_on_sphere();
		}else{
			integrate_positions_rods();
			integrate_orientations_rods();
		}
		step++;
	}

	// Then loop until an appropriate smaller size is found
	printf("Finding time step:\n");
	time_r = 0.0;
	step = 0;
	while (step < tot_steps){
		/* Main integration steps */
		evaluate_forces();
		if(SPHERES){
			integrate_positions_spheres();
			integrate_orientations_spheres();
		}else if(ON_SPHERE){
			integrate_positions_rods_on_sphere();
			integrate_orientations_rods_on_sphere();
		}else{
			integrate_positions_rods();
			integrate_orientations_rods();
		}
		if(HAVE_MESH && DYNAMIC_MESH){
			integrate_positions_mesh();
		}
		if(CROSSLINKS){
			integrate_positions_crosslinks();
			unbind_crosslinks();
			bind_crosslinks();
		}

		// Update time counter
		//time_r += dt;
		step   += 1;
		step2  += 1;

		// Check softening
		if(step % 100 == 0){
			F_soft_frac_prev = F_soft_frac;
			T_soft_frac_prev = T_soft_frac;
			F_soft_frac = ((double)F_softened)/F_evaluated;
			T_soft_frac = ((double)T_softened)/T_evaluated;
		}
		// and print info
		if(step % 500 == 0){
			printf(" Steps t: %06ld   Fsoft: %lf   Tsoft: %lf   Current dt: %.08lf (%.02e)\n",step2, F_soft_frac, T_soft_frac, dt, dt);
		}

		// Update time step size:
		if(step > 1000){
			// Check whether we're done
			if(F_soft_frac == 0.0 && T_soft_frac == 0.0){
				// Decrease time step one last time
				dt *= 0.5;
				set_time_step_size(dt);
				printf("Appropriate time step is: %e\n",dt);
				time_r = 0.0;
				step = 0;
				return;
			}
			// If not, check whether we need to adjust dt
			if(F_soft_frac > F_soft_frac_prev || T_soft_frac > T_soft_frac_prev){
				// Decrease time step
				dt *= 0.5;
				set_time_step_size(dt);

				// Failure criterion
				if(dt < 1E-8){
					printf("Could not find time step for which there is no softening needed. Exiting.\n");
					exit(42);
				}

				// Reset softening counters
				F_soft_frac = 0.0; F_softened = 0; F_evaluated = 0;
				if(!SPHERES){ T_soft_frac = 0.0; T_softened = 0; T_evaluated = 0; }

				// Reset steps
				//time_r = 0.0;
				step = 0;
			}else{

				// Reset softening counters
				F_soft_frac = 0.0; F_softened = 0; F_evaluated = 0;
				if(!SPHERES){ T_soft_frac = 0.0; T_softened = 0; T_evaluated = 0; }

				// Reset steps
				//time_r = 0.0;
				step = 0;
			}
		}
	}

	// If arrive here, failed
	printf("Could not find time step for which there is no softening needed. Exiting.\n");
	exit(42);
}

/*========================== Main ==========================*/
int main(int argc, char **argv){
	long int MSD_measure_steps = 0E5;
	long int next_MSD_measure_step = eq_steps;

	/* Initialize mathematical tensors */
	inittensors();

	/* Initialize random number generator */
	init_random_seed();

	/* Process the program input */
	initialize(argc,argv);

	/* Run a short loop to find an appropriate time step, or just fix it */
	if(FIX_TIME_STEP){
		set_time_step_size(fixed_time_step);
	}else{
		find_time_step_size();
	}

	/* Set the total run time */
	time_max = time_r + tot_steps*dt;

	/* Initialize all averages and state values at zero */
	virial=0.;
	clear_observable( &pressure );
	clear_observable( &virial_pressure );
	clear_observable( &swim_pressure );
	clear_observable( &nematic_order );
	clear_observable( &v_eff );
	clear_observable( &max_cluster_fraction );
	clear_observable( &degree_of_clustering );
	clear_histogram( &hist_gr );
	clear_histogram( &hist_p_eta );
	clear_histogram( &hist_packfrac_voro );
	clear_histogram( &hist_effective_velocity );
	clear_histogram( &hist_virial_pressure_x );
	clear_histogram( &hist_swim_pressure_x );
	clear_histogram( &hist_velocity_probability );
	clear_histogram( &hist_spatial_velocity_correlation );
	clear_histogram( &hist_spatial_orientation_correlation );
	F_softened=0;		F_evaluated=0;
	T_softened=0;		T_evaluated=0;
	measurements=0;

	/* Stuff for shrinking droplet */
	int shrinksteps=(int) (log(startpackfrac/endpackfrac)/(3*log(0.999)));
	// printf("shrinksteps: %d\n",shrinksteps);

	/* Start simulation for real */
	time_t secbefore=time(NULL);
	printf("Begin!\n");
	time_r = 0.0;
	// 	while (time_r<time_max){
	long long int step;
	for(step=0; step < tot_steps; step++){

    // TEMP
		if(step%(tot_steps/shrinksteps)==0){
      float scale = 1 + 1e-3;
      packfrac=Vpart/(box.volume*pow(scale,3.0));
      printf("Shrinking box, new packing fraction is %lf\n",packfrac);
      density=n_part*packfrac/Vpart;
      double lbox=pow(n_part/density,1.0/3.0);
      set_box_size(scale*box.L[0],scale*box.L[1],scale*box.L[2]);
      init_interaction_lengths();
      for(int i=0;i<n_cells;i++){free(cells[i].particles);}
      free(cells);
      init_cells();
    }

		/* NOTE Decrease time step to measure the MSD in the small dt limit */
	// 		if(step == Start_MSD_steps){
	// 			set_time_step_size(dt/1000);
	// 		}

		/* Shrink droplet */
		if(INSIDE_SPHERE){
			if(step%(tot_steps/shrinksteps)==0){
				RADIUS*=0.999;
				RADIUSSQ=RADIUS*RADIUS;
				packfrac=0.8*Vpart/(pow(2*RADIUS,3.0));
				printf("Shrinking droplet, new packing fraction is %lf\n",Vpart*3./(4.*M_PI*pow(RADIUS,3.0)));
				density=n_part*packfrac/Vpart;
				double lbox=pow(n_part/density,1.0/3.0);
				set_box_size(lbox,lbox,lbox);
				// maxsep=box.shL;
				for(int i=0;i<n_cells;i++){free(cells[i].particles);}
				free(cells);
				init_cells();
				// free(wallcells);
				// init_cells_wall();
			}
		}

		/* Make a snapshot */
		if(step%(tot_steps/snapshots)==0){
			save_timeseries();
		}
		/* Make a snapshot less frequently for small long-term storage files */
		if(step%(tot_steps/few_snapshots)==0){
			save_timeseries_sparse();
		}

		/* If equilibration is done, reset averages and counters and measure once */
		if(step==eq_steps){
			virial=0.;
			clear_observable( &pressure );
			clear_observable( &virial_pressure );
			clear_observable( &swim_pressure );
			clear_observable( &nematic_order );
			clear_observable( &v_eff );
			clear_observable( &max_cluster_fraction );
			clear_observable( &degree_of_clustering );
			clear_histogram( &hist_p_eta );
			clear_histogram( &hist_packfrac_voro );
			clear_histogram( &hist_effective_velocity );
			clear_histogram( &hist_virial_pressure_x );
			clear_histogram( &hist_swim_pressure_x );
			clear_histogram( &hist_velocity_probability );
			clear_histogram( &hist_spatial_velocity_correlation );
			clear_histogram( &hist_spatial_orientation_correlation );
			array_hist_reset( &arrayhist_3D_density_field );
			F_softened=0;		F_evaluated=0;
			T_softened=0;		T_evaluated=0;
			measurements=0;
		}

		// After equilibrating, first measure short-time MSD for some time
		// double sim_dt_to_reset_to;
		// if(step == next_MSD_measure_step){
		// 	sim_dt_to_reset_to = dt;
		// 	set_time_step_size(1E-6);
		// }else if(step == next_MSD_measure_step + MSD_measure_steps){
		// 	set_time_step_size(sim_dt_to_reset_to);
		// 	next_MSD_measure_step += (tot_steps - eq_steps) / 1;
		// }

		/* Main integration steps */
		evaluate_forces();
		if(SPHERES){
			integrate_positions_spheres();
			integrate_orientations_spheres();
		}else if(ON_SPHERE){
			integrate_positions_rods_on_sphere();
			integrate_orientations_rods_on_sphere();
		}else{
			integrate_positions_rods();
			integrate_orientations_rods();
		}
		if(HAVE_MESH && DYNAMIC_MESH){
			integrate_positions_mesh();
		}
		if(CROSSLINKS){
			integrate_positions_crosslinks();
			unbind_crosslinks();
			bind_crosslinks();
		}

		/* Rare sampling */
		if(step%nsample==0){
			measurements++;
			measure_gr();
			measure_packfrac_hist_voro();
			// measure_pressure();
			// measure_velocity_profile_x();
			// measure_velocity_and_vorticity_fields();
			measure_nematic_and_polar_order();
			measure_MSD(time_r);
			// measure_orientational_correlation(time_r);
			// measure_effective_velocity();
			// measure_velocity_probability();
			// measure_spatial_velocity_correlation();
			// measure_spatial_orientation_correlation();
			long int p_cluster_idx[n_part];		measure_clusters_density(p_cluster_idx);
			if(LONG_BOX){
				// measure_density_profile_x();
				measure_pressure_profile_x();
				measure_local_density_3D();
			}
			// if(step > next_MSD_measure_step && step < next_MSD_measure_step + MSD_measure_steps){
			// 	measure_MSD(time_r);
			// }
			if(step > eq_steps){
				measure_orientational_correlation(time_r);
			}

		}
		/* Frequent sampling */
		if(step%nfsample==0){
			if(step > eq_steps){
				measure_effective_velocity();
			}
		}


		/* Save measured values */
		if(step%(tot_steps/datapoints)==0 && measurements>0){
			save_histogram("gr", &hist_gr);
			save_histogram("p_eta", &hist_packfrac_voro);
			// save_velocity_profile_x();
			// save_velocity_profile_x_field();
			// save_vorticity_field();
			// save_pressure(time_r);
			save_orderparameter(time_r);
			// save_MSD();
			// save_orientational_correlation();
			// save_velocity_probability();
			// save_spatial_velocity_correlation();
			// save_spatial_orientation_correlation();
			if(LONG_BOX){
				// save_histogram("Density_profile_x", hist_density_x);
				// save_histogram("Pressure_profile_x", hist_virial_pressure_x);
				// save_histogram("Pressure_profile_x", hist_swim_pressure_x);
				// array_hist_save(&arrayhist_3D_density_field,"3D_Density");
			}
			save_observable_with_time("Max_cluster_frac.dat", &max_cluster_fraction, time_r);
			save_observable_with_time("degree_of_clustering.dat", &degree_of_clustering, time_r);
			if(step > eq_steps){
				save_histogram("MSD", &hist_MSD);
			}
			if(step > eq_steps + MSD_measure_steps){
				save_histogram("OrCorr", &hist_or_cor);
				save_effective_velocity(time_r);
			}
		}

		/* Print an estimate for the total simulation time */
		if(step==1E3){
			time_t secafter=time(NULL);
			printf("\nSteps per second: %lf\n", (double) step / (double) (secafter-secbefore));
			printf("Estimated simulation time (hours): %lf\n\n",(double)(tot_steps*(secafter-secbefore))/(1E3*3600));
		}

		/* Print stuff */
		if(step%nprint==0){
			if(step<eq_steps){
				printf("Equilibrating. Time t: %4.04lf tau (%3.03lf %%)    Fsoft: %12.012lf    Tsoft: %12.012lf\n",
						time_r,
						step/((double)(tot_steps))*100,
						((double)F_softened)/F_evaluated,
						((double)T_softened)/T_evaluated
				);
			}else{
				printf("Sampling.     Time t: %4.04lf tau ( %3.03lf %%)    Fsoft: %12.012lf    Tsoft: %12.012lf\n",
						time_r,
						step/((double)(tot_steps))*100,
						((double)F_softened)/F_evaluated,
						((double)T_softened)/T_evaluated
				);
			}
		}

		/* Update time counter */
		time_r+=dt;
		// 		step+=1;
	}

	/* Simulation end, save / print results */
	save_last();
	printf("Average pressure is %f\n",pressure.mean);

	/* Free allocated memory */
	long int i;
	free(part);
	for(i=0;i<n_cells;i++){free(cells[i].particles);}
	free(cells);
	destroy_histogram( &hist_gr );
	destroy_histogram( &hist_gr_idgas );
	destroy_histogram( &hist_p_eta );
	destroy_histogram( &hist_packfrac_voro );
	destroy_histogram( &hist_effective_velocity );
	destroy_histogram( &hist_MSD );
	destroy_histogram( &hist_or_cor );
	destroy_histogram( &hist_velocity_probability );
	destroy_histogram( &hist_spatial_velocity_correlation );
	destroy_histogram( &hist_spatial_orientation_correlation );
	if(LONG_BOX){
		destroy_histogram( &hist_virial_pressure_x );
		destroy_histogram( &hist_swim_pressure_x );
		array_hist_destroy( &arrayhist_3D_density_field );
	}
	for(i=0;i<n_time_lags_in_MSD_array;i++){free(pos_at_t_of_p[i]);}
	free(pos_at_t_of_p);
	for(i=0;i<n_time_lags_in_orcorr_array;i++){free(or_at_dt_of_p[i]);}
	free(or_at_dt_of_p);
	if(HAVE_MESH){
		free(mesh.vertices);
		free(mesh.edges);
		for(i=0;i<mesh.nfaces;i++){
			free(mesh.faces[i].vertices);
		}
		free(mesh.faces);

		for(i=0;i<n_cells_wall;i++){
			free(wallcells[i].particles);
			free(wallcells[i].vertices);
			free(wallcells[i].edges);
			free(wallcells[i].faces);
		}
		free(wallcells);
	}

	// 	int x,y,z;
	// 	for(x=0;x<n_field_bins_x;x++){
	// 		for(y=0;y<n_field_bins_y;y++){
	// 			free(velocity_field[x][y]);
	// 		}
	// 		free(velocity_field[x]);
	// 	}
	// 	free(velocity_field);


	/* Finish */
	return 0;
}
