#include <cstdlib>
#include <iostream>

#include "voro++.hh"
#include "voro_c_wrapper.h"

#ifdef __cplusplus
extern "C" { // Inside this "extern C" block, I can define C functions that are able to call C++ code
#endif

using namespace voro;

// Function that calls Voro++ to make a Voronoi tesselation of the particle configuration
// that is provided and return an array of the volumes of the Voronoi cell of each particle
void c_voro_get_volumes(
	double x_min, double x_max,
	double y_min, double y_max,
	double z_min, double z_max,
	int n_part,
	double pos[][3],
	double vols[]
){
	// Find a very rough number of subregions to divide the box into for numerical efficiency
	// and because this is a required input parameter.
	int n_x = floor(x_max - x_min);
	int n_y = floor(y_max - y_min);
	int n_z = floor(z_max - z_min);

	// Create a container with dimensions given by x/y/z_min/max, periodic in all 3 dimensions,
	// with 8 particles per computational block (this is not very important).
	container con(x_min, x_max, y_min, y_max, z_min, z_max, n_x, n_y, n_z, true, true, true, 8);

	// add particles into the container
	for(int i = 0; i < n_part; i++){
		con.put(i, pos[i][0], pos[i][1], pos[i][2]);
	}

	// Use the voro++ custom loops to loop over all cells and get their volumes
	c_loop_all cla(con);
	voronoicell c;
	if( cla.start() ) do if( con.compute_cell(c,cla) ){
		// Get the position and ID information for the particle
		// currently being considered by the loop. Ignore the radius
		// information.
		int idx;
		double x,y,z,r;
		cla.pos(idx,x,y,z,r);
		// Get the volume and put it in the right place in the array of volumes
		vols[idx] = c.volume();
	} while(cla.inc());
}

// Function that calls Voro++ to make a Voronoi tesselation of the particle configuration
// that is provided and return an array of the volumes of the Voronoi cell of each particle
// and an array of array of neighbour indices.
void c_voro_get_volumes_and_neighbours(
	double x_min, double x_max,
	double y_min, double y_max,
	double z_min, double z_max,
	int n_part,
	double pos[][3],
	double vols[],
	int n_neighbours[],
	int **neighbours
){
	// Find a very rough number of subregions to divide the box into for numerical efficiency
	// and because this is a required input parameter.
	int n_x = floor(x_max - x_min);
	int n_y = floor(y_max - y_min);
	int n_z = floor(z_max - z_min);

	// Create a container with dimensions given by x/y/z_min/max, periodic in all 3 dimensions,
	// with 8 particles per computational block (this is not very important).
	container con(x_min, x_max, y_min, y_max, z_min, z_max, n_x, n_y, n_z, true, true, true, 8);

	// add particles into the container
	for(int i = 0; i < n_part; i++){
		con.put(i, pos[i][0], pos[i][1], pos[i][2]);
	}

	// Calculate the Voronoi tesselation and the neighbours of every cell / particle
	c_loop_all cla(con);
	voronoicell_neighbor c;
	// NOTE: voro negative index means neighbour is its own periodic image
	std::vector< std::vector<int> > p_neighbours(n_part);
	std::vector<double> cell_areas(n_part);
	if( cla.start() ) do if( con.compute_cell(c,cla) ){
		c.neighbors( p_neighbours[cla.pid()] );
		vols[cla.pid()] = c.volume();
	} while(cla.inc());

	// Convert neighbour vector to c-style array of array of ints
	for(size_t i = 0; i < n_part; i++){
		n_neighbours[i] = p_neighbours[i].size();
		// std::cout << i << " has " << n_neighbours[i] << " nb:\n";
		for(size_t j = 0; j < p_neighbours[i].size(); j++){
			neighbours[i][j] = p_neighbours[i][j];
			// std::cout << neighbours[i][j] << " ";
		}
		// std::cout << '\n';
	}
}











#ifdef __cplusplus
}
#endif
