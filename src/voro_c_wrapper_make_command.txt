g++ -fPIC -shared voro_c_wrapper.cpp -I/usr/local/include/voro++ -o ../BDwip/libvoro_c_wrapper.so

gcc ../CODE/Brownian_Dynamics.c -o bd -lm -O3 ../CODE/voro_c_wrapper.cpp -I/usr/local/include/voro++ -L/usr/local/lib/ -lvoro++ -L. -lvoro_c_wrapper -w -lstdc++


On Hera:

g++ -fPIC -shared voro_c_wrapper.cpp -../../local/include/voro++ -o libvoro_c_wrapper.so

gcc Brownian_Dynamics.c -o bd -std=gnu99 -lm -O3 voro_c_wrapper.cpp -Wl,-rpath=. -I../../local/include/voro++ -L../../local/lib/ -lvoro++ -L. -lvoro_c_wrapper -w -lstdc++
